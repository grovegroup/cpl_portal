$(document).ready(function() {
//Edit Answer of Survey AJAX Call
    $('.editSurvey').on('click', function() {
        var id = $(this).data('uid');
        $('#lableAnswer_' + id).hide();
        $('#inputAnswer_' + id).show();

    })
    $('.SaveAnswer').on('click', function() {
        var id = $(this).data('uid');
        var selected = $("#area option:selected");
        var answer = "";
        selected.each(function() {
            answer = $(this).text();
        });

        var answerUpdate = $(".editanswer_" + id).val();
        if (answerUpdate === '' || answerUpdate == null || answerUpdate === 'undefined')
        {   
           var answers = answer;
        } else {
            
           var answers = answerUpdate;
        }
        var postdata = "";
        postdata += "uid=" + id;
        postdata += "&answer=" + answers;
        var url = window.location.href;
        var arr = url.toString().split('?');
        var arr1 = arr[1].toString().split('&');
        var baseurl = url.toString().split('/');
        var SIteUrl = baseurl[0] + '/' + baseurl[1] + '/' + baseurl[2] + '/' + baseurl[3] + '/' + baseurl[4];


        $.ajax({url: "" + SIteUrl + "/buildingsurvey/answerUpdate",
            type: 'POST',
            data: postdata,
            success: function(data) {
                $('#lableAnswer_' + id).html(data);
                $('#lableAnswer_' + id).show();
                $('#inputAnswer_' + id).hide();
            }
        });

    });
});
