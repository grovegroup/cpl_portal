// JavaScript Document
$(document).ready(function() {
	var uri = $('#uri').val();
	$.ajax({
			type: "POST",
			url: uri,
			data: "purpose=screensize&ajax=true&width=" + screen.width + "&height="+screen.height,
			async: false,
			success: function(get) { 
				if(get==='FAIL'){
					window.location.reload();
				}
			}
		});
});

//---------------------Header stylist Dropdown---------------------//
        $(document).ready(function() {
			var uri = $('#uri').val();					   
			domImageUploadInput		= $('.image-upload input')
			// Enhance Custom File Input placeholders
			if(domImageUploadInput.length > 0) {
				domImageUploadInput.each(function(){
					$(this).customFileInput();
				});
			}
								   
			//-------------------------------
            // Preloading data in markup
            //-------------------------------
		var searchtagurl = uri+"wisdom/searchtag/";
            $('#myULTags').tagit({
                //availableTags: sampleTags, // this param is of course optional. it's for autocomplete.
                // configure the name of the input field (will be submitted with form), default: item[tags]
                itemName: 'item',
                fieldName: 'keywords[]',
				allowSpaces: true,
				minLength: 2,
				removeConfirmation: true,
				tagSource: function( request, response ) {
					//console.log("1");
					$.ajax({
						
						url:searchtagurl, 
						data: { term:request.term },
						dataType: "json",
						success: function( data ) {
							response( $.map( data, function( item ) {
								return item
							}));
						}
					});
				}
            });
	    var plcwitg = $('#con_Wisdomtags').val();	
	    $('input.ui-widget-content').attr('placeholder',plcwitg);
			
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).attr("langid");
		var langword = $(this).html();
                $(".dropdown dt a span").html(langword);
		$(".dropdown dd ul").hide();
		getSelectedValue(text);
            });
                        
            
		function getSelectedValue(lang) {
		$('#current_lang').val(lang);
		var uri = $('#uri').val();
		var page = $('#webpage').val();
		$.ajax({
				type: "POST",
				url: uri+'language/',
				data: "lang="+lang+"&page="+page,
				async: false,
				success: function(get) {
					window.location.reload();
					/*var json = $.parseJSON(get);
					
					$('#headervideo').attr('title',json.con_PlayVideo);
					$('#ws_search_input').attr('placeholder',json.con_SearchforWisdom);
					$('#u_find_btn').val(json.con_Find);
					$('.u_add_btn').val(json.con_AddWisdom);
					$('#aboutas').text(json.con_About);
					$('#contactas').text(json.con_Contactus);
					$('#terms').text(json.con_TermsofUse);
					$('#partner').text(json.con_Partners);
					$('#privacy').text(json.con_PrivacyPolicy);
					$('#lang_mostpopular').text(json.con_Mostpopular);
					$('#lang_mostrecent').text(json.con_Mostrecent);
					$('#lang_legeladvicemessage').text(json.con_legeladvicemessage);

					$('#lan_con_profile_mywisdom').text(json.con_MYWISDOM);
					$('#lan_con_profile_editprofile').text(json.con_EDITPROFILE);
					$('#lan_con_profile_changepassword').text(json.con_CHANGEPASSWORD);
					$('#lan_con_profile_signin').text(json.con_Signin);
					
					$('.lang_searchresult_cure').text(json.con_Cure);
					$('.lang_searchresult_country').text(json.con_Country);
					if(page==='wisdom-detail')
					{
						$('.c_cont_name').text(json.con_SelectedArea);
						$('.kids_txt').text(json.con_WisdomforKids);
						$('#lang_cure').text(json.con_Cure);
						$('#lang_country').text(json.con_Country);
						$('#lang_region').text(json.con_Region);
						$('#lang_added').text(json.con_Added);
						$('#lang_vdo_desc').text(json.con_Videodescription);
						$('.replyreadmore').text(json.con_Readmore);
						$('.replyComment').text(json.con_Comment);
					}
					if(page==='add-wisdom')
					{
						$('#lang_cre_title').text(json.con_AddWisdom);
						$('#addwisdomname').attr('placeholder',json.con_Name);
						$('#region').attr('placeholder',json.con_Regionaddwisdom);
						$('#video_link').attr('placeholder',json.con_Videolink);
						$('#video_description').attr('placeholder',json.con_Videodescription);
						$('.c_chk_name').text(json.con_WisdomforKids);
						$('#add_wisdomsubmit').val(json.con_AddWisdom);
					}
					if(page==='sign-in')
					{
						$('#lang_sing_hr').text(json.con_Signin);
						$('#lang_signin').val(json.con_Signin);
						$('.c_or').text(json.con_or);
						$('#email').attr('placeholder',json.con_Email);
						$('#password').attr('placeholder',json.con_Password);
						$('.css-label').text(json.con_Rememberme);
						$('#lang_singin_regi').text(json.con_Notregisteredyet);
						$('#lang_singin_forgot').text(json.con_Forgotyourpassword);
					}
					if(page==='register')
					{
						$('#lang_regi_hr').text(json.con_Register);
						$('.c_or').text(json.con_or);
						$('#name').attr('placeholder',json.con_Name);
						$('#email').attr('placeholder',json.con_Email);
						$('#password').attr('placeholder',json.con_Password);
						$('#cpassword').attr('placeholder',json.con_Confirmpassword);
						$('#addregistration').val(json.con_Register);
						$('.u_terms_link').text(json.con_IaccepttheTermsandConditions);
					}
					if(page==='about')
					{
						$('#lang_about').text(json.con_AboutOmbeliko);
						$('#lang_founder').text(json.con_FounderofOmbeliko);
						$('#about_desc').html(json.cms_about);
					}
					if(page==="contact")
					{
						$('#lang_contactas').text(json.con_Contactus);
						var text = json.con_Pleasegetintouch.split('contact@ombeliko.com');
						$('#lange_cont_text1').text(text[0]);
						$('#lange_cont_text2').text(text[1]);
						$('#name').attr('placeholder',json.con_Name);
						$('#email').attr('placeholder',json.con_Email);
						$('#title').attr('placeholder',json.con_Titleofmessage);
						$('#message').attr('placeholder',json.con_YourMessage);
						$('#sendemail').val(json.con_Send);
					}
					if(page==='terms-of-use')
					{
						$('#lang_terms').text(json.con_TermsofUse);
						$('#lang_terms_description').html(json.cms_terms);
					}
					if(page==='privacy')
					{
						$('#lang_privacy').text(json.con_PrivacyPolicy);
						$('#lang_policy_description').html(json.cms_privacy);
					}
					if(page==='watch-video')
					{
						$('#lang_video').text(json.con_Watchvideo);
						var ytbvdo = '';
						if(lang==='ES')
						{
							ytbvdo = 'www.youtube.com/watch?v=_TdQyzVAzls';
						}
						else
						{
							ytbvdo = 'www.youtube.com/watch?v=bujtO6Xn0Rs';
						}
						var embeded = ytbvdo.replace(/(?:http:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g, '<iframe width="100%" height="380" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>').replace(/(?:http:\/\/)?(?:www\.)?(?:vimeo\.com)\/(.+)/g, '<iframe src="//player.vimeo.com/video/$1" width="100%" height="380" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
						$('.c_video').html(embeded);
					}*/
				}
			});
		//return $("#" + id).find("dt a span.value").html();
	}
            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });
	    //----------for profile dropdown-=----------//
		$(document).ready(function() {
			$(".abc img.flag").addClass("flagvisibility");

			$(".abc .abc_dt a").click(function() {
				$(".abc dd ul").toggle();
			});

			$(".abc dd ul li a").click(function() {
				var text = $(this).html();
				$(".abc .abc_dt a span").html(text);
				$(".abc dd ul").hide();
				//$("#result").html("Selected value is: " + getSelectedValue("sample2"));
			});

			//function getSelectedValue(id) {
				//return $("#" + id).find(".abc_dt a span.value").html();
			//}

			$(document).bind('click', function(e) {
				var $clicked = $(e.target);
				if (! $clicked.parents().hasClass("abc"))
					$(".abc dd ul").hide();
			});
		});
	//---------------for profile dropdown-----------//
        });
//---------------------Header stylist Dropdown End---------------------//


//---------------------For Tabs to accordian---------------------//
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
//---------------------For Tabs to accordian end---------------------//


//------------------------Hove function----------------------------------//
function hovercall(uri)
{
	$(".box_icon1").hover(function() {
		$(this).attr('src', uri + 'assets/images/box_icon1_hover.png');
	}, function() {
		$(this).attr('src', uri + 'assets/images/box_icon1.png');
	}
	);

	$(".box_icon2").hover(function() {
		$(this).attr('src', uri + 'assets/images/box_icon2_hover.png');
	}, function() {
		$(this).attr('src', uri + 'assets/images/box_icon2.png');
	}
	);

	$(".box_icon3").hover(function() {
		$(this).attr('src', uri + 'assets/images/box_icon3_hover.png');
	}, function() {
		$(this).attr('src', uri + 'assets/images/box_icon3.png');
	}
	);

	$(".box_icon4").hover(function() {
		$(this).attr('src', uri + 'assets/images/box_icon4_hover.png');
	}, function() {
		$(this).attr('src', uri + 'assets/images/box_icon4.png');
	}
	);

	/*$(".l_box").hover(function() {
		//alert(this.id);
		$('#flexiselDemo2Div_'+this.id).show();
		$('#flexiselDemo2Arrow_'+this.id).show();
		$('#slider_img_'+this.id).hide();
	}, function() {
		$('#flexiselDemo2Div_'+this.id).hide();
		$('#flexiselDemo2Arrow_'+this.id).hide();
		$('#slider_img_'+this.id).show();
	});*/
}
$(document).ready(function() {
var uri = $('#uri').val();

	//////////////////////for ws page icon//////////////////////
	$(".ws_user_icon1").hover(function() {
		$(this).attr('src', uri + 'assets/images/ws_user_icon1_hover.png');
	}, function() {
		$(this).attr('src', uri + 'assets/images/ws_user_icon1.png');
	}
	);

	$(".ws_user_icon2").hover(function() {
		$(this).attr('src', uri + 'assets/images/ws_user_icon2_hover.png');
	}, function() {
		$(this).attr('src', uri + 'assets/images/ws_user_icon2.png');
	}
	);

	$(".ws_user_icon3").hover(function() {
		$(this).attr('src', uri + 'assets/images/ws_user_icon3_hover.png');
	}, function() {
		$(this).attr('src', uri + 'assets/images/ws_user_icon3.png');
	}
	);

	$(".ws_user_icon4").hover(function() {
		$(this).attr('src', uri + 'assets/images/ws_user_icon4_hover.png');
	}, function() {
		$(this).attr('src', uri + 'assets/images/ws_user_icon4.png');
	}
	);
});
//////////////////-------For Tag-------//////////////
$.fn.wordlist = function(){
	return this.each(function(){
		// Elements to work with:
		var $field = $(this), // textarea
			$mask = $('<div class="wordlist-view" />'), // Styled view
			$input = $('<input type="text" class="js-wordlist-input" />'), // input
			$label = $(this).parents('.controls').siblings('.control-label').children('label');

		// Vars to use:
		var word,
			words = [];

		if ($field.val().trim().length > 0) {
			words = $field.val().split(',');
			cleanwords = [];
			$.each(words, function(){
				cleanwords.push(this.trim());
			});
			words = cleanwords;
			$field.val(words.join(','));
		}

		// Put the styled view in and hide the textarea
		$mask.insertAfter($field);
		$field.hide();

		$.each(words, function(i, word){
			// Add the words to the styled view
			$('<span>'+word.trim()+'</span>').appendTo($mask);
		});

		// Put the input in after all of the words
		$input.appendTo($mask);

		// Delete a word when you click it
		$mask.on('click', 'span', function(){
			var $word = $(this);

			// Find position of word in the array of words
			word = $word.text().trim();
			var i = words.indexOf(word);

			// Remove word from array
			words.splice(i, 1);

			// Remove the word from the styled view
			$word.remove();

			// Update the contents of the textarea
			$field.val(words.join(','));
		});

		$label.click(function(e){
			e.preventDefault();
			$input.focus();
		});

		$input.on('keypress', function(e) {
			// Build up what the user is typing
			var word = $(this).val().trim();
			if (word != '') {
				console.log(e.which);
				if (e.which == 13 || e.which == 44) {
					e.preventDefault();
					e.stopPropagation();
					// When they press enter, add the word to the array
					words.push(word);
					// Add to the display view
					$('<span>'+word.trim()+'</span>').insertBefore($input);
					// Update the contents of the textarea
					$field.val(words.join(','));

					// Clear the input
					$(this).val('');
				}
			}
		});
	});
};
//---------------For wisdom add button && LOGO Click--------------------//


$(document).ready(function() {
	$(".u_add_btn").on("click",function() {
		var uri = $('#uri').val();
		var userid=$('#user_id').val();
		if(userid>'0')
                {
                    window.location=uri+"wisdom/create/";
                }
                else{
		window.location=uri+"wisdom/create/?controller=wisdom_create";
            }
	});
	$(".c_logo").on("click",function() {
		var uri = $('#uri').val();
		window.location=uri;
	});
});



//---------------For change home page map----------------//
$(document).ready(function() {
	$("#l_cntr_cir").hide();
	$(".c_more_btn").hide();
	$('.c_less_btn').hide();
});

//-----------------------------------------------------------//
function slideSwitch(sliderid) {
    var $active = $(sliderid+' IMG.active');
    if ( $active.length == 0 ) $active = $(sliderid+' IMG:last');
    var $next =  $active.next().length ? $active.next()
        : $(sliderid+' IMG:first');
    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}
//----------------Using for ajax call------------------------//
function ajaxcall(url,data)
{
	/*$.blockUI({ 
            message: $('#ajax-loader'), 
            css: { 
                top:  ($(window).height() - 200) /2 + 'px', 
                left: ($(window).width() - 400) /2 + 'px', 
                width: '400px',
				border: 'none', 
				padding: '15px', 
				backgroundColor: '#FFF', 
				'-webkit-border-radius': '10px', 
				'-moz-border-radius': '10px', 
				opacity: .5, 
				color: '#fff'
            } 
        });*/
	$.ajax({
					type: "POST",
					url: url,
					data: data,
					async: false,
					
					success: function(get) {
						
						$(".c_more_btn").hide();
						$('.c_less_btn').hide();
						$('#wisdomDetailC_wrap').hide();
						$('#homepageC_wrap').show();
						$('.l_box_main_mostrecent').html(get);
						if (data.indexOf('onloadwisdom') === -1) {
							//var scrollIndex = $('.l_mid_del').offset();
							//$("html, body").animate({scrollTop:(scrollIndex.top)});
							$('html, body').animate({scrollTop: $(".l_middle").offset().top});
						 } 
						
						var result = $(".l_box_main_mostrecent .l_box_main_main").size();
						//var result = parseInt(size / 2);
						if (result > 7)
						{
							var x = 6;
							$(".c_more_btn").show();
							$('.l_box_main_mostrecent .l_box_main_main').hide();
							$('.l_box_main_mostrecent .l_box_main_main:lt(' + x + ')').show();
						}
						var uri = $('#uri').val();
						hovercall(uri);
						
						$( ".l_box_main_mostrecent .flexiselDemo2Div" ).each( function(){
							setInterval( "slideSwitch('#"+this.id+"')", 2000 );
							/*$('#'+this.id).flexisel({
							visibleItems: 1,
							animationSpeed: 1000,
							autoPlay: true,
							autoPlaySpeed: 3000,            
							pauseOnHover: false,
							enableResponsiveBreakpoints: true,
							responsiveBreakpoints: { 
								portrait: { 
									changePoint:480,
									visibleItems: 1
								}, 
								landscape: { 
									changePoint:640,
									visibleItems: 1
								},
								tablet: { 
									changePoint:768,
									visibleItems: 1
								}
							}
							});*/
						});
			
						//$('.nbs-flexisel-nav-left').css('display','none');
						//$('.nbs-flexisel-nav-right').css('display','none');
						
						$('.put_data_of_social_model').click( function (){
							//alert($(this).attr('uid'));
							var uid = $(this).attr('uid');
							$('#active_uid').val(uid);
						});

						$('.report').click( function (){
							//alert(this.id);
							$('#report_wisdomUid').val(this.id);
						});
						$('.language_selection').click( function (){
							$('.language_dropdown_'+this.id).slideDown( "slow" );
						});
						$('.cancel_language_selection').click( function (){
							$('.language_dropdown_'+this.id).slideUp( "slow" );
						});
						$(".language_dropdown_selection").change(function()
						{
							if (this.value === 0)
							{
								return false;
							}
							var uid = this.id;
							var description = $('#description_eng_'+uid).val();
							var cure = $('#cure_eng_'+uid).val();
							var country = $('#country_eng_'+uid).val();
							var name = $('#username_eng_'+uid).val();
							var lang = this.value;
							$.ajax({
								type: "POST",
								url: uri + "wisdom/translate/",
								data: "purpose=translate&ajax=true&lang=" + lang + "&description="+description+ "&cure="+cure+ "&country="+country+ "&name="+name,
				
								success: function(get) {
									var result = get.split('===>');
									$('.description_'+uid).text(result[0]);
									$('.cure_'+uid).text(result[1]);
									$('.country_'+uid).text(result[2]);
									$('.username_'+uid).text(result[3]);
									$('.language_dropdown_'+uid).slideUp( "slow" );
								}
							});
						});
						//---------------------for getting most popular wisdom-------------------//
						$.ajax({
							type: "POST",
							url: url,
							data: data+"&status=mostpopular",
							async: false,
							success: function(get) {
								$('.l_box_main_mostpopular').html(get);
								$('.mostpopularClass .l_view_more').hide();
								if (data.indexOf('onloadwisdom') === -1) {
									// will not be triggered because str has _..
									//var scrollIndex = $('.l_mid_del').offset();
									//$("html, body").animate({scrollTop:(scrollIndex.top)});
									$('html, body').animate({scrollTop: $(".l_middle").offset().top});
								 } 

								/*var size = $(".l_box_main_main").size();
								var result = parseInt(size / 2);
								if (result > 7)
								{
									var x = 6;
									$(".c_more_btn").show();
									$('.l_box_main_main').hide();
									$('.l_box_main_main:lt(' + x + ')').show();
								}*/
								var uri = $('#uri').val();
								hovercall(uri);
								$( ".l_box_main_mostpopular .flexiselDemo2Div" ).each( function(){
									setInterval( "slideSwitch('#"+this.id+"')", 2000 );
									/*$('#'+this.id).flexisel({
									visibleItems: 1,
									animationSpeed: 1000,
									autoPlay: true,
									autoPlaySpeed: 3000,            
									pauseOnHover: false,
									enableResponsiveBreakpoints: true,
									responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems: 1
										}, 
										landscape: { 
											changePoint:640,
											visibleItems: 1
										},
										tablet: { 
											changePoint:768,
											visibleItems: 1
										}
									}
									});*/
								});
				
								//$('.nbs-flexisel-nav-left').css('display','none');
								//$('.nbs-flexisel-nav-right').css('display','none');
								//$('.nbs-flexisel-item img').css('max-height','90px');

								$('.put_data_of_social_model').click( function (){
									//alert($(this).attr('uid'));
									var uid = $(this).attr('uid');
									$('#active_uid').val(uid);
								});
								$('.report').click( function (){
									//alert(this.id);
									$('#report_wisdomUid').val(this.id);
								});
								$('.language_selection').click( function (){
									$('.language_dropdown_'+this.id).slideDown( "slow" );
								});
								$('.cancel_language_selection').click( function (){
									$('.language_dropdown_'+this.id).slideUp( "slow" );
								});
								$(".language_dropdown_selection").change(function()
								{
									if (this.value === 0)
									{
										return false;
									}
									var uid = this.id;
									var description = $('#description_eng_'+uid).val();
									var cure = $('#cure_eng_'+uid).val();
									var country = $('#country_eng_'+uid).val();
									var name = $('#username_eng_'+uid).val();
									var lang = this.value;
									$.ajax({
										type: "POST",
										url: uri + "wisdom/translate/",
										data: "purpose=translate&ajax=true&lang=" + lang + "&description="+description+ "&cure="+cure+ "&country="+country+ "&name="+name,

										success: function(get) {
											var result = get.split('===>');
											$('.description_'+uid).text(result[0]);
											$('.cure_'+uid).text(result[1]);
											$('.country_'+uid).text(result[2]);
											$('.username_'+uid).text(result[3]);
											$('.language_dropdown_'+uid).slideUp( "slow" );
										}
									});
								});
								//wordlimit('.wordlimit', 40, true);
							}
						});
						
						$.unblockUI();
						//-----------------------------------------------------------------------//
					}
			});
}
//-----------------------------------------------------------//
//-----------Get Wisdom Data for selected country------------//

function GetCountryWisdomFromMap(id,name)
{
	//alert(id); return false;
	var uri = $('#uri').val();
	var url = uri+"wisdom/searchresult/";
	var data = "purpose=getcountry&ajax=true&contryid="+id+"&name="+name;
	ajaxcall(url,data);
}

//-----------Get Wisdom Data for selected continents------------//

function GetContinentsWisdomFromMap(id,name)
{
	//alert(id);
	var uri = $('#uri').val();
	var url = uri+"wisdom/searchresult/";
	var data = "purpose=getcontinents&ajax=true&continentid="+id+"&name="+name;
	ajaxcall(url,data);
}

//----For getting wisdom onload -----//
$(document).ready(function() {
	var uri = $('#uri').val();
	var currentParameter = $(location).attr('hash');
	var currentUrl = $(location).attr('href');
	
	if(currentParameter.indexOf('#keyword') === 0)
	{
		var keyword_url = currentParameter.split('=');
		var keyword = keyword_url[1];
		var url = uri + "wisdom/wisdom_search/";
		var data = "purpose=wisdomsearch&ajax=true&keyword=" + keyword;
		ajaxcall(url, data);
		window.location.hash="";
	}
	else if( currentUrl===uri ||
		currentUrl==='www.'+uri ||
		currentUrl===uri+'#' ||
		currentUrl===uri+'#horizontalTab1' ||
		currentUrl===uri+'#horizontalTab2' ||
		currentUrl===uri+'home/' || 
		currentUrl===uri+'home' || 
		currentUrl===uri+'home/#')
	{
		var url = uri + "wisdom/searchresult/";
		var data = "purpose=onloadwisdom&ajax=true";
		ajaxcall(url, data);
	}
      //alert(uri+"=document ready occurred!");
});
//----------------------------------//
//-------------------------View more---------------------------//
	
$(document).ready(function () {
	var x = 6;
	$('.view_more').css('cursor','pointer');
	$('.show_less').css('cursor','pointer');
	var horizontalTabClass = '';
    $(".view_more").on("click",function() 
	{
		//alert(currentDiv);
		var currentDiv = $(location).attr('hash');
		if(currentDiv === '' || currentDiv === '#horizontalTab1')
		{
			horizontalTabClass = '.l_box_main_mostrecent';
			$('.mostrecentClass .l_view_more').show();
		}	
		else
		{
			horizontalTabClass = '.l_box_main_mostpopular';
			$('.mostpopularClass .l_view_more').hide();
		}
		var result = $(horizontalTabClass+" .l_box_main_main").size();
		//alert(size);
		//var result = parseInt(size / 2);
		//alert(result);
		x = (x + 6 <= result) ? x + 6 : result;
		$('.mostrecentClass .l_box_main_main:lt(' + x + ')').fadeIn('slow');
		if (x == result) {
			$('.mostrecentClass .c_more_btn').hide();
			$('.mostrecentClass .c_less_btn').show();
		}
		//alert('size='+size+',result='+result);
	});
	
	$(".show_less").on("click",function() 
	{
		var currentDiv = $(location).attr('hash');
		if(currentDiv === '' || currentDiv === '#horizontalTab1')
		{
			horizontalTabClass = '.l_box_main_mostrecent';
			$('.mostrecentClass .l_view_more').show();
		}	
		else
		{
			horizontalTabClass = '.l_box_main_mostpopular';
			$('.mostpopularClass .l_view_more').hide();
		}
		var x = 6;
		$(".mostrecentClass .c_more_btn").show();
		$('.mostrecentClass .c_less_btn').hide();
		$(horizontalTabClass+' .l_box_main_main').hide();
		$(horizontalTabClass+' .l_box_main_main:lt(' + x + ')').fadeIn('slow');
	});
});
//-----------------------------------------------------------//
//-----------------------For Create wisdom onchange continents and select contry----------------//
$(document).ready(function () {
	$("#continent_uid").change(function()
	{
		var uid = $("#continent_uid").val();
		var option = $('option:selected',this).attr('slug');
		var uri = $('#uri').val();
		if(uid==0)
		{
			return false;
		}
		$.ajax({
					type: "POST",
					url: uri+"wisdom/create/",
					data: "purpose=countrydropdown&ajax=true&uid="+uid,
					async: false,
					success: function(get) {
						$("#country_uid").html(get);
						GetContinentsWisdomFromMap(option);
					}
			});
	});
	$("#country_uid").change(function()
	{
		var uid = $("#country_uid").val();
		var option = $('option:selected',this).attr('code');
		if (uid === 0)
		{
			return false;
		}
		GetCountryWisdomFromMap(option);
	});
});
//-----------------------------------------------------------------//
//--------------------------Get clicked wisdom detail--------------------//
	function getwisdomdetail(uid)
	{
		//uid = atob(uid);
		var uri = $('#uri').val();
		var url = uri+"wisdom/detail/";
		$.ajax({
					type: "POST",
					url: url,
					data: "purpose=wisdomdetail&ajax=true&uid="+uid,
					async: false,
					success: function(get) {
						$('#homepageC_wrap').hide();
						$('#wisdomC_wrap').show();
						$('#wisdomC_wrap').html(get);
						$("#flexiselDemo3").flexisel({
							visibleItems: 3,
							animationSpeed: 1000,
							autoPlay: true,
							autoPlaySpeed: 3000,            
							pauseOnHover: true,
							enableResponsiveBreakpoints: true,
							responsiveBreakpoints: { 
								portrait: { 
									changePoint:480,
									visibleItems: 1
								}, 
								landscape: { 
									changePoint:640,
									visibleItems: 2
								},
								tablet: { 
									changePoint:768,
									visibleItems: 3
								}
							}
						});
						hovercall(uri);
					}
			});
	}
//-------------------------------------------------------------------------//
//-----------------------For Wisdom search getting value from the title & keyword----------------//
$(document).ready(function() {
	$("#u_find_btn").click(function()
	{
		var uri = $('#uri').val();
		if($('#ws_search_input').val().trim()==='')
		{
			var Pleaseenterkeyword = $('#con_Pleaseenterkeyword').val();
			$('#ws_search_input').val('');
			$('input#ws_search_input').attr('placeholder', Pleaseenterkeyword);
			return false;
		}
		var currentUrl = $(location).attr('href');
		if( currentUrl===uri ||
			currentUrl===uri+'#' ||
			currentUrl===uri+'#horizontalTab1' ||
			currentUrl===uri+'#horizontalTab2' ||
			currentUrl===uri+'home/' || 
			currentUrl===uri+'home' || 
			currentUrl===uri+'home/#')
		{
			var url = uri + "wisdom/wisdom_search/";
			var data = "purpose=wisdomsearch&ajax=true&keyword=" + $('#ws_search_input').val();
			ajaxcall(url, data);

		}
		else {
			location.href = uri+'#keyword='+$('#ws_search_input').val();
		}
	});
	$('#ws_search_input').keypress(function(e) {
		if(e.which === 13) {
			var uri = $('#uri').val();
			if($('#ws_search_input').val().trim()==='')
			{
				var Pleaseenterkeyword = $('#con_Pleaseenterkeyword').val();
				$('#ws_search_input').val('');
				$('input#ws_search_input').attr('placeholder', Pleaseenterkeyword);
				return false;
			}
			var currentUrl = $(location).attr('href');
			if( currentUrl===uri ||
				currentUrl===uri+'#' ||
				currentUrl===uri+'#horizontalTab1' ||
				currentUrl===uri+'#horizontalTab2' ||
				currentUrl===uri+'home/' || 
				currentUrl===uri+'home' || 
				currentUrl===uri+'home/#')
			{
				var url = uri + "wisdom/wisdom_search/";
				var data = "purpose=wisdomsearch&ajax=true&keyword=" + $('#ws_search_input').val();
				ajaxcall(url, data);
				$('#ws_search_input').autocomplete( "close" );
			}
			else {
				location.href = uri+'#keyword='+$('#ws_search_input').val();
			}
		}
	});
});
//-----------------------------------------------------------------//

//-------------------Wisdom box hover effect--------------------//
$(document).ready(function () {
		$(".l_box").bind("mouseover, mouseenter", function () {
			$('.l_box').css('position', "relative");
			$(this).find(".wisdom-hover-box-transparentbg").slideDown(500);

		}).bind("mouseout, mouseleave", function () {
			$(this).find(".wisdom-hover-box-transparentbg").slideUp(500);
		});
	});
//--------------------------------------------------------------//

//--------------Social wisdome share---------------------------//
$(document).ready(function() {
	$('#put_data_of_social_model').click( function (){
		var uid = $(this).attr('uid');
		$('#active_uid').val(uid);
	});
	$('.social_share').click( function (){
		if(this.id === 'share_fb')
		{
			var uid = $('#active_uid').val();
			window.open("https://www.facebook.com/sharer/sharer.php?u=http://ombeliko.com/wisdom/detail/"+uid+"/","menubar=no", "toolbar=no, scrollbars=yes, resizable=yes, top=250, left=500, width=450, height=400");
			$('#active_uid').val('');
		}
		if(this.id === 'share_gplus')
		{
			var uid = $('#active_uid').val();
			window.open("https://plus.google.com/share?url=http://ombeliko.com/wisdom/detail/"+uid+"/", "menubar=no", "toolbar=no, scrollbars=yes, resizable=yes, top=250, left=500, width=450, height=400");
			$('#active_uid').val('');
		}
		if(this.id === 'share_twitter')
		{
			var uid = $('#active_uid').val();
			window.open("https://twitter.com/intent/tweet?source=ombeliko.com&url=http://ombeliko.com/wisdom/detail/"+uid+"/", "menubar=no", "toolbar=no, scrollbars=yes, resizable=yes, top=250, left=500, width=450, height=400");
			$('#active_uid').val('');
		}
		if(this.id === 'share_pin')
		{
			var uid = $('#active_uid').val();
			var pin_image = $('#Image_share_pin_'+uid).val();
			var description = $('#description_eng_'+uid).val();
			window.open("http://pinterest.com/pin/create/button/?url=http://ombeliko.com/wisdom/detail/"+uid+"/&&media="+pin_image+"&description="+description, "menubar=no", "toolbar=no, scrollbars=yes, resizable=yes, top=250, left=300, width=500, height=650");
			$('#active_uid').val('');
		}
		if(this.id === 'share_email')
		{
			var uid = $('#active_uid').val();
			window.open("http://ombeliko.com/emailshare/"+ uid +"/", "menubar=no", "toolbar=no, scrollbars=yes, resizable=yes, top=250, left=200, width=1000, height=700");
			
			$('#active_uid').val('');
		}
	});
});
//---------------------------------------------------------------//
//-------------------Comment-----------------------------------//

$(document).ready(function() {
	var uri = $('#uri').val();
	$('#send_comment').click( function (){
	
		if($(this).attr('login')==='true')
		{
			if($('#comment_input').val().trim()==='')
			{
				var Pleaseenterkeyword = $('#con_Pleaseenterkeyword').val();
				$('#comment_input').attr('placeholder', Pleaseenterkeyword);
				return false;
			}
			else
			{
				var userid= $(this).attr('userUid');
				var wisdomuid = $(this).attr('wisdom_uid');
				var comment = $('#comment_input').val();
				$.ajax({
					type: "POST",
					url: uri + "wisdom/comment/",
					data: "purpose=addcomment&ajax=true&useruid=" + userid +"&wisdomuid="+ wisdomuid +"&comment="+ comment + "&subcomment=0&isMain=1",
					async: false,
					success: function(get) {
						
						/*$('#add_more_comment').append(get);
						$('#comment_input').val('');*/
						var geturl=window.location.hash;
						
						if(geturl=='#add_more_comment')
        					{
							
							//window.location =$(location);
							window.location.reload();
						}
						else{
						window.location = $(location).attr('href')+'#add_more_comment';
						window.location.reload();
						}
					}
				});
			}
			
		}
		else
		{
			window.location = uri + "registration/signin";
		}
	});
	
	$('.replyComment').click( function (){
		if($(this).attr('login')==='true')
		{
			$("#comment_box_"+this.id).slideDown( "slow" );
		}
		else
		{
			window.location = uri + "registration/signin";
		}
	});
	
	$('.reply_comment_cancel').click( function (){
		var array = this.id.split('_');
		var id = array[3];
		$("#comment_box_"+id).slideUp("slow");
		$("#reply_comment_input_"+id).val('');
	});
	
	$('.reply_comment_add').click( function (){
		
		var array = this.id.split('_');
		var id = array[3];
		if($('#reply_comment_input_'+id).val().trim()==='')
		{
			var Pleaseenterkeyword = $('#con_Pleaseenterkeyword').val();
			$('#reply_comment_input_'+id).attr('placeholder', Pleaseenterkeyword);
			return false;
		}
		else
		{
			var userid= $(this).attr('userUid');
			var wisdomuid = $(this).attr('wisdom_uid');
			var comment = $('#reply_comment_input_'+id).val();
			var mainCommentUid = $(this).attr('mainCommentUid');
			$.ajax({
				type: "POST",
				url: uri + "wisdom/comment/",
				data: "purpose=addcomment&ajax=true&useruid=" + userid +"&wisdomuid="+ wisdomuid +"&comment="+ comment+ "&subcomment="+mainCommentUid+"&isMain=0",
				async: false,
				success: function(get) {
					
					/*$('#mainComment_'+id).append(get);
					$('#reply_comment_input_'+id).val('');
					$("#comment_box_"+id).slideUp("slow");*/
					var geturl=window.location.hash;
                                        if(geturl=='#add_more_comment')
        					{
							
							//window.location =$(location);
							window.location.reload();
						}
                                        else{
					window.location = $(location).attr('href')+'#add_more_comment';
					window.location.reload();
                                    }
				}
			});
		}
	});
	$('.replyreadmore').click( function (){
               
               var id=this.id;
               var readless=$ (this).attr('class').split(' ')[1];
               var readmore=$("#read_more").val();
               var showless=$("#show_less").val();
               
                if(readless=='replyreadmore')
                {
		$('.comment_discription_'+this.id).css('overflow','initial');
		$('.comment_discription_'+this.id).css('text-overflow','initial');
		$('.comment_discription_'+this.id).css('white-space','initial');
                $('.c_name_links #'+id).removeClass("replyreadmore");
                $('.c_name_links #'+id).addClass("readless"); 
                $('.c_name_links #'+id).html(showless);
                }
                else if(readless=='readless')
                {
                   $('.comment_discription_'+this.id).removeAttr("style");
                   $('.c_name_links #'+id).html(readmore);
                   $('.c_name_links #'+id).removeClass("readless");
                   $('.c_name_links #'+id).addClass("replyreadmore");
                  
                }
                
	});
});
//------------------------------------------------------------//

//---------------------Comment language-----------------------//
$(document).ready(function() {
	var uri = $('#uri').val();
	$('.language_selection').click( function (){
		$('.language_dropdown_'+this.id).slideDown( "slow" );
	});
	$('.cancel_language_selection').click( function (){
		$('.language_dropdown_'+this.id).slideUp( "slow" );
	});
	$(".language_dropdown_selection").change(function()
	{
		if (this.value === 0)
		{
			return false;
		}
		var uid = this.id;
		var text = $('#comment_discription_eng_'+uid).val();
		var lang = this.value;
		$.ajax({
			type: "POST",
			url: uri + "wisdom/translate/",
			data: "purpose=commentTranslate&ajax=true&lang=" + lang + "&text="+text,

			success: function(get) {
				$('.comment_discription_'+uid).text(get);
				$('.language_dropdown_'+uid).slideUp( "slow" );
			}
		});
	});
	$(".language_dropdown_selection_detail").change(function()
	{
		if (this.value === 0)
		{
			return false;
		}
		var uid = this.id;
		var lang = this.value;
		var jasonData = $('#json_'+uid).val();
		var json = $.parseJSON(jasonData);
		$.ajax({
			type: "POST",
			url: uri + "wisdom/translate/",
			data: "purpose=translate&ajax=true&lang=" + lang + "&description="+json.description+ "&cure="+json.cure+ "&country="+json.country+ "&name="+json.UserName+ "&region="+json.region + "&videodiscription="+json.VideoDiscription,
			async: false,
			success: function(get) {
				var result = get.split('===>');
				$('.description_'+uid).text(result[0]);
				$('.cure_'+uid).text(result[1]);
				$('.country_'+uid).text(result[2]);
				$('.username_'+uid).text(result[3]);
				$('.region_'+uid).text(result[4]);
				$('.videodiscription_'+uid).text(result[5]);
				$('.remodal-close').trigger('click');
				$('.defleng').css('display','block');
			}
		});
	});
});
//------------------------------------------------------------//
//---------For add wisdom description page character count----------//
$(document).ready(function() {
	$("#description").keyup(function(){
		el = $(this);
		if(el.val().length < 101){
			$("#entered_charecter").text(el.val().length);
		} else if(el.val().length > 100){
			$("#entered_charecter").text('100');		
		}
	});
});
//------------------------------------------------------------------//
function getWebcontent(lang,uri){
	return $.ajax({
				type: "POST",
				url: uri+'language/',
				data: "lang="+lang,
				async: false,
				success: function(get) {
					
				}
	}).responseText;
}
//------------check validation of create wisdom---------------//
function checkvalidationofcreatewisdom()
{
	var lang = $('#current_lang').val();
	if(lang=='')
	{
		lang="EN";
	}
	var uri = $('#uri').val();
	var jsondata = getWebcontent(lang,uri);
	//console.log(json);
	var json = $.parseJSON(jsondata);

	var error_message = '';
	var ren = true;
	if($('#addwisdomname').val().length < 4 || $('#addwisdomname').val().length > 250)
	{
		error_message+= json.con_err_alt_addwisdom_5to250charecter+'<br>';
		ren = false;
	}
	if(typeof($('.tagit-hidden-field').val())=== 'undefined')
	{
		error_message+= json.con_err_alt_wisdomtag+'<br>';
		ren = false;
	}
	if($('#addwisdomcountry_uid').val()=== '0')
	{
		error_message+= json.con_err_alt_addwisdom_country_of_wisdom+'<br>';
		ren = false;
	}
	if($('#description').val().length < 100)
	{
		error_message+= 'Wisdom description must be more than 100 characters.<br>';
		ren = false;
	}
	if($('#image_1').val() === '')
	{
		error_message+= json.con_err_alt_addwisdom_upload_image+'<br>';
		ren = false;
	}
	if($('#image_2').val() === '')
	{
		error_message+= json.con_err_alt_addwisdom_upload_image2+'<br>';
		ren = false;
	}
	if($('#image_3').val() === '')
	{
		error_message+= json.con_err_alt_addwisdom_upload_image3+'<br>';
		ren = false;
	}
	
	/*else if(ytVidId($('#video_link').val()) === 'false')
	{
		error_message+= 'Please provide valid youtube video url.<br>';
		ren = 'false';
	}*/
	if(error_message !== '')
	{
		var return_error_message ='<div class="message"><p class="error">';
		return_error_message+=error_message;
		return_error_message+='</p></div>';
		$('.error-msg').html(return_error_message);
	}
	
	return ren;
}
/*function ytVidId(url) {
    var p = '/^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/';
    return (url.test(p)) ? 'true' : 'false';
}*/
//------------------------------------------------------------//


//------------For create wisdom upload image click------------//

$(document).ready(function() {
	$('#wisdomimgeupload1').css('cursor','pointer');
	$('#wisdomimgeupload1').click( function (){
		//alert('call');
		$('#image_1').trigger('click');
	});
	$('#wisdomimgeupload2').css('cursor','pointer');
	$('#wisdomimgeupload2').click( function (){
		//alert('call');
		$('#image_2').trigger('click');
	});
	$('#wisdomimgeupload3').css('cursor','pointer');
	$('#wisdomimgeupload3').click( function (){
		//alert('call');
		$('#image_3').trigger('click');
	});
});
//-----------------------------------------------------------//

//------------For registration upload image click------------//

$(document).ready(function() {
	$('#registerimageupload').css('cursor','pointer');
	$('#registerimageupload').click( function (){
		//alert('call');
		$('#image_1').trigger('click');
	});
});
//-----------------------------------------------------------//
//-----------------For alert wisdom detail-------------------//
$(document).ready(function(){
  $(".btn1").click(function(){
    $(".c_alert2").slideUp();
  });
  $(".btn2").click(function(){
    $(".c_alert2").slideDown();
  });
  $(".btn3").click(function(){
    $(".l_alert").remove();
  });
});
//-----------------------------------------------------------//
//------------For autocompleat search box----------//
$(document).ready(function() {
var uri = $('#uri').val();
	$('#ws_search_input').autocomplete({
			//availableTags: sampleTags, // this param is of course optional. it's for autocomplete.
			// configure the name of the input field (will be submitted with form), default: item[tags]
			source: uri+'autocomplete/',
            minLength: 2,
	    select: function (event, ui) {
			var currentUrl = $(location).attr('href');
			if( currentUrl===uri ||
				currentUrl===uri+'#' ||
				currentUrl===uri+'#horizontalTab1' ||
				currentUrl===uri+'#horizontalTab2' ||
				currentUrl===uri+'home/' || 
				currentUrl===uri+'home' || 
				currentUrl===uri+'home/#')
			{
				var url = uri + "wisdom/wisdom_search/";
				var data = "purpose=wisdomsearch&ajax=true&keyword=" + $('#ws_search_input').val();
				ajaxcall(url, data);

			}
			else {
				location.href = uri+'#keyword='+$('#ws_search_input').val();
			}
		}
	});
});	
	
//-------------------------------------------------------------//
//--------For slider light box------//
$(document).ready(function() {
	$(".group3").colorbox({rel:'group3', slideshow:true,transition:"none", width:"70%", height:"70%"});
});	
//----------------------------------//
//----for add wisdom autocomplete-----//
$(document).ready(function() {
var uri = $('#uri').val();
	$('#addwisdomname').autocomplete({
			//availableTags: sampleTags, // this param is of course optional. it's for autocomplete.
			// configure the name of the input field (will be submitted with form), default: item[tags]
			source: uri+'autocomplete/',
			//autoFocus: true,
            minLength: 2,
	});
});	

//------------------------------------//

//--------For wisdom detail defualt language button------//
$(document).ready(function() {
	var uri = $('#uri').val();
	$('#defleng').click( function (){
		var uid = $('#wisdomUid').val();
		var lang = 'en';
		var jasonData = $('#json_'+uid).val();
		var json = $.parseJSON(jasonData);
		$.ajax({
			type: "POST",
			url: uri + "wisdom/translate/",
			data: "purpose=translate&ajax=true&lang=" + lang + "&description="+json.description+ "&cure="+json.cure+ "&country="+json.country+ "&name="+json.UserName+ "&region="+json.region + "&videodiscription="+json.VideoDiscription,
			async: false,
			success: function(get) {
				var result = get.split('===>');
				$('.description_'+uid).text(result[0]);
				$('.cure_'+uid).text(result[1]);
				$('.country_'+uid).text(result[2]);
				$('.username_'+uid).text(result[3]);
				$('.region_'+uid).text(result[4]);
				$('.videodiscription_'+uid).text(result[5]);
				//$('.remodal-close').trigger('click');
				$('.language_dropdown_selection_detail option[value="en"]').attr("selected",true);
				$('.defleng').css('display','none');
			}
		});
	});
});
//--------------------------------------------------------//

//back code start 


$(document).ready(function() {
    $('.u_back').click( function (){
        window.history.back();
    });
});
//back code end

//---For footer youtub url----//
$(document).ready(function() {
var lang = $('#current_lang').val();
var url = 'https://www.youtube.com/watch?v=_sc93_dh5kE';
if(lang==='DE')
{
	url ='https://www.youtube.com/watch?v=SDycBhGkKjw';
}
$('#footer_youtub').attr('href',url);
});

//---End of For footer youtub url----//

//---For image crop url----//
//$(document).ready(function() {
//$('.cropme').simpleCropper();
//});
$(function() {
       $('.image-editor').cropit({
          exportZoom: 1.25,
          imageBackground: true,
          imageBackgroundBorderWidth: 20,
          imageState: {
            src: ''
          }
        });

        
});
//---End of For footer youtub url----//

$(document).ready(function() {
	
	$('#removewisdom_cancle').click( function (){
		$('.remodal-close').trigger('click');
	});
	$('#removewisdom_remove').click( function (){
		$('.remodal-close').trigger('click');
	});
});



