$(document).ready(function() {

    $('#next1').click(function() {
        var ValidateEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if ($('#name').val() == '')
        {
            alert("Please enter Name");
            return false;
        } else if ($('#email').val() == '') {
            alert("Please enter Email Id");
            return false;
        }else if(!ValidateEmail.test($('#email').val())){
            alert("Please enter Valid Email Id");
            return false;
        }else if ($('#password').val() == '') {
            alert("Please enter Password");
            return false;
        }else if ($('#password').val().length < 6) {
            alert("Password must be at least 6 characters long.");
            return false;
        }else if ($('#cpassword').val() == '') {
            alert("Please enter Confirm Password");
            return false;
        }else if ($('#cpassword').val().length < 6) {
            alert("Confirm Password must be at least 6 characters long.");
            return false;
        }else if ($('#cpassword').val() != $('#password').val()) {
            alert("Password and Confirm Password are not match.");
            return false;
        }else {
            $('.step2').css('display', 'block');
            $('.step1').css('display', 'none');
            $('.step3').css('display', 'none');
        }
    });

    $('#next2').click(function() {
        var $radios = $('input:radio[name=permission]');
        if ($radios.is(':checked') === false) {
           alert("Please Select any one Permission.");
            return false;
        }else {
            $('.step2').css('display', 'none');
            $('.step1').css('display', 'none');
            $('.step3').css('display', 'block');
        }
    });
    $('#previous1').click(function() {

        $('.step1').css('display', 'block');
        $('.step2').css('display', 'none');
        $('.step3').css('display', 'none');
    });
    $('#previous2').click(function() {

        $('.step1').css('display', 'none');
        $('.step2').css('display', 'block');
        $('.step3').css('display', 'none');
    });
    $('#signup').click(function() {     
     if($('#area').val() == '0')
     {
        alert("Please Select Area");
        return false;
     }else{
         $('#registeradd').submit();
     }
    });
});
