
function registrationValidation()
{
	var isValid=false;
	var isEmail=false;
	var country = $('#regi_country_uid').val();
	var register_img=$("#register_img").val();
	var country_err=$("#country_err").val();
	var register_terms=$("#register_terms").val();
	if(country==='0')
	{
		$('#country_alert').show();
		$('#country_alert').css('color','red');
		$('#country_alert').html(country_err);
		$('#country_alert').fadeOut(6000);
		return false;
	}
	else if($("#image_1").val() == ''){
		$('#image_alert').show();
		$('#image_alert').css('color','red');
		$('#image_alert').html(register_img);
		$('#image_alert').fadeOut(6000);
		return false;
	}
	else if (!$("#checkboxG2").is(":checked")) {
		
		$('#terms_alert').show();
		$('#terms_alert').css('color','red');
		$('#terms_alert').html(register_terms);
		$('#terms_alert').fadeOut(6000);
		return false;
    	}
	//Set the URL
	var url = window.location.href;
	var uri = $('#uri').val();
	var email = $('#email').val();
	$.ajax({
				type: "POST",
				url: url,
				data: "purpose=checkemail&ajax=true&email="+email,
				async: false,
				success: function(get) {
					if(get==='SUCCESS')
					{
						isEmail = true;
					}
					else
					{
						$('#email_alert').show();
						$('#email_alert').css('color','red');
						$('#email_alert').html('Entered Email-id already exits.');
						$('#email_alert').fadeOut(6000);
						$('#email').focus();
						isEmail = false;
					}
				}
		});
	if(isEmail)
	{
		//Set captch value
		var value = $('#txtCaptcha').val();
		//Set up the parameters of our AJAX call
		$.ajax({
					type: "POST",
					url: url,
					data: "purpose=captcha&ajax=true&txtCaptcha="+value,
					async: false,
					success: function(get) {
						if(get==='SUCCESS')
						{
							var imageData = $('.image-editor').cropit('export');
							$('.hidden-image-data').val(imageData);
							isValid = true;
						}
						else
						{
							//Get a reference to CAPTCHA image
							img = document.getElementById('imgCaptcha'); 
							//Change the image
							img.src = uri+'captcha/?' + Math.random();
							$('#captcha_alert').show();
							$('#captcha_alert').css('color','red');
							$('#captcha_alert').html('Please Enter Correct Captcha.');
							$('#captcha_alert').fadeOut(6000);
							$('#txtCaptcha').focus();
							isValid = false;
						}
					}
			});
		return isValid;
	}
	else
	{
		return false;
	}
	/*$.post(url, {purpose: 'captcha',ajax:'true',txtCaptcha:value}, function(json) {
		var response = json;
		if (response) {
			console.log(response);
			if(response.status=='SUCCESS')
			{
				temp = '1';
				return true;
			}
			else
			{
				temp = '2';
				//Get a reference to CAPTCHA image
				img = document.getElementById('imgCaptcha'); 
				//Change the image
				img.src = uri+'captcha/?' + Math.random();
				return false;
			}
		}
	}, 'json').error(function() {
		setTimeout(function() {
			registrationValidation()
		}, 5000);
	});*/
}
//---------------------------------edit profile-----------------------//
function editprofileValidation()
{
	var isEmail=false;
	var country = $('#profilecountry_uid').val();
	var country_err=$("#country_err").val();
	
	if(country==='0')
	{
		$('#country_alert').show();
		$('#country_alert').css('color','red');
		$('#country_alert').html(country_err);
		$('#country_alert').fadeOut(6000);
		return false;
	}
	if($("#image_1").val() !== ''){
		var imageData = $('.image-editor').cropit('export');
		$('.hidden-image-data').val(imageData);
	}
	//Set the URL
	var email = $('#email').val();
	var loginEmail = $('#loginEmail').val();
	if(loginEmail !== email)
	{
			var uri = $('#uri').val();
			var url = uri + 'registration/create';
			$.ajax({
				type: "POST",
				url: url,
				data: "purpose=checkemail&ajax=true&email="+email,
				async: false,
				success: function(get) {
					if(get==='SUCCESS')
					{
						isEmail = true;
					}
					else
					{
						$('#email_alert').show();
						$('#email_alert').css('color','red');
						//$('#email_alert').html('Entered Email-id already exits.');
						//$('#email_alert').fadeOut(6000);
						$('#email').focus();
						isEmail = false;
					}
				}
		});
		if(isEmail)
		{
			return isEmail;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return true;
	}
}
//--------------------------------------------------------------------//
window.fbAsyncInit = function() {
    FB.init({
      appId      : '1586975758189070', // Set YOUR APP ID
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
 
    FB.Event.subscribe('auth.authResponseChange', function(response) 
    {
		if (response.status === 'not_authorized') 
		{
			$('#social_alert').show();
			$('#social_alert').css('color','red');
			$('#social_alert').html('Failed to Connect with facebook.');
			$('#social_alert').fadeOut(6000);
			return false;
		} 
	}); 
 
};
 
    function Login()
    {
 
        FB.login(function(response) {
           if (response.authResponse) 
           {
                getUserInfo();
            } 
			else 
            {
				$('#social_alert').show();
				$('#social_alert').css('color','red');
				$('#social_alert').html('User cancelled login or did not fully authorize.');
				$('#social_alert').fadeOut(6000);
				return false;
            }
         },{scope: 'email,user_photos,user_videos'});
 
    }
 
  function getUserInfo() {
		var name = '';
		var id = '';
		var email = '';
		var photo = '';
		FB.api('/me/picture?type=normal', function(response) {

			  //var str="<br/><b>Pic</b> : <img src='"+response.data.url+"'/>";
			  //document.getElementById("status").innerHTML+=str;
			  $('#photourl').val(response.data.url);
			console.log(response);
		});
			FB.api('/me', function(response) {
		
		 /* var str="<b>Name</b> : "+response.name+"<br>";
			  str +="<b>Link: </b>"+response.link+"<br>";
			  str +="<b>Username:</b> "+response.username+"<br>";
			  str +="<b>id: </b>"+response.id+"<br>";
			  str +="<b>Email:</b> "+response.email+"<br>";
			  str +="<input type='button' value='Get Photo' onclick='getPhoto();'/>";
			  str +="<input type='button' value='Logout' onclick='Logout();'/>";
			  document.getElementById("status").innerHTML=str;*/
			var name = response.name;
			var id = response.id;
			var email = response.email;
			//var photo = $('#photourl').val();
			var photo = 'http://graph.facebook.com/'+id+'/picture';
			$.ajax({
							type: "POST",
							url: window.location.href,
							data: "purpose=sociallogin&ajax=true&id="+id+"&email="+email+"&name="+name+"&image="+photo,
							async: false,
							success: function(get) {
								if(get==='SUCCESS')
								{
									$('#photourl').val('');
									var uri = $('#uri').val();
									window.location=uri+"home/";
								}
								else
								{
									$('#social_alert').show();
									$('#social_alert').css('color','red');
									$('#social_alert').html('Failed to Connect with facebook.');
									$('#social_alert').fadeOut(6000);
									return false;
								}
							}
					});
		});	
	}
    function getPhoto()
    {
      FB.api('/me/picture?type=normal', function(response) {
 
          //var str="<br/><b>Pic</b> : <img src='"+response.data.url+"'/>";
          //document.getElementById("status").innerHTML+=str;
		  return response.data.url;
 
    });
 
    }
    function Logout()
    {
        FB.logout(function(){document.location.reload();});
    }
 
  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
   /////*************** Google+ *****************/////
   // Load the SDK asynchronously
   (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
	 
	function onLoadCallback()
	{
		gapi.client.setApiKey('AIzaSyB_FQWlzWxdmGskMF3mm5s9SyOZZv8hN8M');
		gapi.client.load('plus', 'v1',function(){});
	}
	
function logout_g()
{
    gapi.auth.signOut();
    location.reload();
}
function login_google() 
{
  var myParams = {
    'clientid' : '891726500782-utbu5ai8guj0tli5rjgmpioriguhcams.apps.googleusercontent.com',
    'cookiepolicy' : 'single_host_origin',
    'callback' : 'loginCallback',
    'approvalprompt':'force',
    'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
  };
  gapi.auth.signIn(myParams);
}
 
function loginCallback(result)
{
    if(result['status']['signed_in'])
    {
        var request = gapi.client.plus.people.get(
        {
            'userId': 'me'
        });
        request.execute(function (resp)
        {
	
            var email = '';
            if(resp['emails'])
            {
                for(i = 0; i < resp['emails'].length; i++)
                {
                    if(resp['emails'][i]['type'] == 'account')
                    {
                        email = resp['emails'][i]['value'];
                    }
                }
            }

			var name = resp['displayName'];
			var photo = resp['image']['url'].split('?')[0];
			
            var id = resp['id'];
			$.ajax({
					type: "POST",
					url: window.location.href,
					data: "purpose=sociallogin&ajax=true&id="+id+"&email="+email+"&name="+name+"&image="+photo,
					async: false,
					success: function(get) {
						if(get==='SUCCESS')
						{
							$('#photourl').val('');
							var uri = $('#uri').val();
							window.location=uri+"home/";
						}
						else
						{
							$('#social_alert').show();
							$('#social_alert').css('color','red');
							$('#social_alert').html('Failed to Connect with facebook.');
							$('#social_alert').fadeOut(6000);
							return false;
						}
					}
			});
        });
 
    }
	else
	{
		$('#social_alert').show();
		$('#social_alert').css('color','red');
		$('#social_alert').html('User cancelled login or did not fully authorize.');
		$('#social_alert').fadeOut(6000);
		return false;
	}
 
}

$(document).ready(function() {
	$('#crpbtn_rmv').click( function (){
		
				$('#registerimageupload').show();
				$('#crpbtn_slctimg').show();
				$('.cropit-image-preview').hide();
				$('#crpbtn_chng').hide();
				$('#crpbtn_rmv').hide();
                $('.cropit-image-zoom-input').hide();
				$("#image_1").val("");
	});
});
