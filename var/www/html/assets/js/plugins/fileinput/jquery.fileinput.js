/**
 * --------------------------------------------------------------------
 * jQuery customfileinput plugin
 * Author: Scott Jehl, scott@filamentgroup.com
 * Copyright (c) 2009 Filament Group
 * licensed under MIT (filamentgroup.com/examples/mit-license.txt)
 * --------------------------------------------------------------------
 */
$.fn.customFileInput = function(){
	//apply events and styles for file input element
	//add class for CSS
	var randoms = 0;
	var fileInput = $(this).addClass('customfile-input').mouseover(function(){
		upload.addClass('customfile-hover');
		if($(this).attr('id') == 'best-image') {
			$('.fixed').addClass('hovered');
		}
	}).mouseout(function(){
		upload.removeClass('customfile-hover');
		if($(this).attr('id') == 'best-image') {
			$('.fixed').removeClass('hovered');
		}
	}).focus(function(){
		upload.addClass('customfile-focus');
		fileInput.data('val', fileInput.val());
	}).blur(function(){
		upload.removeClass('customfile-focus');
		$(this).trigger('checkChange');
	 }).bind('disable',function(){
		fileInput.attr('disabled',true);
		upload.addClass('customfile-disabled');
	}).bind('enable',function(){
		fileInput.removeAttr('disabled');
		upload.removeClass('customfile-disabled');
	}).bind('checkChange', function(){
		if(fileInput.val() && fileInput.val() != fileInput.data('val')){
			fileInput.trigger('change');
		}
	}).bind('change',function(evt){
		//get file name
		var fileName = $(this).val().split(/\\/).pop();
		//get file extension
		var fileExt = 'customfile-ext-' + fileName.split('.').pop().toLowerCase();

		var isWriter = ($('#account-writer').is(':checked') ? true : false);
		var isVisual = ($('#account-visual').is(':checked') ? true : false);
		var isMusical = ($('#account-musical').is(':checked') ? true : false);
		var isOther = ($('#account-other').is(':checked') ? true : false);

		if($('#profile-type-header').length > 0) {
			var headText = $('#profile-type-header').attr('data-profile-type');
			if(headText=='visual'){
				isVisual=true;
			} else if(headText=='musician'||headText=='musical'){
				isMusical=true;
			} else if(headText=='writer'){
				isWriter=true;
			} else {
				isOther=true;
			}
		}

		var allowed = false;
		var allowedFormat = '';


		var isAvatarImage = fileInput.attr('id')=='avatar-image' ? true : false;

		if(!isAvatarImage) {
			console.log('not avatar');
			console.log('isVisual:', isVisual);
			console.log(fileExt);
			if(isVisual) {
				if(fileExt == 'customfile-ext-png' || fileExt == 'customfile-ext-gif' || fileExt == 'customfile-ext-jpg' || fileExt == 'customfile-ext-jpeg') {
					allowed = true;
					console.log('matched');
				} else {
					console.log('failed');
					allowedFormat = 'Invalid image format selected. Only JPG, PNG or GIF files accepted. Click "Change" to choose a new file.';
				}
			} else if(isWriter) {
				if(fileExt == 'customfile-ext-pdf') {
					allowed = true;
				} else {
					allowedFormat = 'Invalid document format selected. Only PDF files are accepted. Click "Change" to upload a new PDF.';
				}
			} else if(isMusical) {
				if(fileExt == 'customfile-ext-mp3') {
					allowed = true;
				} else {
					allowedFormat = 'Invalid audio format selected. Only MP3 files are accepted. Click "Change" to upload a new MP3.';
				}
			} else if(isOther) {
				if(fileExt == 'customfile-ext-png' || fileExt == 'customfile-ext-gif' || fileExt == 'customfile-ext-jpg' || fileExt == 'customfile-ext-jpeg') {
					allowed = true;
				} else {
					allowedFormat = 'Invalid image format selected. Only JPG, PNG or GIF files accepted. Click "Change" to choose a new file.';
				}
			}
		} else {
			if(fileExt == 'customfile-ext-png' || fileExt == 'customfile-ext-gif' || fileExt == 'customfile-ext-jpg' || fileExt == 'customfile-ext-jpeg') {
				allowed = true;
			} else {
				allowedFormat = 'Invalid image format selected. Only JPG, PNG or GIF files accepted. Click "Change" to choose a new file.';
			}
		}

		if( allowed ) {
			fileInput.closest('.upload-settings').removeClass('filetype-error');
			//update the feedback
			uploadFeedback
				.text(fileName) //set feedback text to filename
				.removeClass(uploadFeedback.data('fileExt') || '') //remove any existing file extension class
				.addClass(fileExt) //add file extension class
				.data('fileExt', fileExt) //store file extension for class removal on next change
				.addClass('customfile-feedback-populated'); //add class to show populated state
			//change text of button
			// removed by AW@MS
			uploadButton.text('Change');
			uploadReset.css({display:'block'});
			//var domPercentMarker = $('.percent-complete-marker');
			//if(!domPercentMarker.hasClass('percent_'+fileInput.attr('id'))) {
			//	domPercentMarker.addClass('percent_'+fileInput.attr('id')).trigger('updateBy',{amount:fileInput.attr('data-percent'), from:fileInput.attr('id')});
			//}
			console.log(isAvatarImage);
			console.log(isVisual);
			console.log(isOther);
			if(isAvatarImage || isVisual || isOther) {
				if (window.FileReader) {
					var files	= evt.target.files,
						f		= files[0],
						reader	= new FileReader();
					reader.onload = (function(theFile) {
						return function(e) {
							fileInput.closest('.image-upload').find('.customfile-preview').css({opacity:0}).html('<img src="'+ e.target.result +'" title="'+ theFile.name +'" />').animate({opacity:1}, 500, function(){
								fileInput.trigger('engage_crop');
							});
							fileInput.closest('.image-upload').find('.customfile-feedback').css({opacity:1}).animate({opacity:0},250);
							if(fileInput.closest('.designer-notice').length > 0) {
								fileInput.closest('.designer-notice').find('.fixed').css({opacity:0});
							}
						};
					})(f);
					reader.readAsDataURL(f);
				} else {
					fileInput.trigger('unsupported_filereader');
				}
			} else {
				if(fileInput.closest('.designer-notice').length > 0) {
					fileInput.closest('.designer-notice').find('.fixed').css({opacity:0});
				}
			}
		} else {
			fileInput.closest('.designer-notice').find('.fixed').css({opacity:0});
			fileInput.closest('.upload-settings').addClass('filetype-error');

			console.log(fileExt);

			uploadFeedback
				.text(allowedFormat) //set feedback text to filename
				.removeClass(uploadFeedback.data('fileExt') || '') //remove any existing file extension class
				.addClass(fileExt) //add file extension class
				.data('fileExt', fileExt) //store file extension for class removal on next change
				.addClass('customfile-feedback-populated'); //add class to show populated state
			//change text of button
			// removed by AW@MS
			uploadButton.text('Change');
			uploadReset.css({display:'block'});
		}
	}).click(function(){ //for IE and Opera, make sure change fires after choosing a file, using an async callback
		fileInput.data('val', fileInput.val());
		setTimeout(function(){
			fileInput.trigger('checkChange');
		},100);
	}).bind('unsupported_filereader',function(){
		// find which elements we want to use as the parent
		if($('.photo-edit-overlay').length < 1) {
			var elParent = $('#signup-form').length > 0 ? $('#signup-form') : null;
			if(!elParent) {
				elParent = $('#form-profile-edit').length > 0 ? $('#form-profile-edit') : null;
			}
			if(elParent) {
				/*var htmlPanel = '<div class="photo-edit-overlay">';
					htmlPanel+= '<div class="photo-edit-unsupported">';
					htmlPanel+= '<h3><span class="step">UNSUPPORTED FEATURE</span></h3>';
					htmlPanel+= '<p>Your browser does not support the new FileReader API used to create and crop a thumbnail for your portfolio. Please upgrade or install a more modern browser. It could make your web browsing experience more awesome and it is more fun.</p>';
					htmlPanel+= '<div class="return-to-previous"><a href="#" class="close-overlay btn submit-button">Continue</a></div>';
					htmlPanel+= '</div>';
					htmlPanel+= '</div>';
				*/
				var htmlPanel = '<div class="photo-edit-overlay">';
					htmlPanel+= '<div class="photo-edit-unsupported">';
					htmlPanel+= '<div class="c-Unsupported-FileReader">';
					htmlPanel+= '<h3 class="c-Unsupported-FileReader-Header">There is a technical hurdle in road ;(</h3>';
					htmlPanel+= '<p class="c-Unsupported-FileReader-description">It appears your web browser is not compatible with the latest cool technologies we use on our site. In order to complete your profile we ask that you please try again using: <a href="https://www.google.com/intl/en/chrome/" target="_blank">Google Chrome</a> or <a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank">Firefox</a>.</p>';
					htmlPanel+= '<div class="c-Unsupported-FileReader-Icons">';
					htmlPanel+= '<a href="https://www.google.com/intl/en/chrome/" target="_new" class="c-Browser-Chrome"></a>';
					htmlPanel+= '<a href="http://www.mozilla.org/en-US/firefox/new/" target="_new" class="c-Browser-Firefox"></a>';
					htmlPanel+= '</div>';
					htmlPanel+= '<p class="c-Unsupported-FileReader-highlight">Simply log in again, choose your path, and continue where you left off. Your info is already saved but your account won’t become active until you complete this step.</p>';
					htmlPanel+= '<p class="c-Unsupported-FileReader-description">We apologize for the buzz-kill but we will see you on the flip side!</p>';
					htmlPanel+= '<p class="c-Unsupported-FileReader-description">Thanks for your patience. (Welcome to our world ;)</p>';
					htmlPanel+= '<div class="return-to-previous"><a href="#" class="close-overlay btn submit-button">Continue</a></div>';
					htmlPanel+= '</div>';
					htmlPanel+= '</div>';
					htmlPanel+= '</div>';
				elParent.append(htmlPanel);
				elParent.find('.photo-edit-overlay').css({opacity:0,top:0,left:0,display:'block',height:elParent.height()+'px',width:elParent.width()+'px'}).animate({opacity:1},350,function(){
					var $overlay = $(this);
					$(this).find('.close-overlay').bind('click',function(){
						$overlay.animate({opacity:0},350,function(){
							$overlay.remove();
						});
					});
				});
			}
		}
	}).bind('engage_crop',function(){
		// find which elements we want to use as the parent
		if($('.photo-edit-overlay').length < 1) {
			var elParent = $('#signup-form').length > 0 ? $('#signup-form') : null;
			if(!elParent) {
				elParent = $('#form-profile-edit').length > 0 ? $('#form-profile-edit') : null;
			}

			if(elParent) {
				var imgUpload 	= fileInput.closest('.image-upload'),
					imgSrc 		= imgUpload.find('.customfile-preview img'),
					imgUrl		= imgSrc.attr('src');

				imgSrc.attr('src','').attr('src',imgUrl).load(function(){
					var oH			= imgSrc.height(),
						oW			= imgSrc.width(),
						imgH		= imgSrc.height(),
						imgW		= imgSrc.width();

					imgSrc.css({'width':'auto','height':'auto'});

						imgH		= imgSrc.height();
						imgW		= imgSrc.width();

					imgSrc.css({'width':oW,'height':oH});

						// The container should be a maximum of 410px high
						// and a maximum of 500px wide, so we need to adjust
						// the image height and width to be in proportion to this
					var maxRatio	= 410/500,
						imgRatio	= parseInt(imgH, 10)/parseInt(imgW, 10),
						newH		= parseInt(imgH, 10),
						newW		= parseInt(imgW, 10);

					if(imgRatio !== maxRatio){
						// Then we need to change it
						if(imgRatio > maxRatio){
							// The height is greater than our box height, proportionately
							// Check if it's physically higher, in which case we need to scale down the height
							//if(newH > 410) {
							//	newH = 510;
							//	newW = newH / imgRatio;
							//} else {
							//	// It should be smaller, so we can leave the dimensions as they were originally
							//}

							if(newW > 500){
								newW = 500;
								newH = imgRatio * newW;
							}
						} else {
							// The width is greater than our box width, proportionately
							// Check if it's physically wider, in which case we need to scale down the width
							if(newW > 500){
								newW = 500;
								newH = imgRatio * newW;
							} else {
								// It should be smaller, so we cna leave the dimensions as they were originally
							}
						}
					} else {
						// If it's bigger than our maximum, scale it down
						//if(newH > 410) {
						//	newH = 410;
						//	newW = 500;
						//}
							if(newW > 500){
								newW = 500;
								newH = imgRatio * newW;
							}
					}

					//console.log('imgH:'+imgH+', imgW:'+imgW);

					var targetId	= 'target'+(new Date()).getTime(),
						previewId	= 'preview-thumb'+(new Date()).getTime(),
						cropHeight	= parseInt(fileInput.attr('data-crop-height'), 10),
						cropWidth	= parseInt(fileInput.attr('data-crop-width'), 10),
						dimensionSuffix = fileInput.attr('name'),

						htmlPanel = '<div class="photo-edit-overlay">';
						htmlPanel+= '<div class="photo-edit-crop">';
						htmlPanel+= '<h3><span class="step">'+imgUpload.attr('data-preview-title')+'</span></h3>';
						htmlPanel+= '<p class="instruction">'+imgUpload.attr('data-preview-instruction')+' <em>(No logos please)</em></p>';
						htmlPanel+= '<div class="crop-container">';
						htmlPanel+= '<div id="crop-raw">';
						htmlPanel+= '<img src="'+imgSrc.attr('src')+'" id="'+targetId+'" width="'+newW+'" height="'+newH+'" />';//width="100%"
						htmlPanel+= '</div>';
						htmlPanel+= '<div id="crop-preview">';
						htmlPanel+= '<h4>Thumbnail Preview</h4>';
						htmlPanel+= '<div id="image-preview">';
						htmlPanel+= '<img src="'+imgSrc.attr('src')+'" id="'+previewId+'" class="jcrop-preview" />';
						htmlPanel+= '</div>';
						htmlPanel+= '<p class="instruction">Drag the handles to resize<br />the frame. Click "save" below<br />to complete your crop.</p>';
						htmlPanel+= '</div>';
						htmlPanel+= '</div>';
						htmlPanel+= '<div class="return-to-previous"><a href="#" class="close-overlay btn submit-button">Choose new Image</a> <a href="#" class="close-overlay btn submit-button save-crop">Save this Crop</a></div>';
						htmlPanel+= '</div>';
						htmlPanel+= '</div>';
					elParent.append(htmlPanel).find('#'+targetId).css({opacity:0});
					elParent.find('.photo-edit-overlay').css({opacity:0,top:0,left:0,display:'block',height:elParent.height()+'px',width:elParent.width()+'px'}).animate({opacity:1},350,function(){
						var $overlay = $(this);
						$(this).find('.close-overlay').bind('click',function(){
							// check if we have 'save' clicked or not
							if($(this).hasClass('save-crop')) {
								imgUpload.find('.customfile-preview').html($('#image-preview').html());
								//alert('do something funky with the image');
							}
							if(!$(this).hasClass('save-crop')) {
								uploadReset.trigger('click');
							}
							$overlay.animate({opacity:0},350,function(){
								$overlay.remove();
							});
							return false;
						});

						var jcrop_api, boundx, boundy;
						// 158 x 180
						// 440 x 500
						// 115 x 144
						//
						var targ = $('#'+targetId);
						var targH = targ.height();
						var targW = targ.width();
						//targ.css({width:'100%'}).animate({opacity:1},150,function(){
						targ.animate({opacity:1},150,function(){
							$('#image-preview').css({
								width: cropWidth+'px',
								height: cropHeight+'px',
								marginLeft: (cropWidth==115 ? '38px' : '16px')
							});
							$('#'+targetId).Jcrop({
								onChange:	updatePreview,
								onSelect:	updatePreview,
								//aspectRatio:500/440
								minSize: 	[ cropWidth, cropHeight ],
								aspectRatio:cropWidth / cropHeight
							},function(){
								// Use the API to get the real image size
								var bounds = this.getBounds();
								boundx = bounds[0];
								boundy = bounds[1];
								// Store the API in the jcrop_api variable
								jcrop_api = this;
							});

							function updatePreview(c) {
								//console.log(c);
								//console.log(dimensionSuffix);
								if (parseInt(c.w) > 0) {
									var rx = cropWidth / c.w;
									var ry = cropHeight / c.h;

									//console.log(boundx+' '+boundy+' '+rx+' ' +ry);
									// Get Original Image dimensions
									var imgWidthCrop = imgW;
									var imgHeightCrop = ( targH / targW ) * imgWidthCrop;
									var vStretch = imgHeightCrop / targH;
									var hStretch = imgWidthCrop / targW;
									/*
									console.log('c.x:'+c.x);
									console.log('c.y:'+c.y);
									console.log('c.x2:'+c.x2);
									console.log('c.y2:'+c.y2);
									console.log('c.w:'+c.w);
									console.log('c.h:'+c.h);
									*/

									/*
									var newX = (c.x / hStretch),
										newX2 = (c.x2 / hStretch),
										newY = (c.y / vStretch),
										newY2 = (c.y2 / vStretch),
										newW = parseInt(newX2-newX, 10),
										newH = parseInt(newY2-newY, 10);
									*/
									var newX = (c.x * hStretch),
										newX2 = (c.x2 * hStretch),
										newY = (c.y * vStretch),
										newY2 = (c.y2 * vStretch),
										newW = parseInt(newX2-newX, 10),
										newH = parseInt(newY2-newY, 10);

									/*
									console.log('newX:'+newX);
									console.log('newY:'+newY);
									console.log('newX2:'+newX2);
									console.log('newY2:'+newY2);
									console.log('newW:'+newW);
									console.log('newH:'+newH);

									console.info('------------------------------------');
									*/

									$('#x1-'+dimensionSuffix+'-image').val(parseInt(newX, 10));
									$('#y1-'+dimensionSuffix+'-image').val(parseInt(newY,10));
									$('#x2-'+dimensionSuffix+'-image').val(parseInt(newX2,10));
									$('#y2-'+dimensionSuffix+'-image').val(parseInt(newY2,10));
									$('#w-'+dimensionSuffix+'-image').val(parseInt(newW,10));
									$('#h-'+dimensionSuffix+'-image').val(parseInt(newH,10));
									$('#'+previewId).css({
										width:		Math.round(rx * boundx) + 'px',
										height:		Math.round(ry * boundy) + 'px',
										marginLeft:	'-' + Math.round(rx * c.x) + 'px',
										marginTop:	'-' + Math.round(ry * c.y) + 'px'
									});
								}
							}




							/*
							function showCoords(c) {
								$('#x1').val(c.x);
								$('#y1').val(c.y);
								$('#x2').val(c.x2);
								$('#y2').val(c.y2);
								$('#w').val(c.w);
								$('#h').val(c.h);
							}

							function clearCoords() {
								$('#coords input').val('');
							}

							$('#target').Jcrop({
								onChange:	showCoords,
								onSelect:	showCoords,
								onRelease:	clearCoords
							});
							*/
						});
					});
				});
			} else {
				//console.log('elParent:does not exists');
			}
		} else {
			//console.log('.photo-edit-overlay does not exist');
		}
	}).bind('edit_enable',function(){
		// set up conditions for editing
		var t = (new Date()).getTime()+randoms;
		randoms+=1;
		$(this).parent().find('.customfile-preview').append('<img src="'+$(this).attr('data-img')+'" id="preview-thumb'+t+'" class="jcrop-preview" style="width:100%;height:auto;" />').css({opacity:1});
		$(this).parent().find('.customfile-feedback').css({opacity:0});
		$(this).parent().find('.customfile-reset').css({display:'block'});
		$(this).parent().find('.customfile-button').text('Change');
		//<img src="https://foliosweep.s3.amazonaws.com/cache/mugshots/6731a60b2b_avatar.jpg" id="preview-thumb" class="jcrop-preview">
	});

	//create custom control container
	var upload = $('<div class="customfile"></div>');
	//create custom control button
	//var uploadButton = $('<span class="customfile-button" aria-hidden="true">Browse</span>').appendTo(upload);
	var uploadButton = $('<span class="customfile-button" aria-hidden="true"></span>').appendTo(upload);
	//create custom control feedback
	var uploadFeedback = $('<span class="customfile-feedback" aria-hidden="true">'+((fileInput.attr('data-helper')) ? fileInput.attr('data-helper') : 'Upload a picture of yourself')+'</span>').appendTo(upload);
	//create custom image preview placeholder
	var uploadPreview = $('<span class="customfile-preview" aria-hidden="true"></span>').appendTo(upload);
	//delete/reset option
	var uploadReset = $('<span class="customfile-reset" aria-hidden="true"><img src="/static/images/close.png" alt="Cancel" /></span>').appendTo(upload);

	//match disabled state
	if(fileInput.is('[disabled]')){
		fileInput.trigger('disable');
	}

	//on mousemove, keep file input under the cursor to steal click
	upload.mousemove(function(e){
		//console.info(e.pageY);
		//console.log(upload.offset().top);
		//console.log($(window).scrollTop());
		//console.log(e.pageY - upload.offset().top - $(window).scrollTop());
		fileInput.css({
			'left': e.pageX - upload.offset().left - fileInput.outerWidth() + 20, //position right side 20px right of cursor X)
			//'top': e.pageY - upload.offset().top - $(window).scrollTop() - 3
			'top': e.pageY - upload.offset().top - 3
		});
	}).insertAfter(fileInput); //insert after the input

	uploadReset.bind({
		click:function(){
			if(fileInput.closest('.designer-notice').length > 0) {
				fileInput.closest('.designer-notice').find('.fixed').css({opacity:1});
			}
			fileInput.closest('.upload-settings').removeClass('filetype-error');
			var id = fileInput.attr('id');
			var name = fileInput.attr('name');
			var helper = (fileInput.attr('data-helper') ? fileInput.attr('data-helper') : ' ');
			var percent = (fileInput.attr('data-percent') ? fileInput.attr('data-percent') : ' ');
			var cropWidth = (fileInput.attr('data-crop-width') ? fileInput.attr('data-crop-width') : ' ');
			var cropHeight = (fileInput.attr('data-crop-height') ? fileInput.attr('data-crop-height') : ' ');
			var input = $('<input type="file" name="'+name+'" id="'+id+'" type="file" data-helper="'+helper+'" data-percent="'+percent+'" data-crop-width="'+cropWidth+'" data-crop-height="'+cropHeight+'" />');
			fileInput.closest('.image-upload').fadeOut('fast',function(){
				$(this).html('').append(input).find('#'+id).customFileInput();
				$(this).fadeIn('fast');
			});
			//var domPercentMarker = $('.percent-complete-marker');
			//if(domPercentMarker.hasClass('percent_'+fileInput.attr('id'))) {
			//	domPercentMarker.removeClass('percent_'+fileInput.attr('id')).trigger('updateBy',{amount:'-'+fileInput.attr('data-percent'), from:fileInput.attr('id')});
			//}
		}
	});

	fileInput.appendTo(upload);

	if(fileInput.hasClass('edit-mode')){
		setTimeout(function(){
			if(fileInput.hasClass('from-facebook')) {
				$('#x1-avatar-image').val('0');
				$('#y1-avatar-image').val('0');
				var img = $('.jcrop-preview').eq(0);
				$('#x2-avatar-image').val(img.width());
				$('#y2-avatar-image').val(img.height());
				$('#coords-avatar-image').append('<input type="hidden" name="facebook_image" value="' + img.attr('src')+'" id="facebook_image" />');
			}
		},1500);
		fileInput.trigger('edit_enable');
	}

	//return jQuery
	return $(this);
};