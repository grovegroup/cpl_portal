$(function() {
    // Side Bar Toggle
    $('.hide-sidebar').click(function() {
	  $('#sidebar').hide('fast', function() {
	  	$('#content').removeClass('span9');
	  	$('#content').addClass('span12');
	  	$('.hide-sidebar').hide();
	  	$('.show-sidebar').show();
	  });
	});

	$('.show-sidebar').click(function() {
		$('#content').removeClass('span12');
	   	$('#content').addClass('span9');
	   	$('.show-sidebar').hide();
	   	$('.hide-sidebar').show();
	  	$('#sidebar').show('fast');
	});
	
	$('#fileInput1').change(function() {
	   var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#wip1").attr("src", this.result);
            }
        }
	});
	$('#fileInput2').change(function() {
	   var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#wip2").attr("src", this.result);
            }
        }
	});
	$('#fileInput3').change(function() {
	   var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#wip3").attr("src", this.result);
            }
        }
	});
	$('#fileInputUserE1').change(function() {
	   var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#uip1").attr("src", this.result);
            }
        }
	});

	$('#partnereditimg').change(function() {
	   var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#uip1").attr("src", this.result);
            }
        }
	});

	 //cms pages change language get data function start
        
	$('#li_Privacy').click(function() {
        var cms_privacy=$("#keywordprivacy").val();
        $.ajax({
                url: uri+'admin/cms/',
                type: 'POST',
                data: {
                       pageselect:'cms_privacy',
                       cmspage:'cmsdata',},
                success: function() {
                  
                }
            });
       
        });
        
        $('#li_Terms').click(function() {
       
            var cms_terms=$("#keywordterms").val();
            $.ajax({
                url: uri+'admin/cms/',
                type: 'POST',
                data: {
                       pageselect:'cms_terms',
                       cmspage:'cmsdata',},
                success: function(data) {
                  
                }
            });
        });
        
        $('#li_Aboutus').click(function() {
       
            var cms_about=$("#keywordabout").val();
            $.ajax({
                url: uri+'admin/cms/',
                type: 'POST',
                data: {
                       pageselect:'cms_about',
                       cmspage:'cmsdata',},
                success: function(data) {
                  
                }
            });
        });
        
        $("#language").change(function() {
         
            var pageValue=$("#keywordprivacy").val();
             var selectedValue=$(this).val();
             var cms_privacy=$("#keywordprivacy").val();
             var uri=$("#uri").val();
            //make the ajax call
            $.ajax({
                url: uri+'admin/cms/',
                type: 'POST',
                data: {language : selectedValue,
                       page:pageValue,
                       cmspage:'cmsdata',
                       pageselect:'cms_privacy'},
                success: function(data) {
                   
                   location.reload(true);
                 
                }
            });
});

$("#languageterms").change(function() {
         
             //get the selected value
             
         var pageValue=$("#keywordterms").val();
          var url=$("#uri").val(); 
          var selectedValue=$(this).val();
          var cms_terms=$("#keywordterms").val();
            //make the ajax call
            $.ajax({
                url: uri+'admin/cms/',
                type: 'POST',
                data: {language : selectedValue,
                       page:pageValue,
                       cmspage:'cmsdata',
                       pageselect:'cms_terms'},
                success: function() {
                    
                        location.reload(true);
                   
                }
            });
});

$("#languageabout").change(function() {
    
         
             //get the selected value
             
         var pageValue=$("#keywordabout").val();
          var url=$("#uri").val(); 
          var selectedValue=$(this).val();
          var cms_about=$("#keywordabout").val();
          
            //make the ajax call
            $.ajax({
                url: uri+'admin/cms/',
                type: 'POST',
                data: {language : selectedValue,
                       page:pageValue,
                       cmspage:'cmsdata',
                       pageselect:'cms_about'},
                success: function() {
                    
                  location.reload(true);
                }
            });
});

        
       //cms pages change language get data function end
});
