$(document).ready(function() {
//admin custom date
$('#SearchDate').change(function() {
        var $this = $(this),
        $value = $this.val();        
        if($value == "Custom Date"){
        $('#startDateDiv').datepicker({
            format: "yyyy-mm-dd"
        }).on('changeDate', function(e) {
            //alert($(this).val());
            $(this).datepicker('hide');
        });
        $('#endDateDiv').datepicker({
            format: "yyyy-mm-dd"
        }).on('changeDate', function(e) {
            $(this).datepicker('hide');
            $('#customDate').val($('#startDateDiv').val()+' to '+$(this).val());
        });
          $('#startDateDiv').css('display', 'block');
          $('#endDateDiv').css('display', 'block');
       }else{
           $('#startDateDiv').css('display', 'none');
           $('#endDateDiv').css('display', 'none');
        }
    });

//admin index link add on viewsurveylist page
    var url = window.location.href; 
    var arr = url.toString().split('?');
    var arr1 = arr[1].toString().split('&');
    var baseurl =url.toString().split('/');
    var SIteUrl= baseurl[0]+'/'+baseurl[1]+'/'+baseurl[2]+'/'+baseurl[3]+'/'+baseurl[4];
    $(".row .dataTables_paginate").before($("<div span='6' style='float:left;'><a        href='"+SIteUrl+"/buildingsurvey/survey/?"+arr1[0]+"&"+arr1[1]+"' style= 'font: bold'>Index</a></div>"));    
});


