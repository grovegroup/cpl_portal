<?php

class ControllerArea extends controller {

    public $arrMethods = array('add', 'view', 'edit', 'delete');
    public $arrPaths = array();
    public $sessionData = array();

    public function __construct() {

        parent::__construct();
        $this->arrPaths = config::req('paths');
        if (isset($this->arrPaths[2]) && !empty($this->arrPaths[2]) && in_array($this->arrPaths[2], $this->arrMethods)) {
            $method = $this->arrPaths[2];
            $this->$method();
        } else {
            $this->register();
        }
    }

    public function add() {
      //Admin Login
      if ($_SESSION['is_admin'] == 1) {
        $_SESSION['RegisterAddsuccess'] = '';
        $_SESSION['AreaAddsuccess'] = '';

        if (isset($_POST) && count($_POST) > 0) {
            $data = area::area_exists($_POST['name']);
            if ($data == 1) {
                $error = "Area is already Exists.";
            } else {
                    $name = $_POST['name'];
                    $response = area::insert($name);
                    if (count($response) > 0) {
                        $error = "Area Insert successfully";
                        $_SESSION['AreaAddsuccess'] = $error;
                        output::redirect(config::url("admin/area/view"));
                    }
            }
        }
        $body = make::tpl('admin/areaAdd')->assign(array(         
            'error' => $error
                )
        );
       } else {
            $msg = "You dont have Permission to access this page.";
            $body = make::tpl('admin/permissionPage')->assign(array(
                'successmsg' => $msg
            ));
        }
        //menu Add in Admin Login
        $menu = config::header();
        $tplRegistration = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'username' => $_SESSION['username'],
                    'homeUrl' => config::urls().'admin/register/userdetail/',
                    'menu' => $menu,
                    'meta_title' => 'CPLaccess | Admin panel | Create Area',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();
        output::as_html($tplRegistration);
    }

    public function view() {
      //Admin Login
      if ($_SESSION['is_admin'] == 1) {
        $_SESSION['RegisterAddsuccess'] = '';
        $users = area::areadetail();

        foreach ($users as $userArray) {
            $userTable.='<tr>';

            $userTable.= '<td>';
            $userTable.= $userArray['name'];

            $userTable.= '</td>';
            $userTable.= '<td>';
            $ONCLICK = 'onclick="return confirm(\'Are you sure you want to delete?\');"';
            $userTable.= '<a class="btn btn-primary" href="' . config::url() . 'admin/area/edit/' . $userArray['uid'] . '">Edit</a>&nbsp;&nbsp;'
                    . '<a  class="btn btn-primary" ' . $ONCLICK . '  href="' . config::url() . 'admin/area/delete/' . $userArray['uid'] . '/">Delete</a>';

            $userTable.= '</td>';
            $userTable.= '</tr>';
        }
        if ($_SESSION['AreaAddsuccess'] != '') {
            $msg = $_SESSION['AreaAddsuccess'];
        }
      
        $body = make::tpl('admin/areaView')->assign(array(
            'userTableData' => $userTable,
            'successmsg' => $msg
        ));

        $active = array(
            //'dashboard' => '',
           // 'wisdom' => '',
            'userdetail' => 'class="active"',
            //'partner' => '',
           // 'content' => '',
          //  'cms' => '',
        );

        } else {
            $msg = "You dont have Permission to access this page.";
            $body = make::tpl('admin/permissionPage')->assign(array(
                'successmsg' => $msg
            ));
        }
        //menu Add in Admin Login
        $menu = config::header();
        $tplSkeleton = make::tpl('admin/index')->assign(array($active))->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'username' => $_SESSION['username'],
                    'homeUrl' => config::urls().'admin/register/userdetail/',
                    'error_message' => $error_message,
                    'meta_title' => 'CPLaccess | Admin panel | Area',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();


        output::as_html($tplSkeleton);
    }

    public function edit() {
     //Admin Login
     if ($_SESSION['is_admin'] == 1) {
        $_SESSION['RegisterAddsuccess'] = '';
        $_SESSION['AreaAddsuccess'] = '';
        $paths = config::req('paths');

        if (isset($paths[3]) && is_numeric($paths[3]) && $paths[3] > 0) {

            $uid = $paths[3];
            $edits = area::editarea($paths[3]);

            foreach ($edits as $edit) {
                $id = $edit['uid'];
                $name = $edit['name'];
            }
            if (isset($_POST['updatearea'])) {

                $edit_id = $_POST['edit_id'];

                $name = $_POST['name'];
                $data = area::area_exists($_POST['name'],$edit_id);
                if ($data == 1) {
                    $error = "Area Name is already Exists.";
                } else {

                    $response = area::updatearea($name, $edit_id);
                    if ($response) {
                        $error = " Area Update successfully";
                        $_SESSION['AreaAddsuccess'] = $error;
                        output::redirect(config::url("admin/area/view"));
                    }
                }
            }
        }
        $body = make::tpl('admin/areaEdit')->assign(array(
            'uid' => $id,
            'name' => $name,
            'error_message' =>$error
        ));

        } else {
            $msg = "You dont have Permission to access this page.";
            $body = make::tpl('admin/permissionPage')->assign(array(
                'successmsg' => $msg
            ));
        }
        //menu Add in Admin Login
        $menu = config::header();
        $tplSkeleton = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'username' => $_SESSION['username'],
                    'error_message' => $error_message,
                    'homeUrl' => config::urls().'admin/register/userdetail/',
                    'meta_title' => 'CPLaccess | Admin panel | Edit Area',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();


        output::as_html($tplSkeleton);
    }

    public function delete() {
        //Admin Login
        if ($_SESSION['is_admin'] == 1) {
        $paths = config::req('paths');
        if (isset($paths[3]) && is_numeric($paths[3]) && $paths[3] > 0) {
            $uid = $paths[3];
            $response = area::deletearea($paths[3]);

            output::redirect(config::url('admin/area/view/'));
        }
      }
    }

}
