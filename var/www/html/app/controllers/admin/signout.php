<?php
class Controllersignout extends Controller {

	public function __construct() {
		
		parent::__construct();
		session_destroy();
		
		output::redirect( config::url().'login' );
	}
}
?>
