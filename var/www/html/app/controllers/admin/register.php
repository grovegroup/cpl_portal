<?php

class ControllerRegister extends controller {

    public $arrMethods = array('add', 'changepassword', 'userdetail', 'edit', 'delete');
    public $arrPaths = array();
    public $sessionData = array();

    public function __construct() {

        parent::__construct();
        $this->arrPaths = config::req('paths');
        if (isset($this->arrPaths[2]) && !empty($this->arrPaths[2]) && in_array($this->arrPaths[2], $this->arrMethods)) {
            $method = $this->arrPaths[2];
            $this->$method();
        } else {
            $this->register();
        }
    }

    public function add() {
     //Admin Login
        if ($_SESSION['is_admin'] == 1) {
        $_SESSION['AreaAddsuccess']='';
        $area=register::selectarea();
        $_SESSION['RegisterAddsuccess'] = '';
        if (isset($_POST) && count($_POST) > 0) {
            $data = register::email_exists($_POST['email']);
            if ($data == 1) {
                $error = "Email is already Exists.";
            } else {

                if ($_POST['password'] == $_POST['cpassword']) {

                    if ($_SESSION['is_facilityManager'] == 1) {
                            $_POST['is_facilityManager'] = '1';
                    }
                    $e = $_POST['email'];
                    $p = $_POST['password'];
                    $name = $_POST['name'];
                    $per = $_POST['permission'];
                    $area = implode(",",$_POST['area']);
                    $response = register::insert($name,$e, $p,$per,$area);
                    if (count($response) > 0) {
                        $error = "Register successfully";
                        $_SESSION['RegisterAddsuccess'] = $error;
                        output::redirect(config::url("admin/register/userdetail"));
                    } else {
                        $error = "Password should be match.";
                    }
                } else {
                    $error = "Password and onfirm Password not match.";
                }
            }
        }
        if($_SESSION['is_facilityManager'] == 1)
            {
                $permission = '<input type="radio" class="" name="permission" value="Read Web"> Read Web';
            }else{                
                $permission = '<input type="radio" class="" name="permission" value="Read App" > Read App
                                                                <br><input type="radio" class="" name="permission" value="Write App"> Write App
                                                                <br><input type="radio" class="" name="permission" value="Read Web"> Read Web
                                                                <br><input type="radio" class="" name="permission" value="Write Web"> Write Web   
                                                                <br><input type="radio" class="" name="permission" value="Read Web Read App"> Read Web Read App
                                                                <br><input type="radio" class="" name="permission" value="Read Web Write App"> Read Web Write App
                                                                <br><input type="radio" class="" name="permission" value="Write Web Read App"> Write Web Read App
                                                                <br><input type="radio" class="" name="permission" value="Write Web Write App"> Write Web Write App';
            }
        $body = make::tpl('admin/register')->assign(array(
            'area' => $area,
            'permission' => $permission,
            'error' => $error,
            'webpage' => $web_page
                )
        );
        } else {
            $msg = "You dont have Permission to access this page.";
            $body = make::tpl('admin/permissionPage')->assign(array(                
                'successmsg' => $msg
            ));
        }
        //menu Add in Admin Login
        $menu = config::header();
        $tplRegistration = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'homeUrl' => config::url().'admin/register/userdetail/',
                    'username' => $_SESSION['username'],
                   // 'name' => $this->sessionData['name'],
                  //  'url' => $this->sessionData['url'],
                    'script' => '<script type="text/javascript" src="' . config::urls() . 'assets/js/register.js"></script>',
                    'meta_title' => 'CPLaccess | Admin panel | Registration',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();


        output::as_html($tplRegistration);
    }
  
   public function userdetail() {
     //Admin Login
        if ($_SESSION['is_admin'] == 1) {
        $_SESSION['AreaAddsuccess']='';
        $users = register::getdetail();
        foreach ($users as $userArray) {
            $userTable.='<tr>';
            $userTable.= '<td>';
            $userTable.= $userArray['name'];
            $userTable.= '</td>';
            $userTable.= '<td>';
            $userTable.= $userArray['email'];
            $userTable.= '</td>';
            $userTable.= '<td>';
            $areaObj = new area();
            $getareaName = $areaObj->getareaName($userArray['area']);
             $userTable.= wordwrap($getareaName,15,"<br>\n",TRUE);
            $userTable.= '</td>';
           
            $userTable.= '<td>';
            $ONCLICK = 'onclick="return confirm(\'Are you sure you want to delete?\');"';
            $userTable.= '<a class="btn btn-primary" href="' . config::url() . 'admin/register/edit/' . $userArray['uid'] . '">Edit</a>&nbsp;&nbsp;'
                    . '<a  class="btn btn-primary" ' . $ONCLICK . '  href="' . config::url() . 'admin/register/delete/' . $userArray['uid'] . '/">Delete</a>';

            $userTable.= '</td>';
            $userTable.= '</tr>';
        }
        if ($_SESSION['RegisterAddsuccess'] != '') {
            $msg = $_SESSION['RegisterAddsuccess'];
        }
        $body = make::tpl('admin/userdetail')->assign(array(
            'userTableData' => $userTable,
            'successmsg' => $msg
        ));
        //$active = array(           
           // 'userdetail' => 'class="active"'
       // );
      } else {
            $msg = "You dont have Permission to access this page.";
            $body = make::tpl('admin/permissionPage')->assign(array(                
                'successmsg' => $msg
            ));
        }
        //menu Add in Admin Login
        $menu = config::header();
        $tplSkeleton = make::tpl('admin/index')->assign(array($active))->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'homeUrl' => config::url().'admin/register/userdetail/',
                    'username' => $_SESSION['username'],
                    'error_message' => $error_message,     
                    'meta_title' => 'CPLaccess | Admin panel | Registration',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();


        output::as_html($tplSkeleton);
    }

   public function edit() {
    //Admin Login
    if ($_SESSION['is_admin'] == 1) {
        $_SESSION['AreaAddsuccess']='';
        $_SESSION['RegisterAddsuccess'] = '';
        $paths = config::req('paths');
        if (isset($paths[3]) && is_numeric($paths[3]) && $paths[3] > 0) {

            $uid = $paths[3];
            $edits = register::edituser($paths[3]);
            $select=register::selectarea($edits[0]['area']);
            foreach ($edits as $edit) {
                $id = $edit['uid'];
                $emailid = $edit['email'];
                $name = $edit['name'];
                $password=$edit['password'];  
                $area=$edit['area'];
                 if($edit['permission'] == 'Read App')
                {
                    $readApp = 'checked = checked';
                }
                 if($edit['permission'] == 'Write App')
                {
                    $writeApp = 'checked = checked';
                }
                if($edit['permission'] == 'Read Web')
                {
                    $readWeb = 'checked = checked';
                }
                 if($edit['permission'] == 'Write Web')
                {
                    $WriteWeb = 'checked = checked';
                }
                if($edit['permission'] == 'Read Web Read App')
                {
                    $readWebApp = 'checked = checked';
                }
                if($edit['permission'] == 'Read Web Write App')
                {
                    $readWebwriteApp = 'checked = checked';
                }
                 
                 if($edit['permission'] == 'Write Web Read App')
                {
                    $writeWebreadApp = 'checked = checked';
                }
                if($edit['permission'] == 'Write Web Write App')
                {
                    $writeWebApp = 'checked = checked';
                }
            }
             if (isset($_POST) && count($_POST) > 0) {
                $edit_id = $_POST['edit_id'];
                $data = register::email_exists($_POST['email'],$edit_id);                
                if ($data == 1) {
                    $error = "Email is already Exists.";
                } else {
                     $_POST['area'] =implode(",",$_POST['area']);
                    $response = register::updateuser();
                    if ($response) {
                        $error = "Update successfully";
                        $_SESSION['RegisterAddsuccess'] = $error;
                        output::redirect(config::url("admin/register/userdetail"));
                    }
                }
            }
        }
        if($_SESSION['is_facilityManager'] == 1)
            {
                $permission = '<input type="radio" class="" name="permission" value="Read Web" '.$readWeb.'> Read Web';
            }else{                
                $permission = '<input type="radio" class="" name="permission" value="Read App" '.$readApp.'> Read App
                                                                <br><input type="radio" class="" name="permission" '.$writeApp.' value="Write App"> Write App
                                                                <br><input type="radio" class="" name="permission" '.$readWeb.' value="Read Web"> Read Web
                                                                <br><input type="radio" class="" name="permission" '.$WriteWeb.' value="Write Web"> Write Web   
                                                                <br><input type="radio" class="" name="permission" '.$readWebApp.' value="Read Web Read App"> Read Web Read App
                                                                <br><input type="radio" class="" name="permission" '.$readWebwriteApp.' value="Read Web Write App"> Read Web Write App
                                                                <br><input type="radio" class="" name="permission" '.$writeWebreadApp.' value="Write Web Read App"> Write Web Read App
                                                                <br><input type="radio" class="" name="permission" '.$writeWebApp.' value="Write Web Write App"> Write Web Write App';
            }
        $body = make::tpl('admin/edituserdetail')->assign(array(
            'uid' => $id,
            'area' => $select,
            'emailid' => $emailid,
            'permission' => $permission,
            'password'=>$password,
            'name'=>$name,
            'readApp' =>$readApp,
            'writeApp'=>$writeApp,
            'readWeb' =>$readWeb,
            'writeWeb' =>$WriteWeb,
            'readwebreadapp'=>$readWebApp,
            'readwebwriteapp'=>$readWebwriteApp,
            'writewebreadapp'=>$writeWebreadApp ,
            'writeWebApp'=>$writeWebApp,
            'error_message' =>$error
        ));
        } else {
            $msg = "You dont have Permission to access this page.";
            $body = make::tpl('admin/permissionPage')->assign(array(                
                'successmsg' => $msg
            ));
        }
        //menu Add in Admin Login
        $menu = config::header();
        $tplSkeleton = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'homeUrl' => config::url().'admin/register/userdetail/',
                    'username' => $_SESSION['username'],
                    'menu' => $menu,
                    'error_message' => $error_message,                                        
                    'meta_title' => 'CPLaccess| Admin panel | User Edit',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                    'script' => '<script type="text/javascript" src="' . config::urls() . 'assets/js/register.js"></script>'
                ))->get_content();


        output::as_html($tplSkeleton);
    }
    public function delete() {
      //Admin Login
      if ($_SESSION['is_admin'] == 1) {
       $_SESSION['RegisterAddsuccess'] ='';
        $paths = config::req('paths');
        if (isset($paths[3]) && is_numeric($paths[3]) && $paths[3] > 0) {
            $uid = $paths[3];
            $response = register::deleteuser($paths[3]);
            if ($response) {
                        $error = "User Deleted Successfully";
                        $_SESSION['RegisterAddsuccess'] = $error;
            }

            output::redirect(config::url('admin/register/userdetail/'));
        }
    }
  }

}