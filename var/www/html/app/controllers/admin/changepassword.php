<?php

class ControllerChangepassword extends Controller {

    public function __construct() {

        parent::__construct();

        $this->Changepassword();
    }

  /*  private function Changepasswordold() {

        $error_message = '';
        if (isset($_POST['signin'])) {
            //print_r($_POST);
            $arrErrors = Admin::try_login($_POST);
            if (isset($arrErrors['success'])) {
                output::redirect(config::url("admin/home"));
            } else {
                // if we come here, that means form has some error.
                $error_message.= '<div class="alert alert-error">';
                $error_message.= '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                $error_message.= '<h4>Authentication Error</h4>';
                $error_message.= implode("<br>", $arrErrors);
                $error_message.= '</div>';
            }
        }

        $remember = '';
        $email = '';
        $password = '';

        if (isset($_COOKIE['admin_remember']) && $_COOKIE['admin_remember'] != '' && isset($_COOKIE['admin_email']) && $_COOKIE['admin_email'] != '' && isset($_COOKIE['admin_password']) && $_COOKIE['admin_password'] != '') {
            $remember = $_COOKIE['admin_remember'] == 1 ? 'checked="checked"' : '';
            $email = $_COOKIE['admin_email'];
            $password = $_COOKIE['admin_password'];
        }

        $wisdomCount = Admin::getWisdomscount();
        $usersCount = Admin::getuserscount();
        $commentscount = Admin::getcommentscount();

        $body = make::tpl('admin/changepassword')->assign(array(
            'wisdomCount' => $wisdomCount,
            'wisdomTableData' => $wisdomTable
        ));
        $active = array(
            'dashboard' => '',
            'wisdom' => '',
            'user' => '',
            'partner' => '',
            'content' => '',
            'cms' => '',
        );
        $tplSkeleton = make::tpl('admin/index')->assign($active)->assign(array(
                    'body' => $body,
                    'wisdomCount' => $wisdomCount,
                    'usersCount' => $usersCount,
                    'commentsCount' => $commentscount,
                    'meta_title' => 'Cplaccess | Admin panel | Changepassword',
                    'meta_keywords' => 'Cplaccess',
                    'meta_description' => 'Cplaccess',
                ))->get_content();


        output::as_html($tplSkeleton);
    }*/

    public function changepassword() {
      
        if (isset($_POST['changepass'])) {
            $op = md5($_POST['opass']);

            $np = md5($_POST['npass']);

            $cp = md5($_POST['cpass']);

            $data = register::getpassword($op);
            if ($op == $data) {
                if ($np == $cp) {
                    $response = register::changepassword($np, $_SESSION['admin_userid']);

                    if (count($response) > 0) {

                        $error = "Password change successfully";
                    } else {
                        $error = "Please Try Again.";
                    }
                } else {
                    $error = "Password and Confirm Password not match";
                }
            } else {
                $error = "Old password not match";
            }
        }
        $body = make::tpl('admin/changepassword')->assign(array(
            'country' => $c,
            'error' => $error,
            'webpage' => $web_page
                )
        );

        $tplRegistration = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'name' => $this->sessionData['name'],
                    'url' => $this->sessionData['url'],
                    'script' => '<script type="text/javascript" src="' . config::url() . 'assets/js/ombelinko.js"></script>',
                    'meta_title' => 'CPLaccess | Admin panel | Changepassword',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();


        output::as_html($tplRegistration);
    }

}

?>
