<?php
class ControllerAdmin extends Controller {

	private $arrControllers = array(
		'login',
		'home',
		'signout',
		'wisdom',
		'user',
		'partner',
		'changepassword',
		'wisdomtag',
		'languagecontent',
		'cms',
		'language',
                'api',
                'register',  
                'area',
                'buildingsurvey',
                'maintenance',
                'facilityManager'
	);
	
	public function __construct() {

		parent::__construct();

		if (!Admin::is_logged_in()) {
			
			$controller='login';
			
		} else {
			
                       //if ($_SESSION['is_admin'] == 1) {
			// $controller = 'register';
                      // }else{
                           //$controller = 'buildingsurvey';
                       //}
                       $controller = 'home';
			if (isset($this->arrPaths[1]) && in_array($this->arrPaths[1], $this->arrControllers)) {
				$controller = $this->arrPaths[1];
			}
		}
               
		$this->load_controller('admin/' . $controller);
	}
}
?>
