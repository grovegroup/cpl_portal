<?php
 
class ControllerIndex extends Controller {
	public $sessionData = array();
	private $page = '';
	public function __construct() {
                                              
		parent::__construct();
		$this->sessionData = signin::getsessiondata();

		if( isset($this->arrPaths[1]) && !empty($this->arrPaths[1])) {
			$this->page = $this->arrPaths[1];
		}
             
		$this->home_page();
		
	}

	private function home_page() {
		if (isset($_POST['ajax']) == "true") {
			if($_POST['purpose']=='screensize')
			{
				if(!empty($_POST['width']) && !empty($_POST['height'])){
					
					if(	isset($_SESSION['screen_resolution_width']) && 
						!empty($_SESSION['screen_resolution_width']) && 
						$_POST['width'] == $_SESSION['screen_resolution_width']
					){
						echo "SUCCESS";
					}
					else{
						$_SESSION['screen_resolution_width'] = $_POST['width'];
						echo "FAIL";
					}
				}
				else{
					echo "FAIL";
				}
			}
			exit;
		}
		$language = $_SESSION['language'] == '' ? 'EN' : $_SESSION['language'];
		$web_page = $this->page == '' ? 'landing' : '';
		$webcontents = Language::getWebcontents($language);		
		//print_r($webcontents);
		$arrContinentWisdomCount = array();
		$WisdomData = array();
		// lets get continent data
		$arrRetriveFields = array('uid','slug');
		$arrContinents = Continent::get_list($arrRetriveFields);
		

		if(is_array($arrContinents) && count($arrContinents)) {
			foreach( $arrContinents as $arrContinent ) {
				// lets get wisdom counts by passing continent id
				$wisdom_count = Wisdom::get_wisdom_counts_by_continent($arrContinent['uid']);
				$WisdomData[$arrContinent['slug']] = $wisdom_count;
				/*$arrContinentWisdomCount[$arrContinent['slug'].'_count'] = $wisdom_count;
				if($wisdom_count <= 1)
				{
					$arrContinentWisdomCount[$arrContinent['slug'].'_bubbletext'] = 'wisdom';
				}
				else {
					$arrContinentWisdomCount[$arrContinent['slug'].'_bubbletext'] = 'wisdoms';
				}*/
			}
			arsort($WisdomData);
			$i = 0;
			$screen_width = $_SESSION['screen_resolution_width'];
			foreach($WisdomData as $key => $value)
			{
				$i++;
				//$wisdomText = $value <= 1 ? '' : 's';
				$wisdomText = 's';
				if($screen_width > 1700)
				{
					switch ($i) {
						case '1':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_2".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 7;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 28;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 49;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -33;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.1;
							break;
						case '2':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_1".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 6;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 26;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 42;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -32;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.2;
							break;
						case '3':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_4".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 5;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 22;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 38;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -28;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.3;
							break;
						case '4':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_3".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 4;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 20;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 32;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -25;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.4;
							break;
						case '5':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_5".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 4;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 18;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 28;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -22;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.5;
							break;
						case '6':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_6".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 4;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 16;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 25;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -20;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.6;
							break;
						default:
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_6".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 4;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 16;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 25;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -20;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.6;
							break;
					}
				}
				else if($screen_width >'1200')
				{
                                    
					switch ($i) {
						case '1':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_2".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 10;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 29;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 49;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -33;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.1;
							break;
						case '2':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_1".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 8;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 26;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 42;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -32;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.2;
							break;
						case '3':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_4".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 7;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 22;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 38;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -28;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.3;
							break;
						case '4':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_3".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 6;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 20;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 32;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -25;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.4;
							break;
						case '5':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_5".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 5.5;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 18;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 28;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -22;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.5;
							break;
						case '6':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_6".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 5;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 16;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 25;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -20;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.6;
							break;
						default:
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_6".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 5;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 16;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 25;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -20;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.6;
							break;
					}
				}
				else
				{
					switch ($i) {
						case '1':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_2".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 12;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 30;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 49;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -33;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.1;
							break;
						case '2':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_1".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 10;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 27;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 42;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -32;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.2;
							break;
						case '3':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_4".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 9;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 24;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 38;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -28;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.3;
							break;
						case '4':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_3".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 8;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 22;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 32;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -25;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.4;
							break;
						case '5':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_5".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 6.5;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 20;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 28;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -22;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.5;
							break;
						case '6':
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_6".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 6.5;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 18;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 25;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -20;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.6;
							break;
						default:
							$arrContinentWisdomCount[$key.'_imageURL'] = "name_loc_6".$wisdomText.".png";
							$arrContinentWisdomCount[$key.'_count'] = $value;
							$arrContinentWisdomCount[$key.'_percentWidth'] = 5;
							$arrContinentWisdomCount[$key.'_percentHeight'] = 16;
							$arrContinentWisdomCount[$key.'_labelFontSize'] = 25;
							$arrContinentWisdomCount[$key.'_labelShiftY'] = -20;
							$arrContinentWisdomCount[$key.'_selectedScale'] = 1.6;
							break;
					}
				}
			}
		}
				
		$body =  make::tpl('home')->assign($webcontents)->assign(
			array(
				// will show all continents as list option
				'continent_list' 	=> Continent::select_options( 0 ),				
				'webpage' => $web_page,
				'error_message'		=> $error_message,
			)
		);
		//------------------For getting Country wisdom bubbles---------------------//
		$continentMaplanguage = array();
		$ArrayMaplanguageData = array();
		if($language != 'EN')
		{
			$jsfile = strtolower($language);
			$map_languagefileData = file_get_contents(config::url()."assets/js/maplanguages/".$jsfile.".js");
			$MaplanguageData = explode("=", $map_languagefileData);
			$jsonMaplanguageData = $MaplanguageData[1];
			
			$ArrayMaplanguageData = json_decode($jsonMaplanguageData,true);
			
			$continentMaplanguage['con_africa']=$ArrayMaplanguageData['Africa'];
			$continentMaplanguage['con_asia']=$ArrayMaplanguageData['Asia'];
			$continentMaplanguage['con_australia']=$ArrayMaplanguageData['Australia'];
			$continentMaplanguage['con_europe']=$ArrayMaplanguageData['Europe'];
			$continentMaplanguage['con_north_america']=$ArrayMaplanguageData['North America'];
			$continentMaplanguage['con_south_america']=$ArrayMaplanguageData['South America'];
		}
		else {
			
			$continentMaplanguage['con_africa']='Africa';
			$continentMaplanguage['con_asia']='Asia';
			$continentMaplanguage['con_australia']='Australia';
			$continentMaplanguage['con_europe']='Europe';
			$continentMaplanguage['con_north_america']='North America';
			$continentMaplanguage['con_south_america']='South America';
		}
		$wisdomData = country::GetCountryWithWisdomCount();
		$countrybubblejson = '';
		foreach ($wisdomData as $wisdom)
		{
			if(!empty($ArrayMaplanguageData) && array_key_exists($wisdom['common_name'], $ArrayMaplanguageData))
			{
				$countryName = $ArrayMaplanguageData[$wisdom['common_name']];
			}
			else 
			{
				$countryName = $wisdom['common_name'];
			}
			//$TextofWisdom = $wisdom['total'] > 1 ? $wisdom['total'].' Wisdoms' : $wisdom['total'].' Wisdom';
			$TextofWisdom = $wisdom['total'].' Wisdoms';
			$flag = "<img src='".config::url()."assets/images/countryflag/".$wisdom['code'].".jpg' style='width: 60px; height: 50px;'/>";
			$countrybubblejson.= '{';
				//$countrybubblejson.= 'zoomLevel : 2,';
				//$countrybubblejson.= 'ContinentId : "'.$wisdom['continent_code'].'",';
				//$countrybubblejson.= 'useTargetsZoomValues : true,';
				$countrybubblejson.= 'languageKeyword: "'.$countryName.'",';
				$countrybubblejson.= 'id : "'.$wisdom['code'].'",';
				$countrybubblejson.= 'customData : "<div style=\'float:left;\'>'.$flag.'</div> <div style=\'float:right;padding-left:5px;padding-top:7px;text-align:left;\'><div class=\'amapCountryBubble\'>'.$countryName.'</div><div class=\'amapWisdomBubble\'>  '.$TextofWisdom.'</div></div>"';
			$countrybubblejson.= '},';
		}
		//------------------------------------------------------------------------//
		$tplSkeleton	= make::tpl('skeleton/index')->assign($continentMaplanguage)->assign($webcontents)->assign($arrContinentWisdomCount)->assign(array(
			'body' => $body, //make:tpl function calls the html or xhtml file
			'name' => $this->sessionData['name'],
			'url' => $this->sessionData['url'],
			'image' => $this->sessionData['image'],
			'DataofMap' => 'continentsDataProvider',
			'zoomCountry' => '',
			'Countrybubbles' => $countrybubblejson,
			'language' => Language::select_options( $language ),
			'script' => '<script type="text/javascript" src="'.config::url().'assets/js/ammap.js"></script><script type="text/javascript" src="'.config::url().'assets/js/worldLow.js"></script><script type="text/javascript" src="'.config::url().'assets/js/continentsLow.js"></script>',
			'meta_title'		=> 'CPLaccess|Admin-Panel|Home',
			'meta_keywords'		=> 'CPLaccess',
			'meta_description'	=> 'CPLaccess',
			'current_lang'=>$language,
		))->get_content();
		
		
		output::as_html($tplSkeleton);
		//include('/home/marq/public_html/rtmp2.php');

	}

	
}

?>