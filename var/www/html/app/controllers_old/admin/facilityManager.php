<?php

class ControllerFacilityManager extends controller {

    public $arrMethods = array('add','view', 'edit', 'delete');
    public $arrPaths = array();
    public $sessionData = array();

    public function __construct() {

        parent::__construct();
        $this->arrPaths = config::req('paths');        
        if (isset($this->arrPaths[2]) && !empty($this->arrPaths[2]) && in_array($this->arrPaths[2], $this->arrMethods)) {
            $method = $this->arrPaths[2];
            $this->$method();
        } else {
            $this->register();
        }
    }
    public function add() {
        //Admin Login
        if ($_SESSION['is_admin'] == 1) {                     
            if (isset($_POST) && count($_POST) > 0) {
                $data = facilitymanager::email_exists($_POST['email']);
                if ($data == 1) {
                    $error = "Email is already Exists.";
                } else {
                    if ($_POST['password'] == $_POST['cpassword']) {
                        $e = $_POST['email'];
                        $p = $_POST['password'];
                        $name = $_POST['name'];                                                
                        $response = facilitymanager::insert($name, $e, $p);
                        if (count($response) > 0) {
                            $error = "Facility Manager Add successfully";
                            $_SESSION['RegisterAddsuccess'] = $error;
                            output::redirect(config::url("admin/facilityManager/view"));
                        } else {
                            $error = "Password should be match.";
                        }
                    } else {
                        $error = "Password and confirm Password not match.";
                    }
                }
            }
            $body = make::tpl('admin/facilityManagerAdd')->assign(array(
                'area' => $area,
                'error' => $error
                    )
            );
        } else {
            $msg = "You dont have Permission to access this page.";
            $body = make::tpl('admin/permissionPage')->assign(array(                
                'successmsg' => $msg
            ));
        }
        //menu Add in Admin Login
        $menu = config::header();
        $tplRegistration = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'homeUrl' => config::url().'admin/register/userdetail/',
//                    'script' => '<script type="text/javascript" src="' . config::url() . 'assets/js/register.js"></script>',
                    'meta_title' => 'CPLaccess | Admin panel | Registration',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();
        output::as_html($tplRegistration);
    }

    public function view() {
        
        //Admin Login
        if ($_SESSION['is_admin'] == 1) {            
            $users = facilitymanager::getdetail();
            
            foreach ($users as $userArray) {
                $userTable.='<tr>';
                $userTable.= '<td>';
                $userTable.= $userArray['name'];
                $userTable.= '</td>';
                $userTable.= '<td>';
                $userTable.= $userArray['email'];
                $userTable.= '</td>';
                $userTable.= '<td>';
                $ONCLICK = 'onclick="return confirm(\'Are you sure you want to delete?\');"';
                $userTable.= '<a class="btn btn-primary" href="' . config::url() . 'admin/facilityManager/edit/' . $userArray['uid'] . '">Edit</a>&nbsp;&nbsp;'
                        . '<a  class="btn btn-primary" ' . $ONCLICK . '  href="' . config::url() . 'admin/facilityManager/delete/' . $userArray['uid'] . '/">Delete</a>';

                $userTable.= '</td>';
                $userTable.= '</tr>';
            }
            if ($_SESSION['RegisterAddsuccess'] != '') {
                $msg = $_SESSION['RegisterAddsuccess'];
            }
            $body = make::tpl('admin/facilityManager')->assign(array(
                'userTableData' => $userTable,
                'successmsg' => $msg
            ));
        } else {
            $msg = "You dont have Permission to access this page.";
            $body = make::tpl('admin/permissionPage')->assign(array(                
                'successmsg' => $msg
            ));
        }
        //menu Add in Admin Login
        $menu = config::header();
        $tplSkeleton = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'homeUrl' => config::url().'admin/register/userdetail/',
                    'error_message' => $error_message,
                    'meta_title' => 'CPLaccess | Admin panel | Registration',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();


        output::as_html($tplSkeleton);
    }
    
    //edit facility manager
    public function edit() {
        //Admin Login
        if ($_SESSION['is_admin'] == 1) {
            $_SESSION['AreaAddsuccess'] = '';
            $_SESSION['RegisterAddsuccess'] = '';
            $paths = config::req('paths');
            //print_r($paths);
            //exit;
            if (isset($paths[3]) && is_numeric($paths[3]) && $paths[3] > 0) {
                $uid = $paths[3];
                $edits = facilitymanager::edituser($paths[3]);
                
                foreach ($edits as $edit) {
                    $id = $edit['uid'];
                    $emailid = $edit['email'];
                    $name = $edit['name'];
                    $password = $edit['password'];
                }
                if (isset($_POST) && count($_POST) > 0) {
                    $edit_id = $_POST['id'];
                    $data = facilitymanager::email_exists($_POST['email'], $edit_id);
                    if ($data == 1) {
                        $error = "Email is already Exists.";
                    } else {
                        $_POST['area'] = implode(",", $_POST['area']);
                        $response = facilitymanager::updateuser();
                        if ($response) {
                            $error = "Update successfully";
                            $_SESSION['RegisterAddsuccess'] = $error;
                            output::redirect(config::url("admin/facilityManager/view"));
                        }
                    }
                }
            }
            $body = make::tpl('admin/facilityManagerEdit')->assign(array(
                'uid' => $id,               
                'email' => $emailid,
                'password' => $password,
                'name' => $name,                
                'error_message' => $error
            ));
        } else {
            $msg = "You dont have Permission to access this page.";
            $body = make::tpl('admin/permissionPage')->assign(array(                
                'successmsg' => $msg
            ));
        }
        //menu Add in Admin Login
        $menu = config::header();
        $tplSkeleton = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'homeUrl' => config::url().'admin/register/userdetail/',
                    'menu' => $menu,
                    'error_message' => $error_message,
                    'meta_title' => 'CPLaccess| Admin panel | User Edit',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                    'script' => '<script type="text/javascript" src="' . config::urls() . 'assets/js/register.js"></script>'
                ))->get_content();


        output::as_html($tplSkeleton);
    }

    public function delete() {
        //Admin Login
        if ($_SESSION['is_admin'] == 1) {
            $_SESSION['RegisterAddsuccess'] = '';
            $paths = config::req('paths');
            if (isset($paths[3]) && is_numeric($paths[3]) && $paths[3] > 0) {
                $uid = $paths[3];
                $response = facilitymanager::deleteuser($paths[3]);
                if ($response) {
                    $error = "User Deleted Successfully";
                    $_SESSION['RegisterAddsuccess'] = $error;
                }
                output::redirect(config::url('admin/facilityManager/view/'));
            }
        }
    }
}
