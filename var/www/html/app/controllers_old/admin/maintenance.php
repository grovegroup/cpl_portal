<?php
class ControllerMaintenance extends controller {

    public $arrMethods = array('maintenancelist','EditMaintenance');
    public $arrPaths = array();
    public $sessionData = array();

    public function __construct() {

        parent::__construct();
        $this->arrPaths = config::req('paths');
       // print_r($this->arrPaths);exit;
        if (isset($this->arrPaths[2]) && !empty($this->arrPaths[2]) && in_array($this->arrPaths[2], $this->arrMethods)) {
            $method = $this->arrPaths[2];
            $this->$method();
        } else {
            $this->register();
        }
    }
    
    public function maintenancelist()
    { 
           $users = maintenance::getmaintenancedetails();
           foreach ($users as $userArray) {
            $userTable.='<tr>';
            $userTable.='<td>';
            $userTable.= $userArray['username'];
            $userTable.='</td>';
            $userTable.= '<td>';
            
            //$count=strlen($userArray['answer']);
            $limit = 20;
            $content=$userArray['answer'];
            if (strlen($content)> $limit){
                $content=substr($content,0,$limit).'[...]';
           }
           $userTable.= $content;
            $userTable.= '</td>';
            $userTable.= '<td>';
            $userTable.= $userArray['question'];
            $userTable.= '</td>';
            $userTable.= '<td>';
            $limit = 20;
            $content=$userArray['building'];
            if (strlen($content)> $limit){
                $content=substr($content,0,$limit).'[...]';
           }
            $userTable.= $content;
            $userTable.= '</td>';
            $userTable.= '<td>';
            $userTable.= '<img src='.config::urls().$userArray['plan_img'] .' style="height:40px;width :40px;" >';
            $userTable.= '</td>';
            $userTable.= '<td>';
            $lastupdated = survey::getlastupdatedby($userArray['lastUpdatedBy'],$userArray['updatedByAdmin']);
            $userTable.= $lastupdated;            
            $userTable.= '</td>';
            $userTable.= '<td>';
            $userTable.= $userArray['lastUpdatedDate'];
            $userTable.= '</td>';
            $userTable.= '<td>';
            $userTable.= '<a class="btn btn-primary" href="' . config::url() . 'admin/maintenance/EditMaintenance/' . $userArray['uid'] . '">Edit</a>';
            $userTable.= '</td>';
            $userTable.= '</tr>';
        }
        $body = make::tpl('admin/viewmaintenancelist')->assign(array(
            'userTableData' => $userTable,
            'error_msg'=>$error,
            'msg'=>$msg,
         ));
        //menu Add in Admin Login
        $menu = config::header();
        if($_SESSION['is_admin'] == 1){
              $homeurl = config::url().'admin/register/userdetail/';
        }else{
            $homeurl = config::url().'admin/buildingsurvey/surveylist/';
        }
         
        $tplSkeleton = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'homeUrl' => $homeurl,
                    'error_message' => $error_message,
                    'remember' => $remember,
                    'email' => $email,
                    'password' => $password,
                    'webpage' => $web_page,
                    'meta_title' => 'CPLaccess | Admin panel | Signin',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();
        
        output::as_html($tplSkeleton);
       
    }
 public function EditMaintenance() {
        $_SESSION['MaintenanceAddsuccess'] = '';
        $paths = config::req('paths');
       // print_r($paths);exit;
        if (isset($paths[3]) && is_numeric($paths[3]) && $paths[3] > 0) {
            $uid = $paths[3];
            $edits = maintenance::editmaintenance($paths[3]);
            foreach ($edits as $edit) {
//print_r($edit);exit;
                $id = $edit['uid'];
                $photoimg = $edit['photo'];
                 if($edit['photo'] != ''){
                    $photoDownload = '<a href="'.config::urls() . $edit['photo'].'" download>Download</a>';
                }
                if($edit['plan_img'] != ''){
                    $planDownload = '<a href="'.config::urls() . $edit['plan_img'].'" download>Download</a>';
                }
                $UserImage = config::urls() . $edit['photo'];
                $planimg = $edit['plan_img'];
                $Image = config::urls() . $edit['plan_img'];
                $building = $edit['building'];
                if ($edit['item_type'] == 'Structural') {
                    $Structural = 'selected = selected';
                }
                if ($edit['item_type'] == 'Mechanical') {
                    $Mechanical = 'selected = selected';
                }
                if ($edit['item_type'] == 'Electrical') {
                    $Electrical = 'selected = selected';
                }
                if ($edit['item_type'] == 'Architectural') {
                    $Architectural = 'selected = selected';
                }
                if ($edit['item_type'] == 'Site/Civil') {
                    $SiteCivil = 'selected = selected';
                }

                $installdate = $edit['install_date'];
                if ($edit['conditions'] == 'End of Life') {
                    $EndofLife = 'selected = selected';
                }
                if ($edit['conditions'] == 'Satisfactory') {
                    $Satisfactory = 'selected = selected';
                }
                if ($edit['conditions'] == 'Good') {
                    $Good = 'selected = selected';
                }
                if ($edit['conditions'] == 'New') {
                    $New = 'selected = selected';
                }

                $usefulllife = $edit['userfull_life'];
                $healthsafety = $edit['health_safety'];
                $bcssection = $edit['bcs_section'];
                $make = $edit['make'];
                $model = $edit['model'];
                $units = $edit['units'];
                $quantity = $edit['quantity'];
                $cpu = $edit['cpu'];
                $estreplace = $edit['est_replacment'];
                $url = $edit['url'];
                $comments = $edit['comment'];
            }
            if (isset($_POST) && count($_POST) > 0) {
                //echo "<pre>";      print_r($_POST);print_r($_FILES);exit;
                $edit_id = $_POST['edit_id'];
                if (isset($_FILES['photo']) && !empty($_FILES['photo']['name']) && format::is_valid_image_upload_file($_FILES['photo']) === true) {

                    $photo = $_FILES['photo']['name'];
                    $image_path = config::sys('uploads') . $photo;
                    //$image_path = config::urls() .'uploads/'. $photo;
                    move_uploaded_file($_FILES['photo']['tmp_name'], $image_path);
                    $_POST['photo'] = 'uploads/' . $photo;
                } else if ($_POST['nphoto'] != '') {
                    $_POST['photo'] = $_POST['nphoto'];
                }

                if (isset($_FILES['plan_img']) && !empty($_FILES['plan_img']['name']) && format::is_valid_image_upload_file($_FILES['plan_img']) === true) {
                    $plan_img = $_FILES['plan_img']['name'];
                    $image_path = config::sys('uploads') . $plan_img;
                   // $image_path = config::urls() .'uploads/'. $plan_img;
                    move_uploaded_file($_FILES['plan_img']['tmp_name'], $image_path);
                    $_POST['plan_img'] = 'uploads/' . $plan_img;
                } else if ($_POST['nplan_img'] != '') {
                    $_POST['plan_img'] = $_POST['nplan_img'];
                }
                $response = maintenance::updatemaintenancelist();
                if ($response) {
                    $error = "Update successfully";
                    $_SESSION['MaintenanceAddsuccess'] = $error;
                    output::redirect(config::url("admin/maintenance/maintenancelist/"));
                }
            }
        }
        $submitButton = '';
        $writepos = strpos($_SESSION['permission'],"Write Web");
        
        if($_SESSION['is_admin'] == 1 || $writepos !== false)
        {
            $submitButton = '<button type="submit" name="update" class="btn btn-primary" id="">Update</button>';
        }
      
        $body = make::tpl('admin/EditMaintenance')->assign(array(
            'uid' => $id,
            'building' => $building,
            'itemtype' => $itemtype,
            'installdate' => $installdate,
            'condition' => $condition,
            'usefulllife' => $usefulllife,
            'healthsafety' => $healthsafety,
            'bcssection' => $bcssection,
            'make' => $make,
            'model' => $model,
            'units' => $units,
            'quantity' => $quantity,
            'cpu' => $cpu,
            'estreplace' => $estreplace,
            'url' => $url,
            'comments' => $comments,
            'Structural' => $Structural,
            'Mechanical' => $Mechanical,
            'Electrical' => $Electrical,
            'Architectural' => $Architectural,
            'SiteCivil' => $SiteCivil,
            'EndofLife' => $EndofLife,
            'Satisfactory' => $Satisfactory,
            'Good' => $Good,
            'New' => $New,
            'error_message' => $error,
            'photo' => $photoimg,
            'image' => $UserImage,
            'planimg' => $planimg,
            'Image' => $Image,
            'submitButton' => $submitButton,
            'imgdownload' =>$photoDownload,
            'plandownload' =>$planDownload,
        ));

        $menu = config::header();
        if($_SESSION['is_admin'] == 1){
              $homeurl = config::url().'admin/register/userdetail/';
        }else{
            $homeurl = config::url().'admin/buildingsurvey/surveylist/';
        }
        $tplSkeleton = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'homeUrl' => $homeurl,
                    'error_message' => $error_message,
                    'meta_title' => 'CPLaccess | Admin panel | Edit User',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                    'script' => '<script type="text/javascript" src="' . config::urls() . 'assets/js/register.js"></script>'
                ))->get_content();


        output::as_html($tplSkeleton);
    }

}
