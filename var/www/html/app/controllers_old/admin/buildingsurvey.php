<?php

class ControllerBuildingsurvey extends controller {

    public $arrMethods = array('surveylist','Viewsurvey','survey','exportPdfSurvey','answerUpdate');
    public $arrPaths = array();
    public $sessionData = array();

    public function __construct() {

        parent::__construct();
        $this->arrPaths = config::req('paths');
      //  print_r($this->arrPaths);exit;
        if (isset($this->arrPaths[2]) && !empty($this->arrPaths[2]) && in_array($this->arrPaths[2], $this->arrMethods)) {
            $method = $this->arrPaths[2];
            $this->$method();
        } else {

            //$this->register();
            $method = "surveylist";
            $this->$method();
        }
    }
    
    public function surveylist(){
       
        $users = survey::getSurveyListing();
        foreach ($users as $userArray) {
            $userTable.='<tr>';
            $userTable.= '<td>';
            $userTable.= $userArray['username'];
            $userTable.= '</td>';
            $userTable.= '<td>';
            $limit = 20;
            $content=$userArray['answer'];
            if (strlen($content)> $limit){
                $content=substr($content,0,$limit).'[...]';
            }
            $userTable.= $content;
            $userTable.= '</td>';
            $userTable.= '<td>';
            $userTable.= $userArray['created_date'];
            $userTable.= '</td>';
            $userTable.= '<td>';
            $lastupdated = survey::getlastupdatedby($userArray['lastUpdatedBy'],$userArray['updatedByAdmin']);
            $userTable.= $lastupdated;
            //$userTable.= $userArray['lastUpdatedBy'];
            $userTable.= '</td>';
            $userTable.= '<td>';
            $userTable.= $userArray['lastUpdatedDate'];
            $userTable.= '</td>';
            $userTable.= '<td>';
            $userTable.= '<a class="btn btn-primary" href="' . config::url() . 'admin/buildingsurvey/survey/?id=' . $userArray['userid'] . '&survey=' . $userArray['surveyid'] . '">View</a>';
            $userTable.= '</td>';
            $userTable.= '</tr>';
        }
        //date dropdown
        //yesterday
        $yesterday = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));
        //last week
        $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last sunday midnight", $previous_week);
        $end_week = strtotime("next saturday", $start_week);

        $start_week1 = date("Y-m-d", $start_week);
        $end_week1 = date("Y-m-d", $end_week);

        //last month
        $lastmonthStartDate = date("Y-n-j", strtotime("first day of previous month"));
        $lastmonthEndDate = date("Y-n-j", strtotime("last day of previous month"));

        //last 6 month
        $last6monthDate = date("Y-n-j", strtotime("-6 months")) . ' to ' . $lastmonthEndDate;
        //selected date
        $exploadDate = explode("to", $_GET['date']);
        $exploadDate1 = $exploadDate[0];
        $exploadDate2 = $exploadDate[1];

        if (strtotime($yesterday) == strtotime($exploadDate1)) {
            $yesterdaySelected = "selected = selected";
        } else {
            $yesterdaySelected = "";
        }
        if (strtotime($start_week1) == strtotime($exploadDate1) && strtotime($end_week1) == strtotime($exploadDate2)) {
            $lastweekSelected = "selected = selected";
        } else {
            $lastweekSelected = "";
        }

        if (strtotime($exploadDate[0]) == strtotime($lastmonthStartDate) && strtotime($exploadDate[1]) == strtotime($lastmonthEndDate)) {
            $lastmonthSelected = "selected = selected";
        } else {
            $lastmonthSelected = "";
        }
        if (strtotime($exploadDate[0]) == strtotime(date("Y-n-j", strtotime("-6 months"))) && strtotime($exploadDate[1]) == strtotime($lastmonthEndDate)) {
            $last6monthSelected = "selected = selected";
        } else {
            $last6monthSelected = "";
        }
        
        $body = make::tpl('admin/surveylist')->assign(array(
            'userTableData' => $userTable,
            'successmsg' => $msg,
            'editsuccess' => $editmsg,
            'yesterday' => $yesterday,
            'lastweek' => $start_week1 . ' to ' . $end_week1,
            'lastmonth' => $lastmonthStartDate . ' to ' . $lastmonthEndDate,
            'last6month' => $last6monthDate,
         
            'yesterdaySelected' => $yesterdaySelected,
            'lastweekSelected' => $lastweekSelected,
            'lastmonthSelected' => $lastmonthSelected,
            'last6monthSelected' => $last6monthSelected,
           ));
      
        //menu Add in Admin Login
        $menu = config::header();
        if($_SESSION['is_admin'] == 1){
              $homeurl = config::url().'admin/register/userdetail/';
        }else{
            $homeurl = config::url().'admin/buildingsurvey/surveylist/';
        }
         
        $tplSkeleton = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'homeUrl' => $homeurl,
                    'error_message' => $error_message,
                    'script' => '<script type="text/javascript" src="' . config::urls() . 'assets/js/surveylist.js"></script>',
                    'meta_title' => 'CPLaccess | Admin panel | Surveylist',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();
        output::as_html($tplSkeleton);
    }

   public function Viewsurvey() {
        $users = survey::viewsurveylist();
//$i=0;
        foreach ($users as $userArray) {
//$i++;
            $userTable.='<tr>';
            $userTable.= '<td>';
            $userTable.=$userArray['qno'];
            $userTable.= '</td>';
            $userTable.= '<td>';
            $limit = 50;
            $content=$userArray['answer'];
            if (strlen($content)> $limit){
                $content=substr($content,0,$limit).'[...]';
           }
             $userTable.= '<b>' . $userArray['question'] . '</b>' . '<br>'
                    . '<span class="edit_answer_show" id="lableAnswer_' . $userArray['uid'] . '" >' . $content . '</span>';
            $userTable.= '<span style="display:none" class="edit_answer" id="inputAnswer_' . $userArray['uid'] . '" data-uid="{{ uid }}">';
                if($userArray['qno']==1){
                   $userTable.= area::getAreaList($userArray['answer']);
                }else{
                 $userTable.= '<input type="text" class="editanswer_' . $userArray['uid'] . '" data-uid="' . $userArray['uid'] . '" name="answer" value="' . $userArray['answer'] . '">';
                }
            $submitButton = '';
            $writepos = strpos($_SESSION['permission'],"Write Web");
            if ($_SESSION['is_admin'] == 1 || $writepos !== false) {
                $userTables = '<input type = "button" class = "btn btn-primary SaveAnswer" data-uid = "' . $userArray['uid'] . '" name = "save" value = "Save">';
              $userTable.='<input type="button" class="btn btn-primary SaveAnswer" data-uid="' . $userArray['uid'] . '" data-qno="' . $userArray['qno'] . '" name="save" value="Save">';   
            }

            $userTable.= '</span>';
            //display option
            $Option = survey::getsurveyOptionlist($userArray['qno'],$userArray['userid'],$userArray['surveyid']);
            if ($Option != '') {            
                $userTable .= $Option;
            }
            $userTable.= '</td>';
            $userTable.= '<td>';

             $userTable.= '<a class="btn btn-primary editSurvey" href="#" data-uid="' . $userArray['uid'] . '" data-qno="' . $userArray['qno'] . '">Edit</a>';
            $userTable.= '</td>';
            $userTable.= '</tr>';
        }
        $body = make::tpl('admin/Viewsurvey')->assign(array(
            'id' => $_REQUEST['id'],           
            'survey' => $_REQUEST['survey'],
            'userTableData' => $userTable,
            'successmsg' => $msg,
            'editsuccess' => $editmsg
        ));
        //menu Add in Admin Login
        $menu = config::header();
        if($_SESSION['is_admin'] == 1){
              $homeurl = config::url().'admin/register/userdetail/';
        }else{
            $homeurl = config::url().'admin/buildingsurvey/surveylist/';
        }
        $tplSkeleton = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'error_message' => $error_message,
                    'homeUrl' => $homeurl,
                    'scriptlist' => '<script type="text/javascript" src="' . config::urls() . 'assets/js/surveylist.js"></script>',
                    'script' => '<script type="text/javascript" src="' . config::urls() . 'assets/js/survey.js"></script>',
                    'meta_title' => 'CPLaccess | Admin panel | Surveylist',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();
        output::as_html($tplSkeleton);
    }
    
    
    public function answerUpdate()
    {
        survey::UpdateAnswer();
        echo $_REQUEST["answer"];
      }
        
    public function survey()
    {
             $userTable = '';
           $users = survey::viewsurveyDetailForPDF();
            $body = make::tpl('admin/survey')->assign(array(
            'userTableData' => $userTable,
            'userID' => $_REQUEST["userId"],
            'survey' => $_REQUEST["survey"],
            'id' => $_REQUEST['id'],
          ));
        //menu Add in Admin Login
        $menu = config::header();
        if($_SESSION['is_admin'] == 1){
              $homeurl = config::url().'admin/register/userdetail/';
        }else{
            $homeurl = config::url().'admin/buildingsurvey/surveylist/';
        }
         
        $tplSkeleton = make::tpl('admin/index')->assign(array(
                    'body' => $body,
                    'menu' => $menu,
                    'homeUrl' => $homeurl,
                    'error_message' => $error_message,
                    'meta_title' => 'CPLaccess | Admin panel | Survey',
                    'script' => '<script type="text/javascript" src="' . config::url() . 'assets/js/surveylist.js"></script>',
                    'meta_keywords' => 'CPLaccess',
                    'meta_description' => 'CPLaccess',
                ))->get_content();
        output::as_html($tplSkeleton);
    }
public function exportPdfSurvey() {
        

        $userTable = '';
        $users = survey::viewsurveyDetailForPDF();
        
        
        foreach ($users as $userArray) {
         if ($userArray['question']) {  

            $userTable.='<tr>';


            //if (!is_numeric($userArray['qno'])) {
            if ($userArray['parentid'] > 0) {

//                $userTable.= '<td></td>';
//                $userTable.= '<td class="tdque">';
//                $userTable.= "&nbsp;&nbsp;" . $userArray['qno'] . '.&nbsp;&nbsp;';
//
//                $userTable.= $userArray['question'];
//                $userTable.= '</td>';
                
                
                $userTable.= '<td></td>';
                $userTable.= '<td class="tdque">';

                $userTable.= '<table>';
                
                $userTable.= '<tr>';
                
                $userTable.= '<td>';

                $userTable.= "&nbsp;&nbsp;" . $userArray['qno'] . '.&nbsp;&nbsp;';
                
                $userTable.= '</td>';

                $userTable.= '<td class="tdque">';
                
                $userTable.= $userArray['question'];
                $userTable.= '</td>';
                
                $userTable.= '</tr>';
                $userTable.= '</table>';

                $userTable.= '</td>';
//                        
                        
            } else {


                $userTable.= '<td>';
                $userTable.= $userArray['qno'] . '.&nbsp;&nbsp;';
                $userTable.= '</td>';
                
                $userTable.= '<td class="tdque">';
                $userTable.= $userArray['question'];
                $userTable.= '</td>';
                
                
            }
            $userTable.= '<td align="right" class="tdanswer">';
                        $userTable.= $userArray['answer'];
            $userTable.= '</td>';
            
            $userTable.= '</tr>';
            
            
//            for line height.start
            $userTable.= '<tr>';
            $userTable.= '<td>&nbsp;';
            $userTable.= '</td>';
            $userTable.= '</tr>';
//            for line height.end
            
            
            if ($userArray['qno'] == 10) {
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 1 - Building Age,Gross Square Footage and Maitenance Staff</b></td></tr>';
                $userTable.= '</table>';

                $userTable.= '<br/><br/>';
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
            }

            if ($userArray['qno'] == 25) {
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 2 - Building Site & Utilities</b></td></tr>';
                $userTable.= '</table>';

                $userTable.= '<br/><br/>';
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                
            }
            if ($userArray['qno'] == '43g') {
                
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 3 - Building Interior</b></td></tr>';
                $userTable.= '</table>';
                
                $userTable.= '<br/><br/>';
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                //break;
            }
            if ($userArray['qno'] == "59g") {
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 4 - Building Envelope </b></td></tr>';
                $userTable.= '</table>';
                
                $userTable.= '<br/><br/>';
                
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
               
            }
            if ($userArray['qno'] == "69g") {
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 5 - Plumbing & Michanical Systems</b></td></tr>';
                $userTable.= '</table>';
                
                $userTable.= '<br/><br/>';
                
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                 
            }
            if ($userArray['qno'] == "81f") {
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 6 - Fife Safty Systems</b></td></tr>';
                $userTable.= '</table>';
                
                $userTable.= '<br/><br/>';
                
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                 
            }
            if ($userArray['qno'] == "86f") {
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 7 - Accessibility & Environment</b></td></tr>';
                $userTable.= '</table>';
                
                $userTable.= '<br/><br/>';
                
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                 
            }
             if ($userArray['qno'] == "96b") {
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 8 - Indoor Air Quality </b></td></tr>';
                $userTable.= '</table>';
                
                $userTable.= '<br/><br/>';
                
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                
            }
            if ($userArray['qno'] == "103d") {
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 9 - Space Adequacy </b></td></tr>';
                $userTable.= '</table>';
                
                $userTable.= '<br/><br/>';
                
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                
            }
            if ($userArray['qno'] == "108") {
                $userTable.= '</table>';
                $userTable.= '<br/><br/>';

                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                $userTable.= '<tr><td><b class="subtitle">Chapter 10 - Space Adequacy </b></td></tr>';
                $userTable.= '</table>';
                
                $userTable.= '<br/><br/>';
                
                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
                
            }
//            if ($userArray['qno'] == "115") {
//                $userTable.= '</table>';
//                $userTable.= '<br/><br/>';
//
//                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
//                $userTable.= '<tr><td><b class="subtitle">Chapter 10 - Space Adequacy </b></td></tr>';
//                $userTable.= '</table>';
//                
//                $userTable.= '<br/><br/>';
//                
//                $userTable.= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
//                
//            }
         }
        }


        $body = make::tpl('admin/survey_pdf')->assign(array(
            'userTableData' => $userTable,
            'successmsg' => $msg,
            'userID' => $_REQUEST["userId"],
            'survey' => $_REQUEST["survey"],
            'editsuccess' => $editmsg
        ));
        ob_start();
        $content = $body->get_content();
//        print_r($content);
//   exit;
        try {
            $html2pdf = new HTML2PDF('P', 'A4', 'en', array(10, 10, 10, 10));

            $html2pdf->setTestTdInOnePage(false);
           // $html2pdf->setDefaultFont('Arial');
            //$html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output('survey_' . $_REQUEST["userId"] . '.pdf');
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }
}
