<?php

/**
 * api.php
 */
class Api extends Controller {

    private $modules = array(
        'Buildingsurvey' => array(
            'Add'
        ),           
        'user'=>array(
            'login'
        ),
        'survey' => array(
            'list',
            'detail'
        ),
        'Maintenance' => array(
            'sync',
            'list',
            'GetList'
        )
    );

    // constructor
    public function __construct() {
        parent::__construct();
        $this->page();
    }

    // default page action
    protected function page() {

        $paths = config::req('paths');

        if (isset($paths[0]) && $paths[0] == 'api') {

            if (isset($paths[1]) && $paths[1] != "" && array_key_exists($paths[1], $this->modules)) {
                if (isset($paths[2]) && $paths[2] != "" && in_array($paths[2], $this->modules[$paths[1]])) {
                    // parse the request
                    $method = "api_" . $paths[1] . "_" . $paths[2];
                    $this->$method();
                } else {
                    echo json_encode(array("Please Provide Valid Action"));
                    exit;
                }
            } else {
                echo json_encode(array("Please Provide Valid Module"));
                exit;
            }
        }
    }

    private function api_Buildingsurvey_Addold() {
            
  // mail('bhavisha.s@quantumtechnolabs.com','survey Request ',print_r($_REQUEST,true));
     
//mail('bevin.p@quantumtechnolabs.com,','survey add',print_r($_REQUEST,true));
        if (!isset($_REQUEST['userid']) || $_REQUEST['userid'] == "") {
            $data = "User id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }        
        if (!isset($_REQUEST['date']) || $_REQUEST['date'] == "") {
            $data = "Date is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        //get last survey id
        $objsurvay = new survey();
     
         //get last survey id
         $servey = $objsurvay->getsurveyid($_REQUEST['userid']);
  
        if (count($_REQUEST['survey']) > 0) { 
           $i = 1; 
           $surveyArray = json_decode($_REQUEST['survey'], true);    
     
            foreach ($surveyArray as $key=>$value) {  
    
                if ($_REQUEST['surveyID'] != '') {
                    $inseredId = $objsurvay->UpdateSurvay($value['discription'], $value['question'],$value['answer'], $value['type'], $_REQUEST['userid'], $i, $_REQUEST['surveyID'], $_REQUEST['date']);
                } else {               
                     $inseredId = $objsurvay->AddSurvay($value['discription'],$value['question'],$value['answer'], $value['type'], $_REQUEST['userid'], $i,$servey,$_REQUEST['date']);
                }
                if ($inseredId < 0) {
                    $response['statuscode'] = 401;
                    $response['message'] = "Please try again.";
                    echo json_encode($response);
                    exit;
                }else{
                if (count($value['subsurvey']) > 0) {
                    $j = 97;
                    foreach ($value['subsurvey'] as $subsurvey) {
                        if (count($subsurvey) > 0) {
                            
                            $letter = chr($j);    
                            if ($_REQUEST['surveyID'] != '') {
                                    $subdata = $objsurvay->UpdateSurvay($subsurvey['discription'], $subsurvey['question'], $subsurvey['answer'], $subsurvey['type'], $_REQUEST['userid'], $i . $letter, $_REQUEST['surveyID'], $_REQUEST['date'], $i);
                                } else {                        
                            $subdata = $objsurvay->AddSurvay($subsurvey['discription'],$subsurvey['question'], $subsurvey['answer'], $subsurvey['type'], $_REQUEST['userid'], $i.$letter,$servey ,$_REQUEST['date'],$i);
                             }
                            if ($subdata < 0) {
                                $response['statuscode'] = 401;
                                $response['message'] = "Please try again.";
                                echo json_encode($response);
                                exit;
                            }
                        }
                        $j++;
                    }
                }
              }    $i++;       
            }
            if ($_REQUEST['surveyID'] != '') {
              $subdata = array('surveyid'=>$_REQUEST['surveyID']);
            }else{
              $subdata = $objsurvay->getlastSurveyId();
            }

            $response['statuscode'] = 200;
            $response['message'] = 'Building Survey Insert Successfully.';
            $response['data'] = $subdata;

            echo json_encode($response);
            exit;
        }
    }

    
    private function api_Buildingsurvey_Add() {
            
  //mail('bhavisha.s@quantumtechnolabs.com','survey Request ',print_r($_REQUEST,true));
        //mail('bhavisha.s@quantumtechnolabs.com','survey Request',print_r($_REQUEST,true));

         mail('gaurav.p@quantumtechnolabs.com','survey 54 server Request',print_r($_REQUEST,true));
         mail('vivek.ta@latitudetechnolabs.com','survey 54 server  Request',print_r($_REQUEST,true));
        

        if (!isset($_REQUEST['userid']) || $_REQUEST['userid'] == "") {
            $data = "User id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }        
        if (!isset($_REQUEST['date']) || $_REQUEST['date'] == "") {
            $data = "Date is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        //get last survey id
        $objsurvay = new survey();
     
         //get last survey id
         $servey = $objsurvay->getsurveyid($_REQUEST['userid']);
  
        if (count($_REQUEST['survey']) > 0) { 
           $i = 1; 
           $surveyArray = json_decode($_REQUEST['survey'], true);    
     
            foreach ($surveyArray as $key=>$value) {  
    
                if ($_REQUEST['surveyID'] != '') {
                    $inseredId = $objsurvay->UpdateSurvay($value['discription'], $value['question'],$value['answer'], $value['type'], $_REQUEST['userid'], $i, $_REQUEST['surveyID'], $_REQUEST['date']);
                } else {               
                     $inseredId = $objsurvay->AddSurvay($value['discription'],$value['question'],$value['answer'], $value['type'], $_REQUEST['userid'], $i,$servey,$_REQUEST['date']);
                }
                if ($inseredId < 0) {
                    $response['statuscode'] = 401;
                    $response['message'] = "Please try again.";
                    echo json_encode($response);
                    exit;
                }else{
                if (count($value['subsurvey']) > 0) {
                    $j = 97;
                    foreach ($value['subsurvey'] as $subsurvey) {
                        if (count($subsurvey) > 0) {
                            
                            $letter = chr($j);    
                            if ($_REQUEST['surveyID'] != '') {
                                    $subdata = $objsurvay->UpdateSurvay($subsurvey['discription'], $subsurvey['question'], $subsurvey['answer'], $subsurvey['type'], $_REQUEST['userid'], $i . $letter, $_REQUEST['surveyID'], $_REQUEST['date'], $i);
                                } else {                        
                            $subdata = $objsurvay->AddSurvay($subsurvey['discription'],$subsurvey['question'], $subsurvey['answer'], $subsurvey['type'], $_REQUEST['userid'], $i.$letter,$servey ,$_REQUEST['date'],$i);
                             }
                            if ($subdata < 0) {
                                $response['statuscode'] = 401;
                                $response['message'] = "Please try again.";
                                echo json_encode($response);
                                exit;
                            }else {
                                    //add subquestion start
                                    if (count($subsurvey['subQuestion']) > 0) {
                                        $a = 97;

                                        foreach ($subsurvey['subQuestion'] as $subquestion) {
                                            $letters1 = chr($a);
                                            if ($_REQUEST['surveyID'] != '') {
 //mail('bhavisha.s@quantumtechnolabs.com','survey subsurvey',print_r($i.$letter.$letters1,true));
                                                $subdatas = $objsurvay->UpdateSurvay($subquestion['discription'], $subquestion['question'], $subquestion['answer'], $subquestion['type'], $_REQUEST['userid'], $i.$letter.$letters1, $_REQUEST['surveyID'], $_REQUEST['date'], $i.$letter);
                                            } else {
                                                $subdatas = $objsurvay->AddSurvay($subquestion['discription'], $subquestion['question'], $subquestion['answer'], $subquestion['type'], $_REQUEST['userid'],$i.$letter.$letters1, $servey, $_REQUEST['date'],$i.$letter);
                                            }
                                        $a++;
                                        }
                                       
                                    }
                                    //end subquestion 
                                }
                        }
                        $j++;
                    }
                }
              }    $i++;       
            }
            if ($_REQUEST['surveyID'] != '') {
              $subdata = array('surveyid'=>$_REQUEST['surveyID']);
            }else{
              $subdata = $objsurvay->getlastSurveyId();
            }

            $response['statuscode'] = 200;
            $response['message'] = 'Building Survey Insert Successfully.';
            $response['data'] = $subdata;
//mail('bhavisha.s@quantumtechnolabs.com','survey response ',print_r($response,true));
//mail('gaurav.p@quantumtechnolabs.com','survey response ',print_r($response,true));

            echo json_encode($response);
            exit;
        }
    }

 private function api_user_login() {

        if (!isset($_REQUEST['email']) || $_REQUEST['email'] == "") {
            $data = "Email id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        if (!isset($_REQUEST['password']) || $_REQUEST['password'] == "") {
            $data = "Password is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        $objregister = new register();
        $inseredId = $objregister->is_register($_REQUEST['email'], $_REQUEST['password']);        
        if ($inseredId > 0) {
            $response['statuscode'] = 200;
            $response['message'] = "Login successful";
            $response['data'] = $inseredId;
            echo json_encode($response);
            exit;
        }else{
            $response['statuscode'] = 401;
            $response['message'] = "Email and Password Invalid.";
            echo json_encode($response);
            exit;
        }        
    }
  private function api_survey_list() {

        if (!isset($_REQUEST['user_id']) || $_REQUEST['user_id'] == "") {
            $data = "User id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        $objsurvay = new survey();
        $servey = $objsurvay->getsurveylist();
        if(!empty($servey)){
        foreach ($servey as $data) {
            $serveylist = $objsurvay->getsurveyDetail($data['userid'], $data['surveyid']);
            $serveydetail[] = $serveylist;
        }
          $response['statuscode'] = 200;
          $response['message'] = 'Survey List';
          $response['data'] = $serveydetail;
          echo json_encode($response);
          exit;   
        }else{
          $response['statuscode'] = 401;
          $response['message'] = 'Record Not found.';
          echo json_encode($response);
          exit;   
        } 
          
    }
    private function api_survey_detail() {
        if (!isset($_REQUEST['userid']) || $_REQUEST['userid'] == "") {
            $data = "User id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        if (!isset($_REQUEST['surveyid']) || $_REQUEST['surveyid'] == "") {
            $data = "survey id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }

        $objsurvay = new survey();
        $servey = $objsurvay->getDetailSurvey($_REQUEST['userid'], $_REQUEST['surveyid']);
        if (count($servey) > 0) {
            $response['statuscode'] = 200;
            $response['message'] = 'Survey List';
            $response['data'] = $servey;
            echo json_encode($response);
            exit;
        } else {
            $response['statuscode'] = 401;
            $response['message'] = 'Record Not found';           
            echo json_encode($response);
            exit;
        }
    }

    private function api_Maintenance_sync() {
//mail('bhavisha.s@quantumtechnolabs.com','maintenance add',print_r($_REQUEST,true));
        
        if (!isset($_REQUEST['userid']) || $_REQUEST['userid'] == "") {
            $data = "User id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        if (!isset($_REQUEST['surveyid']) || $_REQUEST['surveyid'] == "") {
            $data = "Survey id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
         if (!isset($_REQUEST['qno']) || $_REQUEST['qno'] == "") {
            $data = "Question Number is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        //check user is exist or not
        $Objregister = new register();
        $userexits = $Objregister->userIsExists($_REQUEST['userid']);
     
        if (count($userexits) == '') {
            $data = "User is not Exist.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }

        $objmaintenance = new maintenance();        
        if($_REQUEST['maintenanceID'] != '')
        {
            $inseredId = $objmaintenance->updatemaintenance();
        }else{
            $inseredId = $objmaintenance->Addmaintenance();
        }
        if ($inseredId > 0) {
            $data = $objmaintenance->getplandetails($inseredId);
            $response['statuscode'] = 200;
            $response['message'] = "Plan Add Successfully.";
            $response['data'] = $data;
            echo json_encode($response);
            exit;
        } else {
            $response['statuscode'] = 401;
            $response['message'] = "Please Try Again.";
            echo json_encode($response);
            exit;
        }
    }
  private function api_Maintenance_list() {
        
        if (!isset($_REQUEST['userid']) || $_REQUEST['userid'] == "") {
            $data = "User id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        if (!isset($_REQUEST['surveyid']) || $_REQUEST['surveyid'] == "") {
            $data = "Survey id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        if (!isset($_REQUEST['qno']) || $_REQUEST['qno'] == "") {
            $data = "Question Number is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        //check user is exist or not
        $Objregister = new register();        
        $userexits = $Objregister->userIsExists($_REQUEST['userid']);
     
        if (count($userexits) == '') {
            $data = "User is not Exist.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }        
        $objmaintenance = new maintenance();        
        $getlist = $objmaintenance->getMaintenancelist();
        
        if ($getlist > 0) {            
            $response['statuscode'] = 200;
            $response['message'] = "Maintenance List.";
            $response['data'] = $getlist;
            echo json_encode($response);
            exit;
        } else {
            $response['statuscode'] = 401;
            $response['message'] = "Record Not Found.";
            echo json_encode($response);
            exit;
        }
    }
     private function api_Maintenance_GetList() {
        if (!isset($_REQUEST['user_id']) || $_REQUEST['user_id'] == "") {
            $data = "User id is Required.";
            $response = array('statuscode' => '401', 'message' => $data);
            echo json_encode($response);
            exit;
        }
        $objmaintenance = new maintenance();        
        $getlist = $objmaintenance->GetList();
        
        if ($getlist > 0) {            
            $response['statuscode'] = 200;
            $response['message'] = "Maintenance List.";
            $response['data'] = $getlist;
            echo json_encode($response);
            exit;
        } else {
            $response['statuscode'] = 401;
            $response['message'] = "Record Not Found.";
            echo json_encode($response);
            exit;
        }
    }
}
?>
