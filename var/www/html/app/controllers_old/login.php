<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class ControllerLogin extends Controller {

	public function __construct() {

		parent::__construct();
		
		$this->login();
		
	}
     
        
        private function login() {
            
		$error_message = '';
		if (isset($_POST['signin']))
		{
			//print_r($_POST);
			$arrErrors = Admin::try_login( $_POST );
			if(isset($arrErrors['success'])) {                                              
                                if($_SESSION['is_admin'] == 0)
                                {
                                    //register User Login
                                    output::redirect(config::url("admin/buildingsurvey/surveylist/"));
                                }else{   
                                     //Admin Login       
				     output::redirect(config::url("admin/register/userdetail/"));
                                }
			}
			else
			{
				// if we come here, that means form has some error.
				$error_message.= '<div class="alert alert-error">';
				$error_message.= '<button type="button" class="close" data-dismiss="alert">&times;</button>';
				$error_message.= '<h4>Authentication Error</h4>';
				$error_message.= implode("<br>",$arrErrors);
				$error_message.= '</div>';
			}
		}
		
		$remember = '';
		$email = '';
		$password = '';
		
		if(isset($_COOKIE['admin_remember']) && $_COOKIE['admin_remember'] != '' && isset($_COOKIE['admin_email']) && $_COOKIE['admin_email'] != '' && isset($_COOKIE['admin_password']) && $_COOKIE['admin_password'] != '')
		{
			$remember = $_COOKIE['admin_remember']==1 ? 'checked="checked"' : '';
			$email = $_COOKIE['admin_email'];
			$password = $_COOKIE['admin_password'];
		}
		
		$tplSkeleton	= make::tpl('admin/login')->assign(array(
			'error_message'  => $error_message,
			'remember' => $remember,
			'email' => $email,
			'password' => $password,
			'webpage' => $web_page,
			'meta_title'		=> 'CPLaccess | Admin panel | Signin',
			'meta_keywords'		=> 'CPLaccess',
			'meta_description'	=> 'CPLaccess',
		))->get_content();
		
		
		output::as_html($tplSkeleton);
	}
}
?>
