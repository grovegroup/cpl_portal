<?php

class maintenance{
    
     public function __construct() {
        
    }
//admin view maintenancelist
   public function getmaintenancedetails() {
        $response = array();
        $query = "select m.uid, m.surveyid,m.qno,m.building,m.plan_img,b.question as question,b.answer as answer,"
                . "(select name from register as r where r.uid=m.userid) as username,m.lastUpdatedBy,m.updatedByAdmin,m.lastUpdatedDate  "
                . " from maintenance_sync as m,building_survey as b where m.surveyid = b.surveyid and m.qno = b.qno  group by m.surveyid  order by m.userid";
       $row = db::query($query);
       
        if ($row && isset($row->num_rows) && $row->num_rows) {
            while ($data = mysqli_fetch_assoc($row)) {

                $Sdata[] = $data;
            }
        }
        return $Sdata;
    }
//admin Edit Maintenancelist
 public function editmaintenance($id)
    {
        $query="select * from maintenance_sync where uid = '".$id."'";
         $data = db::query($query);
         
           while ($row = mysqli_fetch_assoc($data)) {
           $url = config::sys('site_url');
           $response[] = $row;
        }
        return $response;
    }
//admin Update Maintenancelist
 public function updatemaintenancelist()
    {

         if ($_SESSION['is_admin'] == 1 && $_SESSION['is_facilityManager'] == 0) {
            //user login
            $lastUpdatedBy = $_SESSION['admin_userid'];
            $updatedByAdmin = 1;
        } else if ($_SESSION['is_admin'] == 1 && $_SESSION['is_facilityManager'] == 1) {
            //facility manager login
            $lastUpdatedBy = $_SESSION['admin_userid'];
            $updatedByAdmin = 2;
        } else {
            //register login
            $lastUpdatedBy = $_SESSION['admin_userid'];
            $updatedByAdmin = 0;
        }
        $query = "update maintenance_sync set photo='" . $_POST['photo'] . "',plan_img='" . $_POST['plan_img'] . "',building='" . $_POST['building'] . "',item_type='" . $_POST['itemtype'] . "',install_date='" . $_POST['install_date'] . "',"
                . "conditions='" . $_POST['condition'] . "',userfull_life='" . $_POST['usefull_life'] . "',health_safety='" . $_POST['health_safety'] . "',bcs_section='" . $_POST['bcs_section'] . "',make='" . $_POST['make'] . "',model='" . $_POST['model'] . "',units='" . $_POST['units'] . "',quantity='" . $_POST['quantity'] . "',cpu='" . $_POST['cpu'] . "',est_replacment='" . $_POST['estreplace'] . "',url='" . $_POST['url'] . "',comment='" . $_POST['comments'] . "',lastUpdatedDate = NOW(),lastUpdatedBy = '" . $lastUpdatedBy . "',"
                . "updatedByAdmin = '" . $updatedByAdmin . "'  where uid='" . $_POST['edit_id'] . "'";
        $result = db::query($query);
        return $result;
    }

    
//add maintenance sync api
   public function Addmaintenance() {
//mail('bhavisha.s@quantumtechnolabs.com','add maintenance',print_r($_FILES,true));
        $url = config::sys('site');
        $file = '';
        //photo img upload
        if ($_FILES['photo']['name'] != '') {

                $image_default = md5(time() . "_" . $_FILES["photo"]["name"]) . strrchr($_FILES['photo']['name'], '.');
                $image_path = $url . 'uploads/' . $image_default;

                $upload = move_uploaded_file($_FILES["photo"]["tmp_name"], $image_path);
                if ($upload == 1) {
                    $filename .= 'uploads/' . $image_default . ',';
                }

            $file = trim($filename, ',');
        }
        //plan img upload
        $planimg = '';
        if ($_FILES['plan_img']['name'] != '') {
            $plan_default = md5(time() . "_" . $_FILES["plan_img"]["name"]) . strrchr($_FILES['plan_img']['name'], '.');
            $plan_path = $url . 'uploads/' . $plan_default;

            $upload = move_uploaded_file($_FILES["plan_img"]["tmp_name"], $plan_path);
            if ($upload == 1) {
                $planimg = 'uploads/' . $plan_default;
            }
        }
    
   if(isset($_POST['building']) && $_POST['building'] != ''){
     $building = addslashes($_POST['building']);  
   }else{
       $building = '';
   }
   if(isset($_POST['item_type']) && $_POST['item_type'] != '')
   {
     $item_type = addslashes($_POST['item_type']);
   }else{
       $item_type = '';
   }
   if(isset($_POST['install_date'])  && $_POST['install_date'] != '')
   {
     $install_date = addslashes($_POST['install_date']);
   }else{
       $install_date = '';
   }
   if(isset($_POST['condition']) && $_POST['condition'] != ''){
      $conditions = addslashes($_POST['condition']);
   }else{
       $conditions = '';
   }
   if(isset($_POST['userfull_life']) &&  $_POST['userfull_life'] != ''){
       $userfull_life = addslashes($_POST['userfull_life']);
   }else{
       $userfull_life = '';
   }
   if(isset($_POST['health_safety']) && $_POST['health_safety'] != '')
   {
      $health_safety = addslashes($_POST['health_safety']);
   }else{
       $health_safety = '';
   }
   if(isset($_POST['userfull_life']) && $_POST['userfull_life'] != ''){
   $userfull_life = addslashes($_POST['userfull_life']);
   }else{
       $userfull_life = '';
   }
   if(isset($_POST['bcs_section']) && $_POST['bcs_section']!= ''){
     $bcs_section = addslashes($_POST['bcs_section']);
   }else{
       $bcs_section = '';
   }
   if(isset($_POST['make']) && $_POST['make']!= ''){
     $make = addslashes($_POST['make']);
   }else{
       $make = '';
   }
   if(isset($_POST['model']) && $_POST['model']!= ''){
     $model = addslashes($_POST['model']);
   }else{
       $model = '';
   }
   if(isset($_POST['units']) && $_POST['units']!= ''){
        $units = addslashes($_POST['units']);
   }else{
       $units = '';
   }  
   if(isset($_POST['url']) && $_POST['url']!= ''){
        $url = addslashes($_POST['url']);
   }else{
       $url = '';
   }  
   //if(isset($_POST['plan_text']) && $_POST['plan_text']!= ''){
        // $plan_text = addslashes($_POST['plan_text']);
  // }else{
     //  $plan_text = '';
 //  }  
   if(isset($_POST['comment']) && $_POST['comment']!= ''){
         $comment = addslashes($_POST['comment']);
   }else{
       $comment = '';
   }  
   if(isset($_POST['quantity']) && $_POST['quantity']!= ''){
         $quantity = addslashes($_POST['quantity']);
   }else{
       $quantity = '';
   }  
   if(isset($_POST['cpu']) && $_POST['cpu']!= ''){
       $cpu = addslashes($_POST['cpu']);
   }else{
       $cpu = '';
   }  
   if(isset($_POST['est_replacment']) && $_POST['est_replacment']!= ''){
     $est_replacment = addslashes($_POST['est_replacment']);
   }else{
       $est_replacment = '';
   }  
        
   $insert = 'insert maintenance_sync SET '
                . 'userid = "' . $_POST['userid'] . '" ,surveyid = "' . $_POST['surveyid'] .'" ,qno="'.$_REQUEST['qno'].'", '
                . 'photo = "'.$file.'", plan_img = "'.$planimg.'",building = "'.$building.'",'
                . 'item_type = "'.$item_type.'" ,install_date = "'.$install_date.'",'
                . 'conditions = "'.$conditions.'" ,userfull_life = "'.$userfull_life.'",'
                . 'health_safety = "'.$health_safety.'",bcs_section ="'.$bcs_section.'",'
                . 'make = "'.$make.'",model = "'.$model.'",units ="'.$units.'",'
                . 'quantity = "'.$quantity.'",cpu = "'.$cpu.'",est_replacment= "'.$est_replacment.'",'
                . 'url ="'.$url.'",'
                . 'comment = "'.$comment.'",created_date = NOW()';
  

//mail('bhavisha.s@quantumtechnolabs.com','maintenace add',print_r($insert,true));
        return $data = db::insert($insert);
    }
//end
//Get maintenance sync data
 public function getplandetails($id) {
        $data = "select * from maintenance_sync where uid = '" . $id . "'";
        $row = db::query($data);
        while ($row = mysqli_fetch_assoc($row)) {
            $url = config::sys('site_url');
            if($row['photo'] != ''){
              $row['photo']=$url.$row['photo'];
            }
            if($row['plan_img'] != ''){
              $row['plan_img']=$url.$row['plan_img'];
            }
            $response[] = $row;
        }
        return $response;
    }
 //end
 //get maintance list
    public function getMaintenancelist()
    {
        $query = "select * from maintenance_sync where  surveyid = '".$_REQUEST['surveyid']."' and qno = '".$_REQUEST['qno']."'";
         
         $data = db::query($query);
        while ($row = mysqli_fetch_assoc($data)) {
            $url = config::sys('site_url');
            if($row['photo'] != ''){
             $row['photo']=$url.$row['photo'];
            }
            if($row['plan_img'] != ''){
              $row['plan_img']=$url.$row['plan_img'];
            }
            $response[] = $row;
        }
        return $response;
    }
    //end
  //update maintenance sync api
    public function updatemaintenance() {

        $url = config::sys('site');
        $file = '';
        $update = '';
        //photo img upload
        if ($_FILES['photo']['name'] != '') {

            $image_default = md5(time() . "_" . $_FILES["photo"]["name"]) . strrchr($_FILES['photo']['name'], '.');
            $image_path = $url . 'uploads/' . $image_default;

            $upload = move_uploaded_file($_FILES["photo"]["tmp_name"], $image_path);
            if ($upload == 1) {
                $filename .= 'uploads/' . $image_default . ',';
            }

            $file = trim($filename, ',');
            $update .= "photo = '" . $file . "',";
        }
        //plan img upload
        $planimg = '';
        if ($_FILES['plan_img']['name'] != '') {
            $plan_default = md5(time() . "_" . $_FILES["plan_img"]["name"]) . strrchr($_FILES['plan_img']['name'], '.');
            $plan_path = $url . 'uploads/' . $plan_default;

            $upload = move_uploaded_file($_FILES["plan_img"]["tmp_name"], $plan_path);
            if ($upload == 1) {
                $planimg = 'uploads/' . $plan_default;
                $update .= "plan_img = '" . $planimg . "',";
            }
        }
           if (isset($_POST['surveyid']) && $_POST['surveyid'] != '') {
            $update .= "surveyid = '" . $_POST['surveyid'] . "',";
        }
        if (isset($_POST['qno']) && $_POST['qno'] != '') {
            $update .= "qno = '" . $_POST['qno'] . "',";
        }
        //if (isset($_POST['userid']) && $_POST['userid'] != '') {
            //$update .= "userid = '" . $_POST['userid'] . "',";
      //  }
       if (isset($_POST['userid']) && $_POST['userid'] != '') {
            $update .= "lastUpdatedBy = '" . $_POST['userid'] . "',";
        }
        if (isset($_POST['building']) && $_POST['building'] != '') {
            $building = addslashes($_POST['building']);
            $update .= "building = '" . $building . "',";
        }
        if (isset($_POST['item_type']) && $_POST['item_type'] != '') {
            $item_type = addslashes($_POST['item_type']);
            $update .= "item_type = '" . $item_type . "',";
        }
        if (isset($_POST['install_date']) && $_POST['install_date'] != '') {
            $install_date = addslashes($_POST['install_date']);
            $update .= "install_date = '" . $install_date . "',";
        }
        if (isset($_POST['condition']) && $_POST['condition'] != '') {
            $conditions = addslashes($_POST['condition']);
            $update .= "conditions = '" . $conditions . "',";
        }
        if (isset($_POST['userfull_life']) && $_POST['userfull_life'] != '') {
            $userfull_life = addslashes($_POST['userfull_life']);
            $update .= "userfull_life = '" . $userfull_life . "',";
        }
        if (isset($_POST['health_safety']) && $_POST['health_safety'] != '') {
            $health_safety = addslashes($_POST['health_safety']);
            $update .= "health_safety = '" . $health_safety . "',";
        }
        if (isset($_POST['bcs_section']) && $_POST['bcs_section'] != '') {
            $bcs_section = addslashes($_POST['bcs_section']);
            $update .= "bcs_section = '" . $bcs_section . "',";
        }
        if (isset($_POST['make']) && $_POST['make'] != '') {
            $make = addslashes($_POST['make']);
            $update .= "make = '" . $make . "',";
        }
        if (isset($_POST['model']) && $_POST['model'] != '') {
            $model = addslashes($_POST['model']);
            $update .= "model = '" . $model . "',";
        }      
        if (isset($_POST['units']) && $_POST['units'] != '') {
            $units = addslashes($_POST['units']);
            $update .= "units = '" . $units . "',";
        }  
          if (isset($_POST['quantity']) && $_POST['quantity'] != '') {
            $quantity = addslashes($_POST['quantity']);
            $update .= "quantity = '" . $quantity . "',";
        }
        if (isset($_POST['cpu']) && $_POST['cpu'] != '') {
            $cpu = addslashes($_POST['cpu']);
            $update .= "cpu = '" . $cpu . "',";
        }
        if (isset($_POST['est_replacment']) && $_POST['est_replacment'] != '') {
            $est_replacment = addslashes($_POST['est_replacment']);
            $update .= "est_replacment = '" . $est_replacment . "',";
        }
        if (isset($_POST['url']) && $_POST['url'] != '') {
            $url = addslashes($_POST['url']);
            $update .= "url = '" . $url . "',";
        }
       
       // if (isset($_POST['plan_text']) && $_POST['plan_text'] != '') {
         //   $plan_text = addslashes($_POST['plan_text']);
          //  $update .= "plan_text = '" . $plan_text . "',";
       // }
        if (isset($_POST['comment']) && $_POST['comment'] != '') {
            $comment = addslashes($_POST['comment']);
            $update .= "comment = '" . $comment . "',";
        }
mail('bhavisha.s@quantumtechnolabs.com','request',print_r($_POST,true));
        $update .= "updatedByAdmin= '0',lastUpdatedDate = NOW()  ";
      //  $update = trim($update, ',');
        $update = "update maintenance_sync set "
                . " $update where uid = '" . $_REQUEST['maintenanceID'] . "'";

       // mail('bhavisha.s@quantumtechnolabs.com', 'maintenace update', print_r($update, true));
         $data = db::query($update);
       return $_REQUEST['maintenanceID'];
    }

//end
 //api get mentaince whole list
    public function GetList_Old() {
        $query = "select * from maintenance_sync order by uid desc ";
        $data = db::query($query);
        while ($row = mysqli_fetch_assoc($data)) {
            $url = config::sys('site_url');
            if ($row['photo'] != '') {
                $row['photo'] = $url . $row['photo'];
            }
            if ($row['plan_img'] != "") {
                $row['plan_img'] = $url . $row['plan_img'];
            }
            $response[] = $row;
        }
        return $response;
    }
    //end
//api get mentaince whole list
    public function GetList() {
        //get area fron register table
        $register = "select area from register as r where r.uid = '" . $_POST['user_id'] . "' ";
        $Regrow = db::query($register);
        while ($data = mysqli_fetch_assoc($Regrow)) {
            $List .= $data['area'] . ",";
        }
        $Areas = trim($List, ',');
        //get area name from area table
        $area = "select a.name from area as a where  a.uid IN ($Areas)";
        $Arearow = db::query($area);
        while ($data = mysqli_fetch_assoc($Arearow)) {
            $AreaList .= ",'" . $data['name'] . "'";
        }
        $AreaList = trim($AreaList, ',');   
//      get list of  survey id
        $query = "select surveyid from building_survey where answer IN (" . $AreaList . ") group by surveyid order by uid desc";
        $row = db::query($query);
        while ($datas = mysqli_fetch_assoc($row)) {
            $Sdata .= $datas['surveyid'] . ",";
            
        }
        $Sdata = trim($Sdata,",");
        //get maintenance list 
        $query = "select * from maintenance_sync where surveyid IN ($Sdata) order by uid desc ";
       // $query = "select * from maintenance_sync order by uid desc ";
        $data = db::query($query);
        while ($row = mysqli_fetch_assoc($data)) {
            $url = config::sys('site_url');
            if ($row['photo'] != '') {
                $row['photo'] = $url . $row['photo'];
            }
            if ($row['plan_img'] != "") {
                $row['plan_img'] = $url . $row['plan_img'];
            }
            $response[] = $row;
        }
        return $response;
    }
    //end

    }
