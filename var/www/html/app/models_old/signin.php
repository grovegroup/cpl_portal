<?php

class signin{
		public function __construct() {		
		
		}
		
		public static function try_login( $p=array() ) {
			$arrErrors = array();
			// lets filter user input, to project websit from XSS injection attacts, 
			// in following function I am passing entire post to function, so it will filer all input and return clean value
			$p = format::process_secure($p);
			$language = $_SESSION['language'] == '' ? 'EN' : $_SESSION['language'];
                        $webcontents = Language::getWebcontents($language);
			//lets check name
			
			// lets check email
			if( !isset( $p['email'] )  || ( isset($p['email']) && empty( $p['email'] ) ) ) {
				$arrErrors['email.error'] = "Please provide email id.";
			} 

			// lets check password
			if( !isset( $p['password'] )  || ( isset($p['password']) && empty( $p['password'] ) ) ) {
				$arrErrors['password.error'] = "Please provide password.";
			}else if ( isset($p['password']) && !empty( $p['password'] ) && ( strlen($p['password']) < 6 ) ) {
				$arrErrors['password.error'] = "Password must be at least 6 characters long.";
			}
			
			if(empty($arrErrors))
			{
				$query = "SELECT `uid`,`name`,`email`,`image` FROM `registration` where `email`='".db::escape( $p['email'] )."' AND `password`='".  md5($p['password'])."'";
				$res = db::query($query);
				if ($res->num_rows > 0){
					$arrErrors['success'] = "login successfully";
					$row = mysqli_fetch_assoc( $res );
					$_SESSION['userid'] = $row['uid'];
					
					if (isset($p['remember'])) 
					{
						setcookie('remember', '1', time()+3600*24*30);
						setcookie('email', $p['email'],time()+3600*24*30);
						setcookie('password', $p['password'],time()+3600*24*30);
					} else {
						setcookie('email', '', time() - 3600);
						setcookie('password', '', time() - 3600);
						setcookie('remember', '', time() - 3600);
					}
				}
				else
				{
					$arrErrors['login.error'] = $webcontents['con_err_alt_login'];
				}
				
			}
			return $arrErrors;		
		}
		
		
		public static function is_logged_in(){
			return isset($_SESSION['userid']);
		}
		
		public static function getsessiondata()
		{
			$language = $_SESSION['language'] == '' ? 'EN' : $_SESSION['language'];
			$webcontents = Language::getWebcontents($language);
			$response = array();
			if(self::is_logged_in())
			{
				$data = home::getdata($_SESSION['userid']);
				
				if(!empty($data))
				{
					
					$dropdown = '<dl id="sample2" class="abc">';
						$dropdown.= '<dt class="abc_dt"><a href="#"><span>'.$data['name'].'</span></a></dt>';
						$dropdown.= '<dd>';
							$dropdown.= '<ul>';
									$dropdown.= '<li><a href="'.config::url().'wisdom/mywisdom/" id="lan_con_profile_mywisdom">'.$webcontents['con_MYWISDOM'].'</a></li>';
									$dropdown.= '<li><a href="'.config::url().'registration/edit" id="lan_con_profile_editprofile">'.$webcontents['con_EDITPROFILE'].'</a></li>';
									$dropdown.= '<li><a href="'.config::url().'registration/changepassword/" id="lan_con_profile_changepassword">'.$webcontents['con_CHANGEPASSWORD'].'</a></li>';
									$dropdown.= '<li><a href="'.config::url().'signout">LOGOUT</a></li>';
							$dropdown.= '</ul>';
						$dropdown.= '</dd>';
					$dropdown.= '</dl>';
					$response['name'] = $dropdown;
					$response['url'] = config::url().'signout';
					$response['login_email'] = $data['email'];
					$response['login_username'] = $data['name'];
					$response['image'] = $data['is_social']==0 ? config::url().'uploads/account/'.$data['image'] : $data['image'];
				}
				else
				{
					$login = '<div style="margin-top: 6px;"><a href="'.config::url().'registration/signin" style="color: white;text-decoration: none;text-transform: uppercase;" id="lan_con_profile_signin">'.$webcontents['con_Signin'].'</a></div>';
					$response['name'] = $login;
					$response['url'] = config::url().'registration/signin';
					$response['login_email'] = '';
					$response['login_username'] = $data['name'];
					$response['image'] = config::url().'assets/images/profile_icon.png';
				}
			}
			else
			{
				$login = '<div style="margin-top: 6px;"><a href="'.config::url().'registration/signin" style="color: white;text-decoration: none;text-transform: uppercase;" id="lan_con_profile_signin">'.$webcontents['con_Signin'].'</a></div>';
				$response['name'] = $login;
				$response['url'] = config::url().'registration/signin';
				$response['login_email'] = '';
				$response['login_username'] = $data['name'];
				$response['image'] = config::url().'assets/images/profile_icon.png';
			}
			
			return $response;
		}
}
?>
