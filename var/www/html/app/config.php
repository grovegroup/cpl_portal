<?php

ini_set('max_execution_time', '500');
error_reporting(E_ALL);
/**
 * Enter Configuration Data Below
 */
class config{

	/**
	 * [$settings description]
	 * @var array
	 */
	private static $settings = array(
		// folder names for application
		'system'		=> array(
			'root' 			=> '/var/www/html/app/',
			'application'		=> '/var/www/html/app/',
			'framework'		=> '/var/www/html/framework/',
			'site'			=> '/var/www/html/',
			'uploads'		=> '/var/www/html/uploads/',
                        'site_url' 		=> 'http://54.175.81.170/',
			// default routing
			'default'		=> 'index',
			'404'			=> '404'//404
		),

		// populated by the request
	'request'	
		=> array(

			'raw'			=> array(),
			'paths'			=> array(),

			// use this to output verbose messages if required
			'debug'			=> true,
			'debug_email'    	=> 'bhavisha.s@quantumtechnolabs.com',

			// the url to use in url replacement code in emails etc
			'protocol'		=> 'http://',
			'host'			=> '54.175.81.170/'
		)
	);

	/**
	 * [$data description]
	 * @var array
	 */
	public static $data = array();

	/**
	 * [$log description]
	 * @var array
	 */
	private static $log = array();

	/**
	 * [db description]
	 * @param  string $key [description]
	 * @return [type]      [description]
	 */
	public static function db($key = '') {
		switch ($key){
			case 'server': return self::$settings['db']['db1']['server'];		break;
			case 'username': return self::$settings['db']['db1']['username'];	break;
			case 'password': return self::$settings['db']['db1']['password'];	break;
			case 'database': return self::$settings['db']['db1']['database'];	break;
		}
		return null;
	}

	/**
	 * [sys description]
	 * @param  string $key [description]
	 * @return [type]      [description]
	 */
	public static function sys($key = ''){

		$sys = '';

		if (in_array($key, array('root', 'application', 'framework', 'site'))){
			$sys = self::$settings['system'][$key];
		}else if (isset(self::$settings['system'][$key])){
			$sys = self::$settings['system'][$key];
		}

		return $sys;
	}

	/**
	 * [get description]
	 * @param  string $key [description]
	 * @return [type]      [description]
	 */
	public static function get($key = ''){
		return (isset(self::$data[$key]) ? self::$data[$key] : null);
	}

	/**
	 * [set description]
	 * @param string $key [description]
	 * @param string $val [description]
	 */
	public static function set($key = '', $val = ''){
		self::$data[$key] = $val;
	}

	/**
	 * [req description]
	 * @param  [type] $key [description]
	 * @param  [type] $val [description]
	 * @return [type]      [description]
	 */
	public static function req($key = null, $val = null){
		if ($key && !$val){
//print_r(self::$settings['request'][$key]);
			if (isset(self::$settings['request'][$key])){
                                //echo self::$settings['request'][$key];exit;
				return self::$settings['request'][$key];
			}
		}else if ($key && $val){
			if (isset(self::$settings['request'][$key])){
				self::$settings['request'][$key] = $val;
			}
		} else if($key && 'site'==$key){
			return self::$settings['request']['protocol'].self::$settings['request']['host'];
		}
		return null;
	}

	/**
	 * [debug_enabled description]
	 * @return [type] [description]
	 */
	public static function debug_enabled(){
		return self::$settings['debug'];
	}

	/**
	 * [debug_email description]
	 * @return [type] [description]
	 */
	public static function debug_email(){
		return self::$settings['debug_email'];
	}

	/**
	 * [url description]
	 * @param  string $path [description]
	 * @return [type]       [description]
	 */
	public static function url($path = ''){
		$url = self::req('protocol');
		$url.= self::req('host');
		//$url.= $_SERVER['SERVER_NAME'];
		$url.= 'index.php/';
		$url.= $path;

		return $url;
	}

       public static function urls($path = ''){
		$url = self::req('protocol');
		$url.= self::req('host');
		//$url.= $_SERVER['SERVER_NAME'];
		$url.= '/';
		$url.= $path;
		return $url;
	}
	/**
	 * [msgLog description]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public static function msgLog($message){
		self::$log[] = $message;
	}

	/**
	 * [msgSend description]
	 * @return [type] [description]
	 */
	public static function msgSend(){
		if (true === self::debug_enabled()){
			$msg = implode("\n", self::$log);
			mail(self::debug_email(), 'Log', $msg, 'From: ' . self::debug_email());
			self::$log = array();
		}
	}
       //Menu display in Admin Login
    public static function header() {
      
        if ($_SESSION['is_admin'] == 1) {
            $menu = '<li>
                            <a href="{{ uri }}admin/register/userdetail/"><i class="icon-chevron-right"></i> Users</a>
                        </li>
                        <li>
                            <a href="{{ uri }}admin/area/view/"><i class="icon-chevron-right"></i>School District</a>
                        </li><li>
                            <a href="{{ uri }}admin/facilityManager/view/"><i class="icon-chevron-right"></i>Facility Manager</a>
                        </li>';
        } else {
            $menu = '';
        }
        return $menu;
    }
    //end

}

/**
 * Load classes dynamically
 */
function __autoload($class_name){
	$cls = strtolower($class_name) . '.php';
	
	$classes = array(
		config::sys('framework') . $cls,
		config::sys('application') . 'models/' . $cls,
		config::sys('application') . 'controllers/' . $cls
	);

	$found = false;

	foreach ($classes as $class){

		if (file_exists($class)){
			$found = true;
			include($class);
			break;
		}else{
			$class . ' does not exist<br />';
		}
	}

	if (!$found){
		echo 'class: ' . $class_name . ' not found';
	}
}

if (!isset($_SERVER['OVERRIDE'])){
	
	config::set('request', $_SERVER['REQUEST_URI']);
	$paths = array_values(explode('/', str_replace(config::get('host'), '', config::get('request'))));


	//print_r($_SERVER['REQUEST_URI']);exit;
	foreach ($paths as $index => $path){
		if (strlen($path) < 2){
			unset($paths[$index]);
		}
	}

	$paths = array_values($paths);

       // print_r($paths);exit;
	config::set('paths', $paths);

	router::initialise();
}

?>
