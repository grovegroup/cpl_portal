<?php

class Admin {

	public function __construct() {		
		
	}
	public static function try_login( $p=array() ) {
		$arrErrors = array();
		// lets filter user input, to project websit from XSS injection attacts, 
		// in following function I am passing entire post to function, so it will filer all input and return clean value
		$p = format::process_secure($p);
		//lets check name

		// lets check email
		if( !isset( $p['email'] )  || ( isset($p['email']) && empty( $p['email'] ) ) ) {
			$arrErrors['email.error'] = "Please provide email id.";
		} 

		// lets check password
		if( !isset( $p['password'] )  || ( isset($p['password']) && empty( $p['password'] ) ) ) {
			$arrErrors['password.error'] = "Please provide password.";
		}else if ( isset($p['password']) && !empty( $p['password'] ) && ( strlen($p['password']) < 4 ) ) {
			$arrErrors['password.error'] = "Password must be at least 6 characters long.";
		}

		if(empty($arrErrors))
		{
			  $query = "SELECT * FROM `user` where `email`='".db::escape( $p['email'] )."' AND `password`='".  md5($p['password'])."'";
			$res = db::query($query);
			if ($res->num_rows > 0){
				$arrErrors['success'] = "login successfully";
				$row = mysqli_fetch_assoc( $res );
                                
				$_SESSION['admin_userid'] = $row['uid'];
                                $_SESSION['is_admin'] = 1;
                                $_SESSION['is_facilityManager'] = 0;
                                $_SESSION['username'] = $row['name'];

				if (isset($p['remember'])) 
				{
					setcookie('admin_remember', '1', time()+3600*24*30);
					setcookie('admin_email', $p['email'],time()+3600*24*30);
					setcookie('admin_password', $p['password'],time()+3600*24*30);
				} else {
					setcookie('admin_email', '', time() - 3600);
					setcookie('admin_password', '', time() - 3600);
					setcookie('admin_remember', '', time() - 3600);
				}
			}
			else
			{
                                 //login Register User
                                 $query = "SELECT * FROM `register` where `email`='" . db::escape($p['email']) . "' AND `password`='" . $p['password'] . "' AND ( permission LIKE '%Read Web%' || permission LIKE '%Write Web%' )";
                                  $res = db::query($query);
                                  if ($res->num_rows > 0) {
                                     $arrErrors['success'] = "login successfully";
                                     $row = mysqli_fetch_assoc($res);

                                     $_SESSION['admin_userid'] = $row['uid'];
                                     $_SESSION['permission'] = $row['permission'];
                                     $_SESSION['is_admin'] = 0;
                                     $_SESSION['is_facilityManager'] = 0;
                                     $_SESSION['username'] = $row['name'];


                                     if (isset($p['remember'])) {
                                           setcookie('admin_remember', '1', time() + 3600 * 24 * 30);
                                           setcookie('admin_email', $p['email'], time() + 3600 * 24 * 30);
                                           setcookie('admin_password', $p['password'], time() + 3600 * 24 * 30);
                                      } else {
                                           setcookie('admin_email', '', time() - 3600);
                                           setcookie('admin_password', '', time() - 3600);
                                           setcookie('admin_remember', '', time() - 3600);
                                       }
                                   } else {
                                       //check login user is facility manager
                                        $query = "SELECT * FROM `facilityManager` where `email`='" . db::escape($p['email']) . "' AND `password`='" . $p['password'] . "' ";
                                         $res = db::query($query);
                                         if ($res->num_rows > 0) {                                          

                                             $arrErrors['success'] = "login successfully";
                                             $row = mysqli_fetch_assoc($res);

                                             $_SESSION['admin_userid'] = $row['uid'];                        
                                             $_SESSION['is_admin'] = 1;
                                             $_SESSION['is_facilityManager'] = 1;
                                             $_SESSION['username'] = $row['name'];

                                            if (isset($p['remember'])) {
                                              setcookie('admin_remember', '1', time() + 3600 * 24 * 30);
                                              setcookie('admin_email', $p['email'], time() + 3600 * 24 * 30);
                                              setcookie('admin_password', $p['password'], time() + 3600 * 24 * 30);
                                            } else {
                                               setcookie('admin_email', '', time() - 3600);
                                               setcookie('admin_password', '', time() - 3600);
                                               setcookie('admin_remember', '', time() - 3600);
                                             }
                                    } else {

                                            $arrErrors['login.error'] = "Details supplied did not match our records.";
                                     }
                                  }
				//$arrErrors['login.error'] = "Details supplied did not match our records.";
			}

		}
		return $arrErrors;		
	}
	
	
	public static function is_logged_in(){

		return isset($_SESSION['admin_userid']);
                
	}
	
	
	public static function getWisdomscount(){
		$response = false;
		
			$query = "SELECT ";		
			$query.= "COUNT(`uid`) as total ";		
			$query.= "FROM ";
			$query.= "`wisdom` ";
			$query.= "WHERE ";
			$query.= "`active`= '1' ";
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response = $row['total'];
				}
			}
		return $response;
	}
	
	
	public static function getuserscount(){
		$response = false;
		
			$query = "SELECT ";		
			$query.= "COUNT(`uid`) as total ";		
			$query.= "FROM ";
			$query.= "`registration` ";
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response = $row['total'];
				}
			}
		return $response;
	}
	
	
	public static function getcommentscount(){
		$response = false;
		
			$query = "SELECT ";		
			$query.= "COUNT(`uid`) as total ";		
			$query.= "FROM ";
			$query.= "`comment` ";
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response = $row['total'];
				}
			}
		return $response;
	}
	
	public static function GetHomepageWisdom ( $limit = false ) {
		$response = array();
		
			$query = "SELECT * ";
			$query.= "FROM `wisdom` ";
			//$query.= "WHERE ";
			//$query.= "`active`=1 ";
			$query.= "order by `uid` desc";
			if(false != $limit)
			{
				$query.= " LIMIT ".$limit;
			}
			
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response[] = $row;
				}
			}
		return $response;
	}
	
	public static function GetHomepageUser ($limit = false) {
		$response = array();
		
			$query = "SELECT * ";
			$query.= "FROM `registration` ";
			$query.= "order by `uid` desc";
			if(false != $limit)
			{
				$query.= " LIMIT ".$limit;
			}
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response[] = $row;
				}
			}
		return $response;
	}

	//partner admin side code start
	
		//count no of partner
		public static function getPartnercount(){
		$response = false;
		
			$query = "SELECT ";		
			$query.= "COUNT(`uid`) as total ";		
			$query.= "FROM ";
			$query.= "`partner` ";
			/*$query.= "WHERE ";
			$query.= "`is_active`= '1' ";*/
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response = $row['total'];
				}
			}
			
		return $response;
	}
	
	public static function GetHomepagePartner ( $limit = false ) {
		$response = array();
		
			$query = "SELECT * ";
			$query.= "FROM `partner` ";
			$query.= "order by `uid` desc";
			if(false != $limit)
			{
				$query.= " LIMIT ".$limit;
			}
			
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response[] = $row;
				}
			}
		return $response;
	}
        
        public static function partnercreate($post=array(), $files=array()){
		$response = false;
		// lets filter user input, to project websit from XSS injection attacts, 
		// in following function I am passing entire post to function, so it will filer all input and return clean value
		$post = format::process_secure($_POST);
//		print_r($files);
//		echo $files['partner_image']['name'];
//		exit;
		// lets upload images
		
		$image_1 = '';
		if( isset($files['partner_image']) && !empty($files['partner_image']['name']) && format::is_valid_image_upload_file($files['partner_image']) === true  ) {
//			$arrExplode = explode(".",$files['partner_image']['name']);
//			$extension = end( $arrExplode );
//			$image_1 = "image_1_" . time() . "." . $extension;
			                $image_default = $files['partner_image']['name'];
                            $image_path = config::sys('uploads'). 'partner/' . $image_default;
							
			move_uploaded_file( $files['partner_image']['tmp_name'],$image_path);
		}
		
		
		
		
		
		// prepare insert sql query
		$query = "INSERT INTO `partner`";
		$query.= "SET";
		$query.= "`partner_name`='".$post['name']."',";
		$query.= "`partner_description`='".$_POST['partner_dis']."', ";
		$query.= "`partner_image`='" .$image_default. "', ";
		$query.= "`partner_url`='" . $post['partner_url']. "', ";
		$query.= "`is_active`='" .$post['status']. "'";
		;
		
		// run sql query
		$partner_uid = db::insert( $query );
		
		if(isset($partner_uid) && is_numeric($partner_uid) && $partner_uid > 0) {
			if( isset($post['keywords']) && is_array( $post['keywords'] ) && count( $post['keywords'] ) ) {
				wisdom_keywords_relation::save_wisdom_keywords( $post['keywords'],  $wisdom_uid);
			}
		}
		return $response=true;
		return $partner_uid ;
	}
	
	
	public static function GetPartner($limit=false) {
		$response = false;
		if($limit)
		{
			$limit=$limit;
		}
		else{
			$limit=4;
		}
		$query = "SELECT ";		
		$query.= "* ";		
		$query.= "FROM ";
		$query.= "`partner` ";
		$query.= "WHERE `is_active`= '1' ";
		$query.= "ORDER BY `partner_name`";
		$query.= "LIMIT $limit";
		$result = db::query( $query );

		if( $result && isset($result->num_rows) && $result->num_rows ) 
		{
			$response = array();
			while($row 	= mysqli_fetch_assoc( $result )) 
			{
				$response[] = $row;
			}			
		}
		
		return $response;
	}
	
	public static function EditPartner($partner_id){
		$response = false;
		
		$query = "SELECT ";		
		$query.= "* ";		
		$query.= "FROM ";
		$query.= "`partner` ";
		$query.= "WHERE `uid`= '$partner_id' ";
		$query.= "LIMIT 1";
		$result = db::query( $query );
		
		if( $result && isset($result->num_rows) && $result->num_rows ) 
		{
			$response = array();
			while($row 	= mysqli_fetch_assoc( $result )) 
			{
				$response[] = $row;
			}			
		}
		
		return $response;
	}
	
	public static function UpdatePartner($post=array(), $files=array())
	{
                $response = false;
		//print_r($post);
		//print_r($files);
		
		if( isset($files['partner_image']) && !empty($files['partner_image']['name']) && format::is_valid_image_upload_file($files['partner_image']) === true  ) {

			    $image_default = $files['partner_image']['name'];
                            $image_path = config::sys('uploads'). 'partner/' . $image_default;
                            $unlinkimage=config::sys('uploads'). 'partner/'. $post['image_name'];
			unlink($unlinkimage);				
			move_uploaded_file( $files['partner_image']['tmp_name'],$image_path);
			$image=$image_default;
		}
		else{
			$image=$post['image_name'];
		}
		$query = "update";		
		$query.= "`partner`";		
		$query.= "set";
		$query.= '`partner_name`="'.$post[name].'"';
		$query.= ',`partner_description`="'.$post[partner_dis].'"';
		$query.= ',`partner_image`="'.$image.'"';
		$query.= ',`partner_url`="'.$post[partner_url].'"';
		$query.= ',`is_active`="'.$post[status].'"';
		$query.= "WHERE `uid`= '$post[partner_uid]'";
                $result = db::query( $query );
		if($result)
                {
                    $response=true;
                }
                return $response;
              
	}
        
        public static function partnerdelete($post=array())
        {
            $responce=false;
               $query = "DELETE FROM ";		
		$query.= "`partner`";
		$query.= "WHERE `uid`= $post";
                $result = db::query( $query );
                if($result)
                {
                $responce=true;
                }
                return $responce;
                
        }
	
	//partner admin side code end

	//------------------- for wisdom side------------------//
	public static function removeWisdom ( $wisdomUid = false ) {
		$response = false;
		if(false != $wisdomUid)
		{
			$query = "DELETE FROM `wisdom` ";
			$query.= "WHERE `uid` ='".$wisdomUid."'";
			$result = db::query( $query );
			if($result)
			{
				$response = true;
			}
		}
		return $response;
	}
	//-------------------END OF WISDOM SIDE------------------//
	
	//------------------- for user side------------------//
	public static function removeUser ( $userUid = false ) {
		$response = false;
		if(false != $userUid)
		{
			$query = "DELETE FROM `registration` ";
			$query.= "WHERE `uid` ='".$userUid."'";
			$result = db::query( $query );
			if($result)
			{
				$response = true;
			}
		}
		return $response;
	}
	//--------------------END OF USER SIDE--------------------//

	//wisdom tag code start
        
            public static function getWisdomtagcount(){
		$response = false;
		
			$query = "SELECT ";		
			$query.= "COUNT(`uid`) as total ";		
			$query.= "FROM ";
			$query.= "`wisdom_keywords` ";
			$query.= "WHERE ";
			$query.= "`active`= '1' ";
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response = $row['total'];
				}
			}
			
		return $response;
	}
        
        public static function GetHomepageWisdomtag ( $limit = false ) {
		$response = false;
                        
                       $query = "SELECT wk.*,";
                        $query.= "(select name from language where language.uid=wk.language_relation) ";
                        $query.= "as language ";
                        $query.= "from `wisdom_keywords` as wk ";
                        
			
			if(false != $limit)
			{
				$query.= " LIMIT ".$limit;
			}
			
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response[] = $row;
				}
			}
		return $response;
	}
        
        public static function Getlanguage()
        {
             $query = "SELECT uid,name ";
            $query.= "from `language` where `active`='1' ";
                  
                        $result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response[] = $row;
				}
			}
		return $response;
        }
        
        public static function wisdomtagcreate($post=array()){
            
                $responce=FALSE;
                $keywordlower=  strtolower($post['name']);
                $keywordupper=  ucfirst(strtolower($post['name']));
                $query = "INSERT INTO `wisdom_keywords`";
		$query.= "SET";
		$query.= "`keyword`='".$keywordupper."',";
		$query.= "`keyword_lower_case`='".$keywordlower."', ";
		$query.= "`language_relation`='" .$post['language']. "', ";
		$query.= "`active`='" .$post['status']. "'";
		//echo $query;
//                exit;
		// run sql query
		$wisdomtag_uid = db::insert( $query );
		
		if(isset($wisdomtag_uid) && is_numeric($wisdomtag_uid) && $wisdomtag_uid > 0) {
			if( isset($post['keywords']) && is_array( $post['keywords'] ) && count( $post['keywords'] ) ) {
				wisdom_keywords_relation::save_wisdom_keywords( $post['keywords'],  $wisdom_uid);
			}
		}
		return $response=true;
		return $wisdomtag_uid ;
        }
        
        public static function Editwisdomtag($wisdomtag_id){
		$response = false;
		
		$query = "SELECT ";		
		$query.= "* ";		
		$query.= "FROM ";
		$query.= "`wisdom_keywords` ";
		$query.= "WHERE `uid`= '$wisdomtag_id' ";
		$query.= "LIMIT 1";
		$result = db::query( $query );
		
		if( $result && isset($result->num_rows) && $result->num_rows ) 
		{
			$response = array();
			while($row 	= mysqli_fetch_assoc( $result )) 
			{
				$response[] = $row;
			}			
		}
		
		return $response;
	}
        
        public static function Updatewisdomtags($post=array())
        {
             $response = false;
            
                $keywordlower=  strtolower($post['name']);
                $keywordupper=  ucfirst(strtolower($post['name']));
                $query = "update ";		
		$query.= "`wisdom_keywords` ";		
		$query.= "set ";
		$query.= '`keyword`="'.$keywordupper.'"';
		$query.= ',`keyword_lower_case`="'.$keywordlower.'"';
		$query.= ',`language_relation`="'.$_POST['language'].'"';
		$query.= ",`active`='$post[status]'";
		$query.= "WHERE `uid`= '$post[wisdomtagid]'";
              
                $result = db::query( $query );
		if($result)
                {
                    $response=true;
                }
                return $response;
        }
        
        public static function wisdomtagdelete($post=array())
        {
            $responce=false;
               $query = "DELETE FROM ";		
		$query.= "`wisdom_keywords` ";
		$query.= "WHERE `uid`= $post";
                $result = db::query( $query );
                if($result)
                {
                $responce=true;
                }
                return $responce;
                
        }
        
        //wisdom tag code end

	 //cms data code start
        
            public static function Cmsdata($lang){
                
                $response = false;
		
		$query = "SELECT ";		
		$query.= "* ";		
		$query.= "FROM ";
		$query.= "`cmspages` where language_relation = '$lang'";
                
		$result = db::query( $query );
		
		if( $result && isset($result->num_rows) && $result->num_rows ) 
		{
			$response = array();
			while($row 	= mysqli_fetch_assoc( $result )) 
			{
				$response[] = $row;
			}			
		}
		
		return $response;
                
            }
            
            public static function CmsdataUpdate($datacms=array())
            {
                error_reporting(0);
               
                //print_r($datacms);
               
//                $datacms['cms_privacy1']="";
//                $datacms['cmsid']="";
//                $datacms['cms_terms']="";
//                $datacms['cms_about']='';
               $cms_privacy="";
               $cmsid="";
               $cmsprivacydis="";
               $cmslanguage="";
               $cms_terms="";
               
               $cms_privacy=$datacms['cms_privacy'];
               $cmsid=$datacms['cmsid'];
               $cmsprivacydis=$datacms['privacy_dis'];
               $cmslanguage=$datacms['language'];
               $cms_terms=$datacms['cmsterms'];
               $cmstermsdis=$datacms['terms_dis'];
               $cms_about=$datacms['cms_about'];
               $cmsaboutdis=$datacms['about_dis'];
               
               
                if($cms_privacy=='cms_privacy' && $cmsid=='')
                {
                   
                        $data_privacy=addslashes($cmsprivacydis);
                       
                       
                         $query = "INSERT INTO ";		
                    $query.= "`cmspages`";		
                    $query.= "set";
                    $query.= "`language_relation`='".$cmslanguage."'";
                    $query.= ',`keyword`="'.$cms_privacy.'"';
                    $query.= ',`page`="'.$data_privacy.'"';
//                    echo $query;
//                    exit;
                    $result = db::query( $query );
                    if($result)
                    {
                    $responce=true;
                    }
                    return $responce;
                }
                
                 if($cms_privacy=='cms_privacy' && $cmsid!='')
                {
                    $data_privacy=addslashes($cmsprivacydis);
                     $query = "update";		
		$query.= "`cmspages`";		
		$query.= "set";
                $query.= '`page`="'.$data_privacy.'"';
		$query.= "WHERE `uid`=$cmsid";
		$query.= ' and `keyword`="'.$cms_privacy.'"';
                $result = db::query( $query );
                if($result)
                {
                $responce=true;
                }
                return $responce;
                    
                }
                
                if($cms_terms=='cms_terms' && $cmsid=='')
                {
                   
                        $data_terms=addslashes($cmstermsdis);
                         $query = "INSERT INTO ";		
                    $query.= "`cmspages`";		
                    $query.= "set";
                    $query.= "`language_relation`='".$cmslanguage."'";
                    $query.= ',`keyword`="'.$cms_terms.'"';
                    $query.= ',`page`="'.$data_terms.'"';
                    //echo $query;
                    $result = db::query( $query );
                    if($result)
                    {
                    $responce=true;
                    }
                    return $responce;
                }
                if($cms_terms=='cms_terms' && $cmsid!='')
                {

                  
                     $data_terms=addslashes($cmstermsdis);
                     $query = "update ";		
		$query.= "`cmspages` ";		
		$query.= "set ";
                $query.= "`page`='".$data_terms."' ";
		$query.= "WHERE `uid`=$cmsid";
		$query.= ' and `keyword`="'.$cms_terms.'"';
                
                //echo $query;
                $result = db::query( $query );
                if($result)
                {
                $responce=true;
                }
                return $responce;
                    
                }
                
                if($cms_about=='cms_about' && $cmsid=='')
                {
                   
                    
                        $data_about=addslashes($cmsaboutdis);
                         $query = "INSERT INTO ";		
                    $query.= "`cmspages`";		
                    $query.= "set";
                    $query.= "`language_relation`='".$cmslanguage."'";
                    $query.= ',`keyword`="'.$cms_about.'"';
                    $query.= ',`page`="'.$data_about.'"';
                    
                    $result = db::query( $query );
                    if($result)
                    {
                    $responce=true;
                    }
                    return $responce;
                }
                if($cms_about=='cms_about' && $cmsid!='')
                {

                   // $data = mysqli_real_escape_string($datacms['terms_dis']);
                     $data_about=addslashes($cmsaboutdis);
                     $query = "update ";		
		$query.= "`cmspages` ";		
		$query.= "set ";
                $query.= "`page`='".$data_about."' ";
		$query.= "WHERE `uid`=$cmsid";
		$query.= ' and `keyword`="'.$cms_about.'"';
                
                //echo $query;
                $result = db::query( $query );
                if($result)
                {
                $responce=true;
                }
                return $responce;
                    
                }
            }
            
            public static function  Gettermsdata()
            {
                $response = false;
		
		$query = "SELECT ";		
		$query.= "page ";		
		$query.= "FROM ";
		$query.= "`cmspages`";
                $query.= ' WHERE `keyword`="terms"';
		$result = db::query( $query );
		
		if( $result && isset($result->num_rows) && $result->num_rows ) 
		{
			$response = array();
			while($row 	= mysqli_fetch_assoc( $result )) 
			{
				$response[] = $row;
			}			
		}
		
		return $response;
                
            }
            
            public static function  Getprivacydata()
            {
                $response = false;
		
		$query = "SELECT ";		
		$query.= "page ";		
		$query.= "FROM ";
		$query.= "`cmspages`";
                $query.= ' WHERE `keyword`="privacy"';
		$result = db::query( $query );
		
		if( $result && isset($result->num_rows) && $result->num_rows ) 
		{
			$response = array();
			while($row 	= mysqli_fetch_assoc( $result )) 
			{
				$response[] = $row;
			}			
		}
		
		return $response;
                
            }
            
            public static function getcmsdatalang($post=array())
            {
                $response = false;
		
		$query = "SELECT ";		
		$query.= "* ";		
		$query.= "FROM ";
		$query.= "`cmspages` where language_relation = '".$post['language']."' and keyword='".$post['page']."'";
		$result = db::query( $query );
		
		if( $result && isset($result->num_rows) && $result->num_rows ) 
		{
			$response = array();
			while($row 	= mysqli_fetch_assoc( $result )) 
			{
				$response[] = $row;
			}			
		}
		
		return $response;
            }
            //cms data code end

	//language content start
	public static function Getlanguagekeyword()
                {
                    $query = "SELECT uid,keyword,value ";
            $query.= "from `language_keyword` where language_uid='1' AND `active`=1 ORDER BY `value`";
                  
                        $result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response[] = $row;
				}
			}
		return $response;
                    
                }
                
                public static function languagecontentcreate($post=array())
                {
                    $response=FALSE;
                    
					$query = "INSERT INTO `language_keyword` ";
					$query.= "SET ";
					$query.= "`language_uid`='".$post['language']."',";
					$query.= "`keyword`='".$post['languagekeyword']."', ";
					$query.= "`value`='" .$post['name']. "'";

					// run sql query
					$wisdomtag_uid = db::insert( $query );

					if(isset($wisdomtag_uid) && is_numeric($wisdomtag_uid) && $wisdomtag_uid > 0) {
						
						return $response=true;
						//return $wisdomtag_uid ;
						
					}
                    
                }
	public static function getlanguagecontent()
                {
                    $response = false;
		
		$query = "SELECT ";		
		$query.= "* ";		
		$query.= "FROM ";
		$query.= "`language_keyword`";
                $query.= ' WHERE  `language_uid`="1"';
		$result = db::query( $query );
		//	echo $query;
		if( $result && isset($result->num_rows) && $result->num_rows ) 
		{
			$response = array();
			while($row 	= mysqli_fetch_assoc( $result )) 
			{
				$response[] = $row;
			}			
		}
		
		return $response;
   
                    
                }
		public static function GetLanguageContentByUid($uid = false)
		{
			$response = false;

			$query = "SELECT ";		
			$query.= "* ";		
			$query.= "FROM ";
			$query.= "`language_keyword`";
			$query.= ' WHERE `uid`="'.$uid.'"';
			$result = db::query( $query );

			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				$row = mysqli_fetch_assoc( $result );
				$response = $row;
			}
			
			return $response;
		}
		public static function GetAllLanguageContentByKeyword($langUid = false,	$keyword = false)
		{
			$response = false;

			$query = "SELECT ";		
			$query.= "* ";		
			$query.= "FROM ";
			$query.= "`language_keyword`";
			$query.= ' WHERE `language_uid`="'.$langUid.'" AND `keyword`="'.$keyword.'" AND `active`=1';
			$result = db::query( $query );

			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				$row = mysqli_fetch_assoc( $result ); 
				$response = $row;
			}
			
			return $response;
		}
		public static function getLanguageName($languageUid = '')
		{
			$response = '';
			if(false != $languageUid)
			{
				$query = "SELECT * FROM `language` ";
				$query.= "WHERE `uid`='".$languageUid."' LIMIT 1";
				$result = db::query( $query );
				if( $result && isset($result->num_rows) && $result->num_rows ) {
					$row = mysqli_fetch_assoc( $result );
					$response = $row['name'];
				}
			}

			return $response;
		}
		public static function UpdateLanguageContent($posts = array())
		{
			$response = false;
			if(!empty($posts) && $posts['keyword']!='')
			{
				$response = true;
				foreach ($posts['langValue'] as $languageUid=>$languageValue)
				{
					$select = "SELECT `uid` FROM `language_keyword`";
					$select.= "WHERE `keyword`='".$posts['keyword']."' ";
					$select.= "AND `language_uid`='".$languageUid."' ";
					$getrow = db::query( $select );
					if( $getrow && isset($getrow->num_rows) && $getrow->num_rows ) {
						$query = "UPDATE `language_keyword` SET value='".$languageValue."' ";
						$query.= "WHERE `keyword`='".$posts['keyword']."' ";
						$query.= "AND `language_uid`='".$languageUid."' ";
						$query.= "AND `active`='1'";

						$result = db::query( $query );
						if(!$result) {
							$response = false;
							break;
						}
					}
					else
					{
						$query = "INSERT INTO `language_keyword` ";
						$query.= "SET ";
						$query.= "`language_uid`='".$languageUid."',";
						$query.= "`keyword`='".$posts['keyword']."', ";
						$query.= "`value`='" .$languageValue. "'";

						$result = db::query( $query );
						if(!$result) {
							$response = false;
							break;
						}
					}
				}
			}
			return $response;
		}
	//end of language content

	//language code  start
                
                
                public static function getLanguagecount(){
		$response = false;
		
			$query = "SELECT ";		
			$query.= "COUNT(`uid`) as total ";		
			$query.= "FROM ";
			$query.= "`language` ";
			$query.= "WHERE ";
			$query.= "`active`= '1' ";
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response = $row['total'];
				}
			}
			
		return $response;
	}
	
	public static function GetHomepageLanguage ( $limit = false ) {
		$response = array();
		
			$query = "SELECT * ";
			$query.= "FROM `language` ";
			$query.= "order by `uid` desc";
			if(false != $limit)
			{
				$query.= " LIMIT ".$limit;
			}
			
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response[] = $row;
				}
			}
		return $response;
	}

	public static function Getlaguagetranslate () {
		$response = array();
		
			$query = "SELECT * ";
			$query.= "FROM `language` ";
			$query.= "order by `uid`";
			if(false != $limit)
			{
				$query.= " LIMIT ".$limit;
			}
			
			$result = db::query( $query );
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				while($row = mysqli_fetch_assoc( $result )) 
				{
					$response[$row['uid']] = $row['code'];
				}
			}
                        
		return $response;
	}
        
        public static function Languagecreate($post=array()){
		$response = false;
		// lets filter user input, to project websit from XSS injection attacts, 
		// in following function I am passing entire post to function, so it will filer all input and return clean value
		$post = format::process_secure($_POST);

		
		// prepare insert sql query
		$query = "INSERT INTO `language`";
		$query.= "SET";
		$query.= "`code`='".$post['lan_short']."',";
		$query.= "`name`='".$_POST['lang']."', ";
		$query.= "`active`='" .$post['status']. "'";
		;
//		echo $query;
//                exit;
		// run sql query
		$partner_uid = db::insert( $query );
		
		if(isset($partner_uid) && is_numeric($partner_uid) && $partner_uid > 0) {
			if( isset($post['keywords']) && is_array( $post['keywords'] ) && count( $post['keywords'] ) ) {
				wisdom_keywords_relation::save_wisdom_keywords( $post['keywords'],  $wisdom_uid);
			}
		}
		return $response=true;
		return $partner_uid ;
	}

	public static function EditLanguage($lang_id){
		$response = false;
		
		$query = "SELECT ";		
		$query.= "* ";		
		$query.= "FROM ";
		$query.= "`language` ";
		$query.= "WHERE `uid`= '$lang_id' ";
		$query.= "LIMIT 1";
		$result = db::query( $query );
		
		if( $result && isset($result->num_rows) && $result->num_rows ) 
		{
			$response = array();
			while($row 	= mysqli_fetch_assoc( $result )) 
			{
				$response[] = $row;
			}			
		}
		
		return $response;
	}
	
	public static function UpdateLanguage($post=array())
	{
                $response = false;
		//print_r($post);
		//print_r($files);
               // exit;
		
		
		$query = "update";		
		$query.= "`language`";		
		$query.= "set";
		$query.= '`code`="'.$post[lan_short].'"';
		$query.= ',`name`="'.$post[lang].'"';
		$query.= ',`active`="'.$post[status].'"';
		$query.= "WHERE `uid`= '$post[language_uid]'";
                $result = db::query( $query );
		if($result)
                {
                    $response=true;
                }
                return $response;
              
	}
        
        public static function Languagedelete($post=array())
        {
            $responce=false;
               $query = "DELETE FROM ";		
		$query.= "`language` ";	
		$query.= "WHERE `uid`= $post";
                $result = db::query( $query );
                if($result)
                {
                $responce=true;
                }
                return $responce;
                
        }
        //language code end

	//get wisdom by uid
        
       public static function wisdomedit( $post=array(), $files=array() ){
		$response = false;
		
		// lets filter user input, to project websit from XSS injection attacts, 
		// in following function I am passing entire post to function, so it will filer all input and return clean value
		$post = format::process_secure($post);
		
		// lets upload images
		$image_1 = '';
		if( isset($files['image_1']) && !empty($files['image_1']['name']) && format::is_valid_image_upload_file($files['image_1']) === true  ) {
			$arrExplode = explode(".",$files['image_1']['name']);
			$extension = end( $arrExplode );
			$image_1 = "image_1_" . time() . "." . $extension;
			move_uploaded_file( $files['image_1']['tmp_name'], config::sys('uploads') . 'wisdom/' . $image_1);
		}
		
		$image_2 = '';
		if( isset($files['image_2']) && !empty($files['image_2']['name']) && format::is_valid_image_upload_file($files['image_2']) === true  ) {
			$arrExplode = explode(".",$files['image_2']['name']);
			$extension = end( $arrExplode );
			$image_2 = "image_2_" . time() . "." . $extension;
			move_uploaded_file( $files['image_2']['tmp_name'], config::sys('uploads') . 'wisdom/' . $image_2);
		}
		
		$image_3 = '';
		if( isset($files['image_3']) && !empty($files['image_3']['name']) && format::is_valid_image_upload_file($files['image_3']) === true  ) {
			$arrExplode = explode(".",$files['image_3']['name']);
			$extension = end( $arrExplode );
			$image_3 = "image_3_" . time() . "." . $extension;
			move_uploaded_file( $files['image_3']['tmp_name'], config::sys('uploads') . 'wisdom/' . $image_3);
		}
		$countinent_uid = '';
		$continent_code = Country::GetContinentUidByCountryuid($post['country_uid']);
		$countinent_uid = Continent::GetContinentByCode($continent_code);
		$is_for_kids = isset($post['is_for_kids']) ? 1 : 0;
		// prepare insert sql query
		$query = "UPDATE `wisdom` ";
		$query.= "SET ";
		//$query.= "`user_uid`='". $_SESSION['userid'] ."',";
		$query.= "`continent_uid`='" . db::escape( $countinent_uid ) . "', ";
		$query.= "`name`='" . db::escape( $post['name'] ) . "', ";
		$query.= "`country_uid`='" . db::escape( $post['country_uid'] ) . "', ";
		$query.= "`region`='" . db::escape( $post['region'] ) . "', ";
		$query.= "`description`='" . db::escape( nl2br($post['description'] )) . "', ";
		if($image_1!='')
		{
			$query.= "`image_1`='" . db::escape( $image_1 ) . "', ";
		}
		if($image_2!='')
		{
			$query.= "`image_2`='" . db::escape( $image_2 ) . "', ";
		}
		if($image_3!='')
		{
			$query.= "`image_3`='" . db::escape( $image_3 ) . "', ";
		}
		$query.= "`video_link`='" . db::escape( $post['video_link'] ) . "', ";
		$query.= "`video_description`='" . db::escape( nl2br($post['video_description'] )) . "', ";
		$query.= "`is_for_kids`='" . $is_for_kids . "', ";
		$query.= "`active`='".$post['status']."' ";
		$query.= "WHERE uid='".db::escape($post['uid'])."'";
		
		// run sql query
		$wisdom = db::query( $query );
		
		if($wisdom)  {
			if( isset($post['keywords']) && is_array( $post['keywords'] ) && count( $post['keywords'] ) ) {
				wisdom_keywords_relation::remove_wisdom_keywords_by_WisdomUid($post['uid']);				
					
				wisdom_keywords_relation::save_wisdom_keywords( $post['keywords'],  $post['uid']);
			
				$response = true;
			}
		}
                return $response;
       }
       
       public static function GetWisdomByUid ( $uid = FALSE ) {
		$response = false;
		if(false != $uid)
		{
			$query = "SELECT ";		
			$query.= "* ";		
			$query.= "FROM ";
			$query.= "`wisdom` ";
			$query.= "WHERE ";
			$query.= "`uid`= '".$uid."' ";
//			$query.= "AND `active`= '1' ";
			$query.= "LIMIT 1";
			$result = db::query( $query );
			
			if( $result && isset($result->num_rows) && $result->num_rows ) 
			{
				$row = mysqli_fetch_assoc( $result ); 
				$response = $row;
			}
		}
		return $response;
	}
}
?>