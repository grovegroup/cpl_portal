<?php

class mail {

	public function send_mail($array=array()){
		header('Content-type: text/json');

		$from = "workstation@mystream.co.uk";
		$to = "workstation@mystream.co.uk";
		//$to = $from;
		$subject = (isset($_SESSION['mail_title'])?$_SESSION['mail_title']:"")."";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= "From: ".(isset($array['from']) ? $array['from'] : $from) . "\r\n";
		$tpl = make::tpl('public/mail/email.table')->assign($array);

		return mail((isset($array['to']) ? $array['to'] : $to),$subject,$tpl->get_content(),$headers);
	}
}
?>