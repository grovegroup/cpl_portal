<?php
/**
 * Router
 */
class router {

	/**
	 * [controller description]
	 * @return [type] [description]
	 */
	private static function controller () {

		/**
		 * Set the default handler to the index controller
		 */
		config::set('controller',config::sys('default'));

		/**
		 * Find the Controller From the Requested URL
		 */
		$paths = config::req('paths');
//print_R($paths);exit;
		if(count($paths) > 0) {
			config::set('controller',strtolower(trim(preg_replace('/[^a-zA-Z\d_\-]/','',$paths[0]))));
		}

		$controllerPath = config::get('controller');

		$controller	= config::sys('application').'controllers/'.$controllerPath.'.php';
		$not_found	= config::sys('application').'controllers/'.config::sys('404').'.php';

		if(file_exists($controller) || (file_exists($not_found))) {
			
			/**
			 * Get the Framework and Application Controller Classes
			 */
			include (config::sys('framework').'controller.php');
			include (file_exists($controller) ? $controller : $not_found);

			$list		= get_declared_classes();
			$instance	= new $list[count($list)-1]();
		} else {
			self::stop('no controller:'.$controller);
		}
	}

	/**
	 * [stop description]
	 * @param  string $message [description]
	 * @return [type]          [description]
	 */
	private static function stop ($message = '') {
		echo 'stop:'.$message;
	}

	/**
	 * [initialise description]
	 * @return [type] [description]
	 */
	public static function initialise () {
//print_r(getcwd());exit;
               // ini_set('session.save_path',getcwd(). '/session');
		session_start();

		/**
		 * Determine the protocol to use
		 */
		$p = 'SERVER_PROTOCOL';
		$protocol = (isset($_SERVER[ $p ]) && 4 < strlen($_SERVER[ $p ])) ? strtolower(substr($_SERVER[ $p ],0,5)) : '';
		config::req('protocol','http'.(('https' === $protocol || 143 === (int)$_SERVER['SERVER_PORT']) ? 's' : '').'://');
		// if ssl doesn't get picked up try this below
		//config::set('protocol','http'.((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '') . '://');

		/**
		 * Capture the Raw Request
		 */
		config::req('raw',$_SERVER['REQUEST_URI']);
		$paths = array_values(explode('/', str_replace(config::get('host'), '', config::req('raw'))));

//print_r($paths);
		foreach($paths as $index=>$path) {
			if(strlen($path) < 1) {
				unset($paths[$index]);
			}
		}
                
		array_shift($paths);                
		//array_shift($paths);
               // array_shift($paths);

               //print_r($paths);exit;
		config::req('paths',array_values($paths));

		/**
		 * Call the appropriate Controller
		 */
		self::controller();
	}
}

?>
