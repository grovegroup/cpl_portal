<?php

/**
 *
 */
class session {

	/**
	 * [__construct description]
	 */
	public function __construct() {

	}

	/**
	 * [$hash description]
	 * @var array
	 */
	private static $hash = array();

	/**
	 * [get description]
	 * @param  boolean $key [description]
	 * @return [type]       [description]
	 */
	public static function get($key= false){
	    return (false !== $key ? (isset($_SESSION[ $key ]) ? $_SESSION[ $key ] : false) : false);
	}

	/**
	 * [get_value description]
	 * @param  boolean $key [description]
	 * @return [type]       [description]
	 */
	public static function get_value($key= false){ 
	    return (false !== $key ? (isset($_SESSION[ $key ]['value']) ? $_SESSION[ $key ]['value'] : false) : false);
	}

}

?>