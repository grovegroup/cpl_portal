<?php

/**
 * 
 */
class response {

	/**
	 * [$arrData description]
	 * @var array
	 */
	public $arrData	= array ();

	/**
	 * [$message description]
	 * @var string
	 */
	public $message	= '';

	/**
	 * [$status description]
	 * @var boolean
	 */
	public $status	= false;

	/**
	 * [__construct description]
	 * @param array   $arrData [description]
	 * @param string  $message [description]
	 * @param boolean $status  [description]
	 */
	public function __construct($arrData=array(),$message='',$status=false){
		$this->create($arrData,$message,$status);
	}

	/**
	 * [create description]
	 * @param  array   $arrData [description]
	 * @param  string  $message [description]
	 * @param  boolean $status  [description]
	 * @return [type]           [description]
	 */
	public function create($arrData=array(),$message='',$status=false) {
		$this->arrData	= $arrData;
		$this->message	= $message;
		$this->status	= $status;
	}

}

?>