<?php

/**
 * 
 */
class format {

	/**
	 * [__construct description]
	 */
	public function __construct() {

	}

	/**
	 * [loop description]
	 * @param  string $tpl     [description]
	 * @param  array  $arrData [description]
	 * @return [type]          [description]
	 */
	public static function loop($tpl='',$arrData=array()){
		$strResponse = '';
		if('' !== $tpl && is_array($arrData) && 0 < count($arrData)) {
			$arrTpls = array();
			foreach ($arrData as $arrContent) {
				$arrTpls[] = make::tpl($tpl)->assign($arrContent)->get_content();
			}
			$strResponse = implode("\n",$arrTpls);
		}
		return $strResponse;
	}

	/**
	 * [to_email description]
	 * @param  string $text [description]
	 * @return [type]       [description]
	 */
	public static function to_email($text = '') {
		return '<a href="mailto:' . $text . '">' . $text . '</a>';
	}

	/**
	 * [encrypt description]
	 * @param  string $text [description]
	 * @param  string $salt [description]
	 * @return [type]       [description]
	 */
	public static function encrypt($text = "", $salt = "marqmerocks") {
		if (!function_exists('mcrypt_encrypt') || !function_exists('mcrypt_decrypt')) {
			return $text;
		}
		return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
	}

	/**
	 * [decrypt description]
	 * @param  string $text [description]
	 * @param  string $salt [description]
	 * @return [type]       [description]
	 */
	public static function decrypt($text = "", $salt = "marqmerocks") {
		if (!function_exists('mcrypt_encrypt') || !function_exists('mcrypt_decrypt')) {
			return $text;
		}
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
	}

	/**
	 * [is_valid_date description]
	 * @param  string  $date [description]
	 * @return boolean       [description]
	 */
	public static function is_valid_date( $date = '' ) {

		$continue = false;

		$month	= 0;
		$day	= 0;
		$year	= 0;

		$parts = explode('-',$date);

		if(count($parts) == 3) {
			$month	= $parts[1];
			$day	= $parts[2];
			$year	= $parts[0];

			$continue = true;
		}

		return ($continue ? checkdate($month,$day,$year) : false);
	}

	/**
	 * [is_valid_table_name description]
	 * @param  string  $table_name [description]
	 * @return boolean             [description]
	 */
	public static function is_valid_table_name( $table_name = '' ) {
		return (preg_replace('/[^a-z_]/','',strtolower($table_name)) == $table_name && strlen($table_name) > 0);
	}

	/**
	 * [is_valid_sid description]
	 * @param  string  $sid [description]
	 * @return boolean      [description]
	 */
	public static function is_valid_sid( $sid = '' ) {
		return (preg_replace('/[^A-Z0-9\-]/','',$sid) == $sid && strlen($sid) > 0);
	}

	/**
	 * [to_error description]
	 * @param  string $text [description]
	 * @return [type]       [description]
	 */
	public static function to_error($text='') {
		return '<div class="message"><p class="error">'.$text.'</p></div>';
	}

	/**
	 * [to_success description]
	 * @param  string $text [description]
	 * @return [type]       [description]
	 */
	public static function to_success($text='') {
		return '<div class="message"><p class="success">'.$text.'</p></div>';
	}

	/**
	 * [to_warning description]
	 * @param  string $text [description]
	 * @return [type]       [description]
	 */
	public static function to_warning($text='') {
		return '<div class="message"><p class="notice">'.$text.'</p></div>';
	}

	/**
	 * [to_info description]
	 * @param  string $text [description]
	 * @return [type]       [description]
	 */
	public static function to_info($text='') {
		return '<div class="message"><p class="info">'.$text.'</p></div>';
	}

	/**
	 * [to_status_icon description]
	 * @param  string $status [description]
	 * @return [type]         [description]
	 */
	public static function to_status_icon($status='') {
		$icon = $status;

		switch($status){
			case 'Running':
			case '1':
			case 'Yes':
				$icon = '<img src="/img/icon_online.png" alt="Running" />';
			break;
			case 'Offline':
			case '0':
			case 'No':
				$icon = '<img src="/img/icon_offline.png" alt="Offline" />';
			break;
			case 'Restart Now':
				$icon = '<img src="/img/icon_restarting.png" alt="Restart Now" />';
			break;
			case 'Schedule Restart':
				$icon = '<img src="/img/icon_scheduled.png" alt="Schedule Restart" />';
			break;
		}

		return $icon;
	}

	/**
	 * [to_enabled description]
	 * @param  string $status [description]
	 * @return [type]         [description]
	 */
	public static function to_enabled($status='') {
		$text = $status;

		switch($status) {
			case '1':
				$text = '<span class="ajax-trigger ajax-enabled" data-field="status" data-value="0" title="Click to Disable">Disable</span>';
			break;
			case '0':
				$text = '<span class="ajax-trigger ajax-disabled" data-field="status" data-value="1" title="Click to Enable">Enable</span>';
			break;
		}

		return $text;
	}

	/**
	 * [to_gal description]
	 * @param  string $gal [description]
	 * @return [type]      [description]
	 */
	public static function to_gal($gal='') {
		$text = $gal;

		switch($gal) {
			case '1':
				$text = '<span class="ajax-trigger ajax-shown" data-field="hide_from_gal" data-value="0" title="Click to Show">Show</span>';
			break;
			case '0':
				$text = '<span class="ajax-trigger ajax-hidden" data-field="hide_from_gal" data-value="1" title="Click to Hide">Hide</span>';
			break;
		}

		return $text;
	}

	/**
	 * [to_public_folder_admin description]
	 * @param  string $pfa [description]
	 * @return [type]      [description]
	 */
	public static function to_public_folder_admin($pfa='') {
		$text = $pfa;

		switch($pfa) {
			case '1':
				$text = '<span class="ajax-trigger ajax-is-admin" data-field="public_folder_admin" data-value="0" title="Click to Remove">Remove</span>';
			break;
			case '0':
				$text = '<span class="ajax-trigger ajax-is-not-admin" data-field="public_folder_admin" data-value="1" title="Click to Add">Add</span>';
			break;
		}

		return $text;
	}

	/**
	 * [to_ratio_icon description]
	 * @param  integer $dividend [description]
	 * @param  integer $divisor  [description]
	 * @return [type]            [description]
	 */
	public static function to_ratio_icon($dividend=0,$divisor=0) {
		$percentage = 0;

		if(0 !== (float)$divisor) {
			$percentage = number_format(100*$dividend/$divisor,0);
		}

		return '<span class="ratio" data-proportion="'.$percentage.'" data-numerator="'.$dividend.'" data-denominator="'.$divisor.'"><span class="position"></span><span class="tooltip">'.$dividend.'/'.$divisor.'</span></span>';
	}

	// Format: [disk] - [size]GB - [size]GB
	/**
	 * [to_ratio_disks description]
	 * @param  array  $arrDisks [description]
	 * @return [type]           [description]
	 */
	public static function to_ratio_disks($arrDisks = array()) {
		$icons = array();

		if(count($arrDisks) > 0) {
			foreach($arrDisks as $disk) {
				$split = explode('-',$disk);
				$drive = trim($split[0]);
				$max = 0;
				$used = 0;
				$percentage = 0;
				$parts = preg_match_all('/([\d]+)/',$disk,$matches);
				if(count($matches)==2) {
					$max = (int)$matches[0][0];
					$used = (int)$matches[0][1];
					if($max > 0) {
						$percentage = number_format(100*($max > 0 ? $used / $max:0),2);
					}
				}
				$icons[] = '<span class="ratio" data-proportion="'.$percentage.'" data-numerator="'.$used.'" data-denominator="'.$max.'"><span class="position"></span><span class="tooltip">'.$drive.': '.$used.'GB/'.$max.'GB</span></span>';
			}
		}

		return implode('',$icons);
	}

	/**
	 * [to_space_used description]
	 * @param  integer $used     [description]
	 * @param  integer $capacity [description]
	 * @return [type]            [description]
	 */
	public static function to_space_used($used=0,$capacity=0) {
		$text = '';

		if($capacity === 0) {
			$text.= '<span class="ratio-entry">';
			$text.= '<span class="used">'.number_format((float)$used,2).'GB </span>';
			$text.= '<span class="ratio"><span class="position" style="width:0%"></span></span>';
			$text.= '<span class="capacity"> ? GB</span>';
			$text.= '</span>';
		} else if($capacity==='Un') {
			$percent = ceil(100*$used/25);
			$text.= '<span class="ratio-entry">';
			$text.= '<span class="used">'.number_format((float)$used,2).'GB </span>';
			$text.= '<span class="ratio"><span class="position '.($percent >= 90 ? 'high-position' : ($percent >= 75 && $percent <= 89 ? 'medium-position' : 'low-position')).'" style="width:'.$percent.'%"></span></span>';
			$text.= '<span class="capacity"> 25GB</span>';
			$text.= '</span>';
		} else if(0 < (int)$capacity) {
			$percent = ceil(100*$used/(float)$capacity);
			$text.= '<span class="ratio-entry">';
			$text.= '<span class="used">'.number_format((float)$used,2).'GB </span>';
			$text.= '<span class="ratio"><span class="position '.($percent >= 90 ? 'high-position' : ($percent >= 75 && $percent <= 89 ? 'medium-position' : 'low-position')).'" style="width:'.$percent.'%"></span></span>';
			$text.= '<span class="capacity"> '.$capacity.'GB</span>';
			$text.= '</span>';
		}

		return $text;
	}

	// iOS/6.0.1 (10A523) dataaccessd/1.0;iPhone;03/07/2013 17:40:19
	// into
	// [ icon ] iOS/6.0.1 (10A523) dataaccessd/1.0
	/**
	 * [to_mobile description]
	 * @param  string $device [description]
	 * @return [type]         [description]
	 */
	public static function to_mobile($device = '') {
		$string = '';

		if(strlen($device) > 0) {
			$parts = explode(';',$device);
			if(count($parts) == 3) {
				$icon = self::to_device_icon($parts[1], $parts[0]);
				$string = $icon . '<span class="device-details" title="'.$parts[0].'">'.$parts[1].'</span> <span class="time" title="'.$parts[0].'">'.date('d/m/Y H:i:s',strtotime($parts[2])).'</span>';
			}
		}

		return $string;
	}

	/**
	 * [to_device_icon description]
	 * @param  string $make  [description]
	 * @param  string $title [description]
	 * @return [type]        [description]
	 */
	public static function to_device_icon($make = '', $title='') {

		$text = '';

		switch($make){
			case 'iPhone':
				$text = '<img src="/img/iphone.png" alt="'.$make.'" title="'.$title.'" height="24" />';
			break;
			case 'iPad':
				$text = '<img src="/img/ipad.png" alt="'.$make.'" title="'.$title.'" height="24" />';
			break;
			case 'Android':
				$text = '<img src="/img/android.png" alt="'.$make.'" title="'.$title.'" height="24" />';
			break;
			case 'htcace':
			case 'htcbravo':
				$text = '<img src="/img/htc.png" alt="'.$make.'" title="'.$title.'" height="24" />';
			break;
			case 'SAMSUNGGTI9300':
			case 'SAMSUNGGTI9100':
				$text = '<img src="/img/samsung.png" alt="'.$make.'" title="'.$title.'" height="24" />';
			break;
			case 'PlayBook':
				$text = '<img src="/img/blackberry.png" alt="'.$make.'" title="'.$title.'" height="24" />';
			break;
			case 'WP8':
				$text = '<img src="/img/windows.png" alt="'.$make.'" title="'.$title.'" height="24" />';
			break;
		}

		return $text;

	}

	/**
	 * [array_to_select description]
	 * @param  array   $arrData       [description]
	 * @param  string  $valueKey      [description]
	 * @param  string  $displayKey    [description]
	 * @param  array   $selectIfFound [description]
	 * @param  boolean $pleaseSelect  [description]
	 * @return [type]                 [description]
	 */
	public static function array_to_select($arrData=array(),$valueKey='',$displayKey='',$selectIfFound=array(),$pleaseSelect=true) {
		$strSelect = "\n";

		if($pleaseSelect) {
			$strSelect.='<option value="">Please Select</option>'."\n";
		}

		$checkSelected = (is_array($selectIfFound) && 0 < count($selectIfFound)) ? true : false;

		if(is_array($arrData) && 0 < count($arrData)) {
			foreach($arrData as $key=>$value){
				$strSelect.='<option value="'.$value[$valueKey].'"';
				$selected = false;
				if($checkSelected){
					foreach($selectIfFound as $match){
						if($match==$value[$valueKey] || $key==$match){
							$selected = true;
						}
					}
				}
				if($selected){
					$strSelect.=' selected="selected"';
				}
				$strSelect.='>'.$value[$displayKey].'</option>'."\n";
			}
		}

		return $strSelect;
	}

	/**
	 * [to_suffix description]
	 * @param  [type] $i [description]
	 * @return [type]    [description]
	 */
	public static function to_suffix($i){
	    $mod = $i % 10;
	    if ($mod === 1 && $i !== 11) {
	    $ordinalSuffix = 'st';
	    } else if ($mod === 2 && $i !== 12) {
	    $ordinalSuffix = 'nd';
	    } else if ($mod === 3 && $i !== 13) {
	    $ordinalSuffix = 'rd';
	    } else {
	    $ordinalSuffix = 'th';
	    }
	    return $ordinalSuffix;
	}

	/**
	 * [to_print_r variable]
	 * @param  [any] $i [description]
	 * @return [formated data] [description]
	 */
	public static function my_print_r($array,$var_dump = false){
	    echo "<pre>";
	    switch ($var_dump) {
	    	case true:
	    		var_dump($array);
	    		break;
	    	
	    	default:
	    		print_r($array);
	    		break;
	    }
	    echo "</pre>";
	}
	
	/**
   * sanitize()
   * 
   * @param mixed $string
   * @param bool $trim
   * @return
   */
  public static function sanitize($string, $trim = false, $int = false, $str = false) {
      $string = filter_var($string, FILTER_SANITIZE_STRING);
      $string = trim($string);
      $string = stripslashes($string);
      $string = strip_tags($string);
      $string = str_replace(array(
          '�',
          '�',
          '�',
          '�'), array(
          "'",
          "'",
          '"',
          '"'), $string);

      if ($trim)
          $string = substr($string, 0, $trim);
      if ($int)
          $string = preg_replace("/[^0-9\s]/", "", $string);
      if ($str)
          $string = preg_replace("/[^a-zA-Z\s]/", "", $string);

      return $string;
  }

  /**
   * cleanSanitize()
   * 
   * @param mixed $string
   * @param bool $trim
   * @return
   */
  public static function cleanSanitize($string, $trim = false, $end_char = '&#8230;') {
      $string = cleanOut($string);
      $string = filter_var($string, FILTER_SANITIZE_STRING);
      $string = trim($string);
      $string = stripslashes($string);
      $string = strip_tags($string);
      $string = str_replace(array(
          '�',
          '�',
          '�',
          '�'), array(
          "'",
          "'",
          '"',
          '"'), $string);

      if ($trim) {
          if (strlen($string) < $trim) {
              return $string;
          }

          $string = preg_replace("/\s+/", ' ', str_replace(array(
              "\r\n",
              "\r",
              "\n"), ' ', $string));

          if (strlen($string) <= $trim) {
              return $string;
          }

          $out = "";
          foreach (explode(' ', trim($string)) as $val) {
              $out .= $val . ' ';

              if (strlen($out) >= $trim) {
                  $out = trim($out);
                  return (strlen($out) == strlen($string)) ? $out : $out . $end_char;
              }
          }
      }
      return $string;
  }
  
  	/**
	 * [secure process_secure]
	 * @param  array  $ArrVariable [description]
	 * @return array  $ArrVariable  [description]
	 * @auther swyam joshi
	 */
	
	public static function process_secure( $ArrVariable = array() ) {
		
		if(isset($ArrVariable) && is_array($ArrVariable) && count($ArrVariable)) {
			foreach($ArrVariable as $index => $value ) {
				// if array index is type of Array than lets use again that function to get result
				if(isset($value) && is_array($value) && count($value)) {
					$ArrVariable[$index] = format::process_secure( $value  );
				} else {
					$ArrVariable[$index] = format::sanitize( $value  );
				}
			}
		}
		return $ArrVariable;
		
	}
	
	public static function is_valid_youtube_url ( $url = '' ){
		
		if(!empty($url)) {
			$url = str_replace('https://','http://',$url);
		}
		
		// Regular Expression (the magic).
		$youtube_regexp = "/^http:\/\/(?:www\.)?(?:youtube.com|youtu.be)\/(?:watch\?(?=.*v=([\w\-]+))(?:\S+)?|([\w\-]+))$/";
		
		// Match a URL.
		preg_match($youtube_regexp, $url, $matches);
		
		// Remove empty values from the array (regexp shit).
		$matches = array_filter($matches, function($var) {
			return($var !== '');
		});
		
		// If we have 2 elements in array, it means we got a valid url!
		// $matches[2] is the youtube ID!
		if (sizeof($matches) == 2) {
			return true;
			//echo end($matches);
			//var_dump($matches);
		} else {
			return false;
		}
	}
	
	public static function is_valid_image_upload_file( $arrFile = array() ) {
		$valid_mime_types = array(
			"image/gif",
			"image/png",
			"image/jpeg",
			"image/pjpeg",
		);
		
		 
		// Check that the uploaded file is actually an image
		// and move it to the right folder if is.
		if (in_array($arrFile["type"], $valid_mime_types)) {
			return true;
		} else {
			return false;
		}
	}
	
	//image 
	
	public static function to_image($attributes = array()) {

		$attribute_string = array();
		if (is_array($attributes) && count($attributes) > 0) {
			foreach ($attributes as $attribute => $value) {
				$attribute_string[] = $attribute . '="' . $value . '"';
			}
		}
		$attribute_string = implode(' ', $attribute_string);

		$html = '<img ' . ((strlen($attribute_string) > 0) ? ' ' . $attribute_string : '') . ' />';
		return $html;
	}
	
}

?>