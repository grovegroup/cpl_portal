<?php

/**
 * 
 */
class validation {

	/**
	 * [is_phone description]
	 * @param  string  $phone [description]
	 * @return boolean        [description]
	 */
	public static function is_phone($phone = "") {
		$phone = preg_replace('/[^\d]/','',$phone);
		return (isset($phone[8]) && !isset($phone[21]))?true:false;
	}

	/**
	 * [is_email description]
	 * @param  string  $email [description]
	 * @return boolean        [description]
	 */
	public static function is_email($email = "") {
		return (filter_var($email,FILTER_VALIDATE_EMAIL))?true:false;
	}

	/**
	 * [is_url description]
	 * @param  string  $url [description]
	 * @return boolean      [description]
	 */
	public static function is_url($url = "") {
		return (filter_var($url,FILTER_VALIDATE_URL))?true:false;
	}

	/**
	 * [is_ip_address description]
	 * @param  string  $ip_address [description]
	 * @return boolean             [description]
	 */
	public static function is_ip_address($ip_address = "") {
		return (filter_var($value,FILTER_VALIDATE_IP))?true:false;
	}

	/**
	 * [is_percentage description]
	 * @param  string  $value [description]
	 * @return boolean        [description]
	 */
	public static function is_percentage($value = "") {
		return (is_numeric($value) && $value > 0 && $value < 100);
	}

	/**
	 * [validateDate description]
	 * @param  [type] $date   [description]
	 * @param  string $format [description]
	 * @return [type]         [description]
	 */
	function validateDate($date, $format = 'd/m/Y')	{
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}
	
	/**
	 * [is_digits description]
	 * @param  string  $value [description]
	 * @return boolean        [description]
	 */
	function is_digits($value = ""){
	    return (ctype_digit(str_replace(",","",$value)));
	}

	// validate date greater than 18
	/**
	 * [validate_age description]
	 * @param  [type]  $year  [description]
	 * @param  [type]  $month [description]
	 * @param  [type]  $date  [description]
	 * @param  integer $age   [description]
	 * @return [type]         [description]
	 */
	function validate_age($year, $month, $date, $age = 18)	{
	 $date = (($year!='')?date("Y",strtotime($year)):'')."/".(($month!='')?date("m",strtotime($month)):'')."/".(($date!='')?date("d",strtotime($date)):'');
	 
	  // $date can be UNIX_TIMESTAMP or just a string-date.
	  if(is_string($date)){
	    $date = strtotime($date);
	  }

	  // check
	  // 31536000 is the number of seconds in a 365 days year.
	  if(!$date) {
	  	return false;
	  }
	  
	  if(time() - $date < $age * 31536000) {
	    return false;
	  }
	  return true;

	}
	
}

?>