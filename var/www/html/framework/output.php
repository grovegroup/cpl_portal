<?php

/**
 *  class output.php
 */
class output {

	/**
	 * [redirect description]
	 * @param  [type] $url [description]
	 * @return [type]      [description]
	 */
	public static function redirect($url) {
		if (!headers_sent($filename, $linenum)) {
			header("Status: 200");
			header('Location: ' . $url);
			exit();
		} else {
			echo "Headers already sent in $filename on line $linenum\n";
		}
	}

	/**
	 * [send description]
	 * @param  [type] $usecached     [description]
	 * @param  [type] $content       [description]
	 * @param  [type] $mime          [description]
	 * @param  [type] $size          [description]
	 * @param  [type] $file_modified [description]
	 * @return [type]                [description]
	 */
	public static function send($usecached, $content, $mime, $size, $file_modified) {
		if ($usecached) {
			if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
				$modified_since = $_SERVER['HTTP_IF_MODIFIED_SINCE']; // e.g. If-Modified-Since: Thu, 13 Nov 2008 00:40:58 GMT
				$parts = explode(' ', $modified_since);
				$time = explode(':', $parts[4]);
				$modified_since = mktime($time[0], $time[1], $time[2], format::to_month($parts[2]), $parts[1], $parts[3]);
			} else {
				$modified_since = false;
			}

			if ($modified_since && ($file_modified <= $modified_since)) {
				// if the browser has a cached version of this image, send 304
				header('HTTP/1.1 304 Not Modified');
			} else {
				// set up the last modified time based upon the file time
				header("Pragma: private");
				header("Cache-Control: public, max-age=86400, pre-check=86400");
				header("Expires: " . gmdate('D, d M Y H:i:s', time() + 86400) . "GMT");
				header("Etag: " . '"' . md5($file_modified) . '"');
				header("Last-Modified: " . gmdate('D, d M Y H:i:s', $file_modified) . " GMT");
				header("Content-Type: " . $mime);
				echo $content;
			}
		} else {
			header("Content-Type: " . $mime);
			echo $content;
		}
	}

	/**
	 * [as_html description]
	 * @param  [type]  $object [description]
	 * @param  boolean $cache  [description]
	 * @return [type]          [description]
	 */
	public static function as_html($object = null, $cache = false) {
		if (is_object($object)) {
			$file_content = $object->get_content();
		} else {
			$file_content = $object;
		}
		$usecached = false;
		$file_mime = 'text/html; charset=utf-8';
		self::send($usecached, $file_content, $file_mime, 0, 0);
	}

	/**
	 * [as_xml description]
	 * @param  [type]  $object [description]
	 * @param  boolean $cache  [description]
	 * @return [type]          [description]
	 */
	public static function as_xml($object = null, $cache = false) {
		if (is_object($object)) {
			$file_content = $object->get_content();
		} else {
			$file_content = $object;
		}
		$usecached = false;
		$file_mime = 'text/xml';
		self::send($usecached, $file_content, $file_mime, 0, 0);
	}

	/**
	 * [as_pdf description]
	 * @param  string $content [description]
	 * @return [type]          [description]
	 */
	public static function as_pdf($content = '') {
		// Delegate to PDF plugin
	}

	/**
	 * [as_javascript description]
	 * @param  [type] $object [description]
	 * @return [type]         [description]
	 */
	public static function as_javascript($object = null) {
		$usecached = false;
		$file_modified = filemtime($object->get_source());
		$file_size = filesize($object->get_source());
		$file_mime = 'text/javascript';
		$file_content = $object->get_content(false);

		self::send($usecached, $file_content, $file_mime, $file_size, $file_modified);
	}

	/**
	 * [as_stylesheet description]
	 * @param  [type]  $object    [description]
	 * @param  boolean $usecached [description]
	 * @return [type]             [description]
	 */
	public static function as_stylesheet($object = null, $usecached = false) {
		$file_modified = filemtime($object->get_source());
		$file_size = filesize($object->get_source());
		$file_mime = 'text/css';
		$file_content = $object->get_content(false);

		self::send($usecached, $file_content, $file_mime, $file_size, $file_modified);
	}

	/**
	 * [as_image description]
	 * @param  [type]  $object    [description]
	 * @param  boolean $usecached [description]
	 * @return [type]             [description]
	 */
	public static function as_image($object = null, $usecached = false) {
		$file_modified = filemtime($object->get_source());
		$file_size = filesize($object->get_source());
		$file_mime = 'image/' . $object->get_suffix();
		$file_content = $object->get_content();

		self::send($usecached, $file_content, $file_mime, $file_size, $file_modified);
	}

	/**
	 * [as_email description]
	 * @param  string $to          [description]
	 * @param  string $subject     [description]
	 * @param  string $message     [description]
	 * @param  string $from        [description]
	 * @param  string $receiptname [description]
	 * @param  string $receiptmail [description]
	 * @param  string $cc          [description]
	 * @param  string $bcc         [description]
	 * @return [type]              [description]
	 */
	public static function as_email($to = '', $subject = '', $message = '', $from = '', $receiptname = '', $receiptmail = '', $cc = '', $bcc = '') {
		$header = "Content-Transfer-Encoding: 8bit";
		$header .="\nContent-Type: text/html; charset=utf-8";
		if ($from != '') {
			$header .="\nFrom: " . $from;
		}
		if ($cc != '') {
			$header .= "\nCc: " . $cc;
		}
		if ($bcc != '') {
			$header .= "\nBcc: " . $bcc;
		}

		$message = str_replace(
				array("<br>", "<br />", "<p>"), array("<br>\n", "<br>\n", "<p>\n"), $message
		);

		mail($to, $subject, $message, $header);
	}

}

?>