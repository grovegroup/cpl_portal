<?php

/**
 * Database Connector
 */
class db {

	/**
	 * [$db description]
	 * @type {[type]}
	 */
	private static $db = null;

	/**
	 * [$result description]
	 * @type {[type]}
	 */
	private static $result = null;

	/**
	 * [$logging description]
	 * @type {Boolean}
	 */
	private static $logging = true;


	/**
	 * [$error description]
	 * @type {Boolean}
	 */
	public static $error = false;

	/**
	 * [$query description]
	 * @type {String}
	 */
	public static $query = '';

	/**
	 * [$affected description]
	 * @type {[type]}
	 */
	public static $affected = null;

	/**
	 * [connect description]
	 * @return {[type]} [description]
	 */
	public static function connect () {
		if(!self::$db) {
			$arrConfigDB = parse_ini_file(config::sys('application') . '/'. "config.database.ini");
			self::$db = new mysqli($arrConfigDB['server'], $arrConfigDB['username'], $arrConfigDB['password'], $arrConfigDB['database']);

			if(self::$db->connect_errno > 0){
				die('Unable to connect to database [' . self::$db->connect_error . ']');
			}
			if(!self::$db) {
				//echo 'Could not connect to server';
			} else {
				self::query("SET NAMES 'utf8'");
			}
		}
	}

	/**
	 * [escape description]
	 * @param  string $string [description]
	 * @return [type]         [description]
	 */
	public static function escape($string='') {
		self::connect();
		return self::$db->escape_string($string);

	}

	/**
	 * [query description]
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public static function query ($query) {
		self::reset();
		self::connect();
		self::$query = $query;
		if(!self::$result = self::$db->query($query)){
			self::$error = self::$db->error;
			echo 'There was an error running the query [' . self::$db->error . ']';
		} else {
			self::$affected = self::$db->affected_rows;
		}

		if(self::$logging) {
			$file = 'query_log.txt';
			file_put_contents($file, $query."\n\n-------------------------------------------------------\n", FILE_APPEND | LOCK_EX);
		}

		return self::$result;
	}

	/**
	 * [insert description]
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public static function insert($query) {
		$result = self::query($query);
		if(self::$result !== null) {
			return self::$db->insert_id;
		} else {
			return null;
		}
	}

	/**
	 * [reset description]
	 * @return [type] [description]
	 */
	protected static function reset () {
		if(is_object(self::$result)) {
			self::$result->free();
		}
		self::$result = null;
		self::$error = false;
	}

	/**
	 * [close description]
	 * @return [type] [description]
	 */
	public static function close() {
		if(self::$db) {
			self::$db->close();
		}
	}
}

?>