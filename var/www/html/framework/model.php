<?php

/**
 * 
 */
class model {

	/**
	 * [_add description]
	 * @param array $arrData [description]
	 */
	protected static function _add ( $arrData = array() ) {

		$sql	= "INSERT INTO `".get_called_class()."` (";

		$fields = array_keys($arrData);
		$values = array_values($arrData);

		$sqlFields = array();
		$sqlValues = array();

		foreach($fields as $field) {
			$sqlFields[] = "`".db::escape($field)."`";
		}
		foreach($values as $value) {
			$sqlValues[] = "'".db::escape($value)."'";
		}

		$sql.= implode(", ", $sqlFields);
		$sql.= ") VALUES (";
		$sql.= implode(", ", $sqlValues);
		$sql.= ")";

		return (null !== ($uid = db::insert( $sql ))) ? $uid : false;
	}

	/**
	 * [_addMultiple description]
	 * @param array $arrData [description]
	 */
	protected static function _addMultiple( $arrData = array() ) {
		/**
			$arrData = array(
				array(
					'field1'	=> 'value1',
					...
				), ...
			);
		*/

		$sql	= "INSERT INTO `".get_called_class()."` (";

		$fields = array_keys(current($arrData));
		$values = array_values($arrData);

		$sqlFields = array();
		$sqlValues = array();

		foreach($fields as $field) {
			$sqlFields[] = "`".db::escape($field)."`";
		}
		foreach($values as $index=>$array) {
			$sqlValueTmp = array();
			foreach($array as $field=>$value) {
				$sqlValueTmp[] = "'".db::escape($value)."'";
			}
			$sqlValues[] = "(".implode(", ", $sqlValueTmp).")";
		}

		$sql.= implode(", ", $sqlFields);
		$sql.= ") VALUES ";
		$sql.= implode(", ", $sqlValues);

		return db::query( $sql );
	}

}

?>