<?php

/**
 * The controller file sets the base path to use
 * and should list modules, which are 1:1 to pages
 *
 * This project is using controllers directly
 * making use of the modules logic
 */

class Controller {

	public $controller_path = '';
	public $arrPaths		= array();

	public function __construct () {

		$this->arrPaths	= config::req( 'paths' );

	}

	public function load_controller ($controller_path='') {

		if($controller_path!='') {
			config::set('controller',$controller_path);
		}

		$controller	= config::sys('application').'controllers/'.config::get('controller').'.php';

		if(file_exists($controller)) {
			include($controller);
			$list		= get_declared_classes();
			$instance	= new $list[count($list)-1]();
		} else {
			die("class $controller not found");
		}
	}

	public function loopDataTemplate($tplName='',$arrData=false) {
		$strTemplate = '';

		if(strlen($tplName) > 0 && is_array($arrData) && count($arrData) > 0) {
			foreach($arrData as $uidItem=>$arrItem) {
				$strTemplate.=make::tpl($tplName)->assign($arrItem)->get_content();
			}
		}

		return $strTemplate;
	}

	public function get_route ( $segment=1, $default = 'main' ) {
		return (isset($this->arrPaths[$segment]) ? str_replace('-','_',$this->arrPaths[$segment]) : $default);
	}

}

?>