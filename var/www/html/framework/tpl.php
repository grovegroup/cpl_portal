<?php

class tpl extends text {

	public function __construct ($file='') {
		$this->path = config::sys('application').'views/'.$file.'.tpl';
	}

	public function load ($data='') {
		if(file_exists($this->path)) {
			$this->content = file_get_contents($this->path);
		} else {
			$this->content = $data;
		}
		return $this;
	}
}

?>