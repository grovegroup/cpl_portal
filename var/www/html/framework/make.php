<?php

/**
 * 
 */
class make {

	/**
	 * [tpl description]
	 * @param  string  $file    [description]
	 * @param  boolean $content [description]
	 * @param  array   $arrData [description]
	 * @return [type]           [description]
	 */
	public static function tpl ($file='', $content=false, $arrData=array()) {

		$tpl = new xhtml($file);
		$tpl->load();

		if(0 < count($arrData)) {
			$tpl->assign($arrData);
		}

		return $tpl;

	}

	/**
	 * [__callStatic description]
	 * @param  [type] $object [description]
	 * @param  array  $args   [description]
	 * @return [type]         [description]
	 */
	public static function __callStatic($object=null, $args=array()) {

		return new $object($args);

	}

}

?>